<?php

/**
 * @property Doctrine $doctrine
 * @property Twig $twig
 */

class MY_Controller extends CI_Controller {

    protected $user;
    protected $affaire;
    protected $sinistre;
    protected $location;

    public function __construct() {
        
        parent::__construct();

        $user = $this->session->userdata('user');
        if (!$user || !$user->id_utilisateur > 0) {
            redirect(base_url());
        } else {
            $this->user = $user;
        }

        $affaire = $this->session->userdata("affaire");
        $sinistre = $this->session->userdata("sinistre");

        if ($affaire)
            $this->affaire = $affaire;

        if ($sinistre)
            $this->sinistre = $sinistre;
    }

    public function setAffaire($affaire) {
        $this->session->set_userdata("affaire", $affaire);
        $this->affaire = $affaire;
    }

    public function setSinistre($sinistre) {
        $this->session->set_userdata("sinistre", $sinistre);
        $this->sinistre = $sinistre;
    }

    public function setUser($user) {
        $this->session->set_userdata("user", $user);
        $this->user = $user;
    }

    public function getContext() {
        return array(
            "user" => $this->user,
            "affaire" => $this->affaire,
            "sinistre" => $this->sinistre,
            "location" => $this->location
        );
    }

    public function display($view, $data = null) {
        if ($data == null)
            $data = array();

        $data["context"] = $this->getContext();

        $this->twig->display($view, $data);
    }

}
