<?php

//ini_set("display_errors", true);
//error_reporting(E_ALL);

use Entities\Adresse;
use Entities\Affaire;

/**
 * @property ImportAffaire $importAffaire
 */
class Affaire_c extends MY_Controller {

    /**
     * Affiche la fiche affaire
     *
     * Si un numero d'affaire est spécifié, l'affaire est recherchée dans la base contentieux.
     * Si l'affaire est trouvée, elle est enregistrée dans le contexte, sinon l'affaire du contexte est récupérée.
     *
     * @see Context
     * @param int $id_affaire
     * @return view
     */
    public function index($id_affaire = null) {

        $this->context->setStatut('FA');
        $sinistre = null;
        $affaires_op = null;

        // Recuperation de l'affaire par id, ou dans le contexte (session)
        if ($id_affaire != null) {
            $affaire = $this->doctrine->em->getRepository('Entities\Affaire')->find($id_affaire);
        } else {
            $affaire = $this->context->getAffaire();
        }

        //Si l'affaire est liée a qualiperf, recupération des informations de l'affaire
        if ($affaire->getId_qp_contrat()) {
            $this->load->model('ImportAffaire');
            $this->ImportAffaire->updateAffaire($affaire);
            $affaires_op = $this->ImportAffaire->getAffairesOp($affaire);
        }

        //Vérification des droits d'accès
        if ($affaire->checkAcces() == false) {
            echo "<script>alert('Vous n\'avez pas acces à cette affaire')</script>";
            redirect('');
            return;
        }

        //Enregistrement de l'affaire dans le contexte (session), si l'affaire n'a qu'un seul sinistre celui ci est également enregistré dans le contexte
        $this->context->setAffaire($affaire);
        $sinistres = $affaire->getSinistres();
        if (count($sinistres) == 1) {
            $this->context->setSinistre($sinistres[0]);
        }

        // Récupération de la Mo et Moe
        $this->load->model('Mo');
        $mos = $this->Mo->getMo($affaire);
        $this->load->model('Moe');
        $moes = $this->Moe->getMoe($affaire);

        //Récupération du RCR lié à l'affaire
        $this->load->model('User');
        $rcrs = $this->User->getRcr();
        $id_rcr_aff = $affaire->getRcr();
        if ($id_rcr_aff == null) {
            $rcr_aff = $this->doctrine->em->getRepository('Entities\Rcr')->findOneBy(array('id_agence' => $affaire->getId_agence()));
            if ($rcr_aff) {
                $id_rcr_aff = $rcr_aff->getId_rcr();
                $affaire->setRcr($id_rcr_aff);
                $this->doctrine->em->persist($affaire);
                $this->doctrine->em->flush();
            }
        }

        //Récupération du nom du service émetteur
        if ($affaire->getId_service() && $affaire->getId_service() != "") {
            $this->load->model('Utils');
            $nom_svc = $this->Utils->getNomService($affaire->getId_service());
        }

        //Récupération du nom du CA
        $this->load->model('User');
        $ca = $this->User->getBaseUser($affaire->getId_ca());
        $nom_ca = "";
        if ($ca)
            $nom_ca = $ca->prenom . " " . $ca->nom;

        // Recuperation des tranches dans gaia
        $tranches = null;
        if ($affaire->getId_g_affaire()) {
            $tranches = $this->ImportAffaire->getTranches($affaire);
        }

        // liste des services pour sélection
        $this->load->model("Ajax");
        $services = $this->Ajax->getServices();

        //Affichage de la fiche affaire
        $this->twig->display('affaire/fiche_affaire.html.twig', array(
            'mos' => $mos,
            'moes' => $moes,
            'topbar2' => 3,
            'rcrs' => $rcrs,
            'id_rcr_aff' => $id_rcr_aff,
            'affaires_op' => $affaires_op,
            'nom_service' => @$nom_svc,
            'nom_ca' => $nom_ca,
            'tranches' => $tranches,
            "services" => $services
        ));
    }

    /**
     * Crée/ modifie une nouvelle affaire
     */
    public function edit($isnew = null) {

        $this->load->model('importAffaire');

//        ini_set("display_errors", true);
//        error_reporting(E_ALL);
//        var_dump($_POST);

        if (!$isnew)
            $affaire = $this->context->getAffaire();
        else {
            $affaire = $this->importAffaire->importQualiperf($_POST["numero"], false);
        }

        if (!$affaire instanceof Affaire)
            $affaire = new Affaire();

        $num_affaire = $_POST["numero"];
        $libelle = $_POST["libelle"];
        $rue1 = $_POST["rue1"];
        $rue2 = $_POST["rue2"];
        $rue3 = $_POST["rue3"];
        $codePostal = $_POST["code_postal"];
        $commune = $_POST["commune"];
        $missions = $_POST["missions"];
        $id_service = $_POST["id_service"];
        $service = $_POST["service"];
        $id_service_ctx = @$_POST["id_service_ctx"];
        $service_ctx = $_POST["service_ctx"];
        $rcr = @$_POST["rcr"];
        $mo = isset($_POST['mo_nom']) ? $_POST['mo_nom'] : "";
        $ct_mo = isset($_POST['mo_contact']) ? $_POST['mo_contact'] : null;
        $id_ca = isset($_POST['id_ca']) ? $_POST['id_ca'] : null;

        if ($id_ca) {
            $this->load->model('User');
            $ca = $this->User->getBaseUser($id_ca);
            $nom_ca = $ca->prenom . " " . $ca->nom;
        }
        if (!empty($_POST['id_service_ctx']) && $_POST['id_service_ctx'] != "" && $_POST['id_service_ctx'] != null) {
            $affaire->setId_service_contentieux($_POST['id_service_ctx']);
            $affaire->setNom_service_contentieux($_POST['service_ctx']);
        }

        $id_agence = $this->importAffaire->getId_agence($affaire->getId_service_contentieux());
        if ($id_agence)
            $affaire->setId_agence($id_agence);

        if ($rcr != "" && $rcr != null) {
            $affaire->setRcr($rcr);
        } else {
            $rcr_aff = $this->doctrine->em->getRepository('Entities\Rcr')->findOneBy(array('id_agence' => $affaire->getId_agence()));
            if ($rcr_aff) {
            $affaire->setRcr($rcr_aff->getId_rcr());
        }
        }

        $affaire->setCode($num_affaire);
        $affaire->setLibelle($libelle);
        $affaire->setMissions($missions);
        $affaire->setId_service($id_service);
        $affaire->setNom_service($service);

        if ($id_ca) {
            $affaire->setId_ca($id_ca);
            $affaire->setCa($nom_ca);
        }


        if ($affaire->getId_service()) {
            $this->load->model('Utils');
            $sage = $this->Utils->getSAGE($id_service);

            $affaire->setSage($sage);
        }

        $this->set_region($affaire);


        $affaire->setAdresse(new Adresse($rue1, $rue2, $rue3, $codePostal, $commune));

        $affaire->setMo($ct_mo);
        $affaire->setNom_mo($mo);

        $moes = array();

        foreach ($_POST as $k => $elem) {
            if (substr($k, 0, 8) == "moe_nom_") {
                $num = substr($k, 8, 9);
                if ($num == "") {
                    $moes[$elem] = $_POST['moe_contact_'];
                } else {
                    $moes[$elem] = $_POST['moe_contact_' . $num];
                }
            }
        }
        $affaire->setMoe($moes);

        $this->doctrine->em->persist($affaire);
        $this->doctrine->em->flush();

        $this->context->setStatut('FA');
        $this->context->setAffaire($affaire);

        redirect("/affaire_c/index/");
    }

    /**
     * vue de l'email de demande d'informations
     */
    public function identification() {
        $this->twig->display('mails/mail_identification_affaire.html.twig', array('test' => true, 'edit' => true, 'panel_title' => 'Affaire non identifiable', 'mail_content' => 'mails/patron/affaire_non_identifiable.html.twig'));
    }

    public function nouvelle_affaire() {

        if (!isset($_POST['affaire_numero'])) {
            $this->twig->display('affaire/nouvelle_affaire.html.twig', array('topbar2' => 2));
            return;
        }

        if (isset($_POST['affaire_numero']) && $_POST['affaire_numero'] != "") {
            $num_affaire = $_POST['affaire_numero'];

            $affaire = $this->check_contentieux($num_affaire);
            if ($affaire) {
                $this->context->setStatut('FA');
                $this->context->setAffaire($affaire);
                // liste des services pour sélection
                $this->load->model("Ajax");
                $services = $this->Ajax->getServices();
                $this->twig->display("affaire/fiche_affaire.html.twig", array('affaire' => $affaire, 'services' => $services, 'topbar2' => 2));
                return;
            }

            $affaire = $this->check_qualiperf($num_affaire);
            if ($affaire) {
                $this->import($affaire->code);
            } else {
                $this->creation($num_affaire);
            }
        }

        if (isset($_POST['affaire_numero']) && $_POST['affaire_numero'] == "") {
            $this->creation("NC");
        }
    }

    public function creation($num_affaire = null) {
        
        if (!isset($_POST['numero'])) {
            $this->load->model("Ajax");
            $services = $this->Ajax->getServices();
            $this->twig->display('affaire/creation.html.twig', array('num_affaire' => $num_affaire, "services" => $services));
            return;
        }

        $affaire = new Affaire();
        $affaire->setCode($_POST['numero']);
        $affaire->setLibelle($_POST['libelle']);
        @$affaire->setMissions($_POST['missions']);
        @$affaire->setId_ca($_POST['ca']);
        @$adresse = new Entities\Adresse($_POST['rue1'], $_POST['rue2'], $_POST['rue3'], $_POST['code_postal'], $_POST['commune']);
        @$affaire->setAdresse($adresse);
        $affaire->setId_service_contentieux($_POST['id_service_ctx']);
        $affaire->setNom_service_contentieux($_POST['nom_service_ctx']);

        if ($_POST['mo_nom'] != "") {
            $affaire->setNom_mo($_POST['mo_nom']);
            $affaire->setMo($_POST['mo_contact']);
        }
        $this->load->model('Utils');
        if (isset($_POST['id_service']) && $_POST['id_service'] != "")
            $sage = $this->Utils->getSAGE($_POST['id_service']);
        if (@$sage)
            $affaire->setSage($sage);
        if ($affaire->getId_ca() != "" && $affaire->getId_ca() != null)
            $this->getLoc($affaire);
        if (isset($_POST['id_service']) && $_POST['id_service'] != "") {
            $affaire->setId_service($_POST['id_service']);
            $this->load->model('Utils');
            $nom_service = $this->Utils->getNomService($_POST['id_service']);
            $affaire->setNom_service($nom_service);
        }
        if (isset($_POST['id_service_ctx']) && $_POST['id_service_ctx'] != "") {
            $affaire->setId_service_contentieux($_POST['id_service_ctx']);
            $this->load->model('Utils');
            $nom_service_contentieux = $this->Utils->getNomService($_POST['id_service_ctx']);
            $affaire->setNom_service_contentieux($nom_service_contentieux);
        }

        $this->set_region($affaire);

        $i = 1;
        $moe = array();

        while (isset($_POST['moe_nom_' . $i])) {
            if ($_POST['moe_nom_' . $i] != "")
                $moe[$_POST['moe_nom_' . $i]] = $_POST['moe_contact_' . $i];
            $i++;
        }

        $affaire->setMoe(serialize($moe));
        $this->doctrine->em->persist($affaire);
        $this->doctrine->em->flush();

        $this->context->setAffaire($affaire);
        redirect("/affaire_c/index/");
    }

    public function import($num_affaire = null) {
//        ini_set("display_errors", true);
//        error_reporting(E_ALL);
        if ($num_affaire == null && isset($_POST['numero'])) {
            $this->load->model('importAffaire');
            $num_affaire = $_POST['numero'];
            $rcr = isset($_POST['rcr']) ? $_POST['rcr'] : null;
            $affaire = $this->importAffaire->importQualiperf($num_affaire);
            $affaire->setRcr($rcr);
            $affaire->setId_service_contentieux($_POST['id_service_ctx']);
            $affaire->setNom_service_contentieux($_POST['service_ctx']);
            $this->doctrine->em->persist($affaire);
            $this->doctrine->em->flush();
            $this->context->setAffaire($affaire);
            redirect("/affaire_c/index/");
            return;
        }

        $this->load->model('ImportAffaire');
        $affaire = $this->ImportAffaire->importQualiperf($num_affaire);

        if ($affaire)
            $affaire->setCode($num_affaire);

        $this->load->model('Mo');
        $mos = $this->Mo->getMo($affaire);

        $this->load->model('Moe');
        $moes = $this->Moe->getMoe($affaire);

        $message = "Cette affaire sera importée dans la base contentieux, merci de contrôler les informations et de valider l'import.";

        $this->load->model('User');
        $rcrs = $this->User->getRcr();
        $id_rcr_aff = $affaire->getRcr();
        if ($id_rcr_aff == null) {
            $rcr_aff = $this->doctrine->em->getRepository('Entities\Rcr')->findOneBy(array('id_agence' => $affaire->getId_agence()));
            if ($rcr_aff)
                $id_rcr_aff = $rcr_aff->getId_rcr();
        }

        $this->load->model("Ajax");
        $services = $this->Ajax->getServices();

        $this->twig->display('affaire/preview_affaire.html.twig', array('affaire' => $affaire, 'message' => $message, 'import' => true, 'mos' => $mos, 'moes' => $moes, 'topbar2' => 2, 'rcrs' => $rcrs, 'id_rcr_aff' => $id_rcr_aff, "services" => $services));
    }

    public function affaire_non_identifiable() {
        $user = $this->context->getUser();

        if (isset($_POST['destinataires'])) {
            $vref = $_POST['vref'];
            $date_reunion = $_POST['date_reunion'];
            $mail = $this->twig->render('mails/patrons/identification.html.twig', array('user' => $user, 'edit' => false, 'vref' => $vref, 'date_reunion' => $date_reunion));
            $destinataires = $_POST['destinataires'];
            $ccs = $_POST['cc'];
            $objet = $_POST['objet'];

            $this->load->library('email');
            $this->email->initialize(array('mailtype' => 'html'));
            $this->email->from($user->email, 'Qualiconsult - Service contentieux');
            $this->email->to($destinataires);
            $this->email->cc($ccs);
            $this->email->subject($objet);
            $this->email->message($mail);
            $this->email->send();
            echo '<script>alert("email envoyé");</script>';
            redirect(base_url());
        }

        $mail = $this->twig->render('mails/patrons/identification.html.twig', array('user' => $user, 'edit' => true));
        $diff = $this->doctrine->em->getRepository('Entities\Mailinglist')->findOneBy(array('type' => 0));
        $objet = "Affaire suivant convocation expert";
        $this->twig->display('affaire/identification_affaire.html.twig', array('mail_content' => $mail, 'edit' => true, 'objet' => $objet, 'topbar2' => 3));
    }

    public function check_contentieux($num_affaire) {
        $affairesRepo = $this->doctrine->em->getRepository('Entities\Affaire');
        $affaire = $affairesRepo->findOneBy(array('code' => $num_affaire));
        if ($affaire)
            return $affaire;
        else
            return false;
    }

    public function check_qualiperf($num_affaire) {
        $this->load->model('ImportAffaire');
        $affaire = $this->ImportAffaire->checkInQualiperf($num_affaire);

        if ($affaire)
            return $affaire[0];
        else
            return false;
    }

    public function set_region(Affaire &$affaire) {
        $this->load->model('importAffaire');
        $infos_region = $this->importAffaire->get_region($affaire->getId_service_contentieux());
        $affaire->setId_region($infos_region["id_Region"]);
        $affaire->setNom_region($infos_region["Region"]);

        $this->doctrine->em->persist($affaire);
        $this->doctrine->em->flush();
    }

    public function getLoc(Affaire &$affaire) {
        $this->load->model('importAffaire');
        $id_ca = $affaire->getId_ca();
        $loc = $this->importAffaire->getLoc($id_ca);
        if ($loc) {
            $affaire->setCa($loc->aff_collaborateur_ca);
            $affaire->setId_agence($loc->aff_id_agence);
            $affaire->setId_service($loc->aff_id_service);
            $affaire->setNom_service($loc->aff_nom_service);
        }
    }

    public function toggle_archiver($id_affaire) {
        /* @var $affaire \Entities\Affaire */
        $affaire = $this->doctrine->em->getRepository('Entities\Affaire')->find($id_affaire);
        if ($affaire->getStatut_affaire() == 1) {
            $affaire->setStatut_affaire(0);
        } else {
            foreach ($affaire->getSinistres() as $sinistre) {
                if ($sinistre->getFlag_sinistre_desactive() == 0) {
                    die("NOK");
                }
            }
            $affaire->setStatut_affaire(1);
        }

        $this->doctrine->em->persist($affaire);
        $this->doctrine->em->flush();

        echo "OK";
    }

}
