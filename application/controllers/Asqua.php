<?php

/**
 * Classe pour la synchro asqua, sert également à toute action journalière, un cron execute synchroAsqua() tous les jours à 4h30
 */
class Asqua extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function tdb($token = null) {
        if ($token != "0305E82C3301")
            die('Accès restreint');

        $sr = $this->doctrine->em->getRepository('Entities\Sinistre');
        $sinistres = $sr->getNonSynchros();

        $this->twig->display('tdb_asqua.html.twig', array('sinistres' => $sinistres));
    }

    public function toDB() {
        $this->load->model('Asqua_model');
        $liste = $this->getListe();

        $this->Asqua_model->refresh($liste);
    }

    public function ref_asqua($id, $guid) {
        $this->load->library('asqua_lib');
        $this->asqua_lib->ref_asqua($id, $guid);
    }

    private function update_new_sinistre() {
        $this->load->library('asqua_lib');
        $this->asqua_lib->update_new_sinistre();
    }

    public function synchroAsqua($pass, $ref_asqua = null, $update = "true") {
        try {
            $update = ($update == "true");
            $this->load->library('asqua_lib');
            $this->asqua_lib->synchroAsqua($pass, $ref_asqua, $update);
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function index() {
        $liste = $this->getListe();
        $this->twig->display('recherche_asqua.html.twig');
    }

    private function getListe() {
        $path_csv1 = realpath(APPPATH . "../files/asqua/gaia_axa.csv");
        $path_csv2 = realpath(APPPATH . "../files/asqua/gaia_sma.csv");

        $array1 = $this->parse_file($path_csv1);
        $array2 = $this->parse_file($path_csv2);

        $liste = array_merge($array1, $array2);

        return $liste;
    }

    private function parse_file($file) {
        $doc = array();
        $handle = fopen($file, 'r');
        while (!feof($handle)) {
            $data = fgetcsv($handle, 0, "|");
            if (is_array($data) && $data[0] != "date derniere modif") {
                foreach ($data as $i => $k) {
                    $data[$i] = utf8_encode($k);
                }
                $doc[] = $data;
            }
        }

        return $doc;
    }

    public function getListAjax() {
        $liste = $this->getListe();
        echo json_encode($liste);
    }

    public function download($id) {

        if ($id[0] != "g" && $id[0] != "q") {
            $fileR = $this->doctrine->em->getRepository('Entities\File');
            $file = $fileR->find($id);
        }
        if ($id[0] == "g") {
            $id = str_replace("g", "", $id);
            $this->load->model('ImportDocuments');
            $file = $this->ImportDocuments->getDoc($id);
        }
        if ($id[0] == "q") {
            $id = str_replace("q", "", $id);
            $this->load->model('ImportDocuments');
            $file = $this->ImportDocuments->getDocQp($id);
            $tmp = $this->getByCurl($file->getUri());
            $file->setUri($tmp);
        }
        log_message('DEBUG', $file->getUri());
        if (!$file) {
            echo 'erreur';                                                              //exceptionme
            return;
        } else {
            $data = $file->getUri();
            ignore_user_abort(true);
            set_time_limit(0); // disable the time limit for this script
            if ($fd = fopen($data, "r")) {
                $fsize = filesize($data);
                $path_parts = pathinfo($data);
                header("Content-type: application/octet-stream");
                header("Content-Disposition: filename=\"" . $file->getNom() . "\"");
                header("Content-length: $fsize");
                header("Cache-control: private");
                while (!feof($fd)) {
                    $buffer = fread($fd, 2048);
                    echo $buffer;
                }
                fclose($fd);
            }
            $id = $file->getId();
            if ($id[0] == "q") {
                unlink($file->getUri());
            }
            exit;
        }
    }

    public function fiche_sinistre() {

        $sin = $this->context->getSinistre();
        $statut_int = $sin->getStatut();

        if ($sin->getAssureur() == "SMA")
            $url = "http://87.238.144.42/quali-sagebat.nsf/WSinistres/" . $sin->getRef_asqua() . "?OpenDocument";
        else
            $url = "http://87.238.144.42/qualiconsult.nsf/WSinistres/" . $sin->getRef_asqua() . "?OpenDocument";

        if (!isset($_POST['destinataires'])) {
            $this->twig->display("fiche_asqua.html.twig", array("topbar2" => 7, "url" => $url));
            return;
        }
    }

    public function trim_ref_asqua() {
        header("Content-type:Text/plain");
        $this->load->model('Utils');
        $refs = $this->Utils->get_refs_asqua();
        //mb_internal_encoding('UTF-8');
        $c = 0;
        foreach ($refs as $ref) {
            $oldref = strval($ref["ref_asqua"]);
            $newref = strval(preg_replace("/(.*\/|[^\.a-zA-Z\d:]*)/u", '', $oldref));

            if ($newref !== $oldref) {
                echo "\"$oldref\" > \"$newref\"\n";
                $c++;

                //$this->Utils->update_ref_asqua($ref["id"], $newref);
            }
        }

        echo $c . " refs à modifier";
    }

}
