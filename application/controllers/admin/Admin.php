<?php

class Admin extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $user = $this->context->getUser();

        if ($user->role != 10 && $user->role != 8 && $user->role != 7)
        {
            redirect(base_url());
            die();
        }
    }

    public function acces()
    {

        if (isset($_POST['libelle_1']))
        {
            $i = 1;
            while (isset($_POST['libelle_' . $i]))
            {
                $row = $this->doctrine->em->getRepository('Entities\Acces')->find($i);
                if (!$row instanceof Entities\Acces)
                    $row = new Entities\Acces ();
                $row->setLibelle($_POST['libelle_' . $i]);
                $row->setClasse($_POST['classe_' . $i]);
                if ($_POST['methode_' . $i] != "")
                    $row->setMethode($_POST['methode_' . $i]);
                else
                    $row->setMethode("global");
                $row->setGeneraliste($_POST['generaliste_' . $i]);
                $row->setAca($_POST['aca_' . $i]);
                $row->setRca($_POST['rca_' . $i]);
                $row->setDa($_POST['da_' . $i]);
                $row->setRcr($_POST['rcr_' . $i]);
                $row->setDr($_POST['dr_' . $i]);
                $row->setDcn($_POST['dcn_' . $i]);
                $row->setDj($_POST['dj_' . $i]);
                $row->setAsqua($_POST['asqua_' . $i]);

                $this->doctrine->em->persist($row);
                $i++;
            }
            $this->doctrine->em->flush();
        }

        $acces = $this->doctrine->em->getRepository('Entities\Acces')->findAll();
        $this->twig->display('admin/acces.html.twig', array('acces' => $acces, 'topbar2' => 4));
    }

    public function mail()
    {

        $mailingListRepo = $this->doctrine->em->getRepository('Entities\MailingList');

        if (isset($_POST))
        {
            foreach ($_POST as $key => $value)
            {
                $split = explode("_", $key);
                $id = $split[0];
                $field = $split[1];

                $mail = $mailingListRepo->find($id);

                switch ($field)
                {
                    case "dest":
                        $mail->setDestinataires($value);
                        break;
                    case "cc":
                        $mail->setCcs($value);
                        break;
                }
                $this->doctrine->em->persist($mail);
            }
        }

        $this->doctrine->em->flush();
        $mails = $mailingListRepo->findBy(array(), array('type' => 'ASC'));
        $this->twig->display('admin/mails.html.twig', array('mails' => $mails, 'topbar2' => 1));
    }

    public function documents()
    {

        $user = $this->session->userdata('user');
        if ($user->role != 10)
            redirect('/');

        $docsTypeRepo = $this->doctrine->em->getRepository('Entities\Type_docs');
        if (isset($_POST['doc_1']))
        {
            $i = 1;
            while (isset($_POST['doc_' . $i]))
            {
                $doc = $docsTypeRepo->find($i);
                $cat = $this->doctrine->em->getRepository('Entities\Categories_docs')->find($_POST['doc_' . $i]);
                $doc->setCategorie($cat);
                $this->doctrine->em->persist($doc);
                $i++;
            }
            $this->doctrine->em->flush();
        }
        $categories = $this->doctrine->em->getRepository('Entities\Categories_docs')->findAll();

        $doctypes = $docsTypeRepo->findBy(array(), array('num' => 'ASC'));
        $this->twig->display('admin/docs.html.twig', array('docs' => $doctypes, 'topbar2' => 2, 'categories' => $categories));
    }

    public function roles()
    {
        if (isset($_POST))
        {
            $deleted_rca = array();
            $deleted_aca = array();
            foreach ($_POST as $key => $post)
            {
                if ($post != "")
                {
                    if (substr($key, 0, 3) == "rca")
                    {
                        $id_agence = substr($key, 4);
                        $asso = $this->doctrine->em->getRepository('Entities\Rcr')->findOneBy(array('id_agence' => $id_agence));
                        $this->load->model('User');
                        $rca = $this->User->getById($post);

                        $asso->setId_rca($post);
                        $asso->setNom_rca($rca->prenom . " " . $rca->nom);
                        $asso->setMail_rca($rca->email);
                        $this->doctrine->em->persist($asso);

                        $this->setRole($rca->id_utilisateur, 3);
                    } elseif (substr($key, 0, 3) == "aca")
                    {
                        $id_agence = substr($key, 4);
                        $asso = $this->doctrine->em->getRepository('Entities\Rcr')->findOneBy(array('id_agence' => $id_agence));
                        $this->load->model('User');
                        $aca = $this->User->getById($post);

                        $asso->setId_aca($post);
                        $asso->setNom_aca($aca->prenom . " " . $aca->nom);
                        $asso->setMail_aca($aca->email);
                        $this->doctrine->em->persist($asso);

                        $this->setRole($aca->id_utilisateur, 2);
                    } else
                    {
                        $id_agence = $key;
                        $asso = $this->doctrine->em->getRepository('Entities\Rcr')->findOneBy(array('id_agence' => $id_agence));
                        $this->load->model('User');
                        $rcr = $this->User->getById($post);
                        if ($rcr)
                        {
                            $asso->setId_rcr($post);
                            $asso->setNom_rcr($rcr->prenom . " " . $rcr->nom);
                            $asso->setMail_rcr($rcr->email);
                            if ($post != 3950)
                            { // luc moitry = dcn
                                $this->setRole($rcr->id_utilisateur, 5);
                            }
                        }
                        $this->doctrine->em->persist($asso);
                    }
                } else
                {
                    if (substr($key, 0, 3) == "rca")
                    {
                        $id_agence = substr($key, 4);
                        $asso = $this->doctrine->em->getRepository('Entities\Rcr')->findOneBy(array('id_agence' => $id_agence));
                        $id_utilisateur = $asso->getId_rca();
                        $asso->setId_rca(null);
                        $asso->setNom_rca(null);
                        $asso->setMail_rca(null);
                        if ($id_utilisateur != null && $id_utilisateur != "")
                            $deleted_rca[] = $id_utilisateur;
                        $this->doctrine->em->persist($asso);
                    } elseif (substr($key, 0, 3) == "aca")
                    {
                        $id_agence = substr($key, 4);
                        $asso = $this->doctrine->em->getRepository('Entities\Rcr')->findOneBy(array('id_agence' => $id_agence));
                        $id_utilisateur = $asso->getId_aca();
                        $asso->setId_aca(null);
                        $asso->setNom_aca(null);
                        $asso->setMail_aca(null);
                        if ($id_utilisateur != null && $id_utilisateur != "")
                            $deleted_aca[] = $id_utilisateur;
                        $this->doctrine->em->persist($asso);
                    } else
                    {
                        $id_agence = $key;
                        $asso = $this->doctrine->em->getRepository('Entities\Rcr')->findOneBy(array('id_agence' => $id_agence));
                        $asso->setId_rcr(null);
                        $asso->setNom_rcr(null);
                        $asso->setMail_rcr(null);
                        $this->doctrine->em->persist($asso);
                    }
                }
            }
            $this->doctrine->em->flush();

            foreach ($deleted_rca as $id_rca)
            {
                $isRca = $this->doctrine->em->getRepository('Entities\Rcr')->findOneBy(array('id_rca' => $id_rca));
                if (!$isRca)
                {
                    $this->load->model('User');
                    $this->User->update_role($id_rca, null);
                }
            }
            foreach ($deleted_aca as $id_aca)
            {
                $isAca = $this->doctrine->em->getRepository('Entities\Rcr')->findOneBy(array('id_aca' => $id_aca));
                if (!$isAca)
                {
                    $this->load->model('User');
                    $this->User->update_role($id_aca, null);
                }
            }
        }

        $this->load->model('Ajax');
        $rcrs = $this->Ajax->getRCR();

        $assos = $this->doctrine->em->getRepository('Entities\Rcr')->findBy(array(), array("cp_agence" => "asc"));

        $this->twig->display('admin/roles.html.twig', array('rcrs' => $rcrs, 'assos' => $assos, 'topbar2' => 3));
    }

    public function fillRcrWithAgences()
    {
        $this->load->model('Utils');
        $agences = $this->Utils->getAgences();

        foreach ($agences as $agence)
        {
            $rcr = new \Entities\Rcr();
            $rcr->setId_agence($agence->id_entite);
            $rcr->setNom_agence($agence->nom);
            $rcr->setCp_agence($agence->code_postal);
            $this->doctrine->em->persist($rcr);
        }
        $this->doctrine->em->flush();
        echo 'done';
    }

    private function setRole($id, $role)
    {
        $this->load->model('User');
        $this->User->update_role($id, $role);
    }

    public function logAs($id_utilisateur)
    {
        $user = $this->session->userdata('user');
        if ($user->role != 10)
            redirect('/');

        $this->load->model('User');
        $user = $this->User->get(null, $id_utilisateur);

        if ($user && $user->role != '')
        {
            $this->session->set_userdata('user', $user);
            log_message("INFO", "Utilisateur connecté : " . $user->prenom . ' ' . $user->nom);
            redirect(base_url());
            return;
        }
    }

}
