<?php

/**
 * @property Admin_documents_model $Admin_documents_model
 */
Class Admin_documents extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $user = $this->context->getUser();

        if ($user->role != 10 && $user->role != 8) {
            redirect(base_url());
            die();
        }
    }

    public function index() {

        $this->load->model("Admin_documents_model");
//        ini_set("display_errors", 1);
//        error_reporting(E_ALL);  //gerer FK

        if (@$_GET["action"] == "change") {
            $id = $_GET["id"];
            $col = $_GET["col"];
            $val = json_decode($_GET["val"]);
            $this->Admin_documents_model->update_categorie_doc($id, $col, $val);
            die();
        } else if (@$_GET["action"] == "delete") {
            $id = $_GET["id_doc"];
            $this->Admin_documents_model->del_categorie_doc($id);
            die("ok");
        } else if (@$_GET["action"] == "add") {
            die(json_encode($this->Admin_documents_model->add_categorie_doc()));
        }

        $categories = $this->Admin_documents_model->get_categories_docs();
        $types = $this->Admin_documents_model->get_types_docs();

        $this->twig->display("admin/categories_docs.html.twig", array("topbar2" => 5, "categories" => $categories, "types" => $types));
    }

    public function lier_type_categorie($num_type, $num_categorie) {
        $this->load->model('Admin_documents_model');
        $this->Admin_documents_model->lier_type_categorie($num_type, $num_categorie);
    }

    public function supprimer_asso_type_cat($id) {
        $this->load->model('Admin_documents_model');
        $this->Admin_documents_model->supprimer_asso_type_cat($id);
        die("ok");
    }

    public function change_libelle_type($id) {
        $this->load->model('Admin_documents_model');
        $libelle_str = json_decode($_GET["libelle"]);
        $this->Admin_documents_model->change_libelle_type_doc($id, $libelle_str);
    }

    public function change_num_type($id) {
        $this->load->model('Admin_documents_model');
        $num = json_decode($_GET["num"]);
        $this->Admin_documents_model->change_num_type_doc($id, $num);
    }

    public function del_type($id) {
        $this->load->model('Admin_documents_model');
        $this->Admin_documents_model->del_type($id);
        die("ok");
    }

    public function ajout_type_doc() {
        $this->load->model("Admin_documents_model");
        $id = $this->Admin_documents_model->add_type_doc();
        die($id);
    }

    public function dedoublonner() {
        session_write_close();
//        ini_set("display_errors", true);
//        error_reporting(E_ALL);
        $this->load->model("Admin_documents_model");
        $doublons = $this->Admin_documents_model->rechercher_doublons();
        $this->twig->display("admin/doublons_docs.html.twig", array("topbar2" => 6, "doublons" => $doublons));
    }

    public function remplir_asso_histo_doc() {
//        ini_set("display_errors", true);
//        error_reporting(E_ALL);
        $this->load->model("Admin_documents_model");
        $this->Admin_documents_model->remplir_asso_histo_doc();
    }

    public function fix_serialized() {
//        ini_set("display_errors", true);
//        error_reporting(E_ALL);
        $this->load->model("Admin_documents_model");
        $this->Admin_documents_model->fix_serialized();
    }

}
