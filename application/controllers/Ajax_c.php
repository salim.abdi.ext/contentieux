<?php

class Ajax_c extends CI_Controller {

    public function listCa() {
        $this->load->model('Ajax');
        $cas = $this->Ajax->getCa();

        $list = "";

        foreach ($cas as $ca) {
            $list .= "<option value=$ca->id_utilisateur>$ca->prenom $ca->nom</option>";
        }

        echo $list;
    }

    public function listCaRaw() {
        $this->load->model('Ajax');
        $cas = $this->Ajax->getCa();

        echo json_encode($cas);
    }

    public function listUsers() {
        $this->load->model('Ajax');
        $cas = $this->Ajax->getUsers();

        echo json_encode($cas);
    }

    public function listSvRaw() {
        $this->load->model('Ajax');
        $services = $this->Ajax->getServices();

        echo json_encode($services);
    }

    public function listCodes() {
        $this->load->model('Ajax');
        $r = $this->Ajax->getCodes($_GET['term']);
        echo json_encode($r);
    }

    public function getRCR() {
        $this->load->model('Ajax');
        $rcrs = $this->Ajax->getRCR();
        $list = "";
        foreach ($rcrs as $rcr) {
            $list .= "<option value='$rcr->prenom $rcr->nom'>$rcr->prenom $rcr->nom</option>";
        }
        echo $list;
    }

    public function getTypeDocs() {
        $typesRepo = $this->doctrine->em->getRepository('Entities\Type_docs');
        $types = $typesRepo->findAll(array('type' => 'desc'));

        $list = "";
        foreach ($types as $type) {
            $id = $type->getId();
            $libelle = $type->getType();
            $list .= "<option value='$id'>$libelle</option>";
        }

        echo $list;
    }

    public function getLibelleHist($type) {
        $sinistre = $this->context->getSinistre();
        $repo = $this->doctrine->em->getRepository('Entities\Historique');

        if ($type == "mail") {
            $items = $repo->getLibellesMail($sinistre);
        }
        if ($type == "courrier") {
            $items = $repo->getLibellesCourrier($sinistre);
        }
        $data = "<option value=''>Tous</option>";
        if ($items)
            foreach ($items as $item) {
                if ($item['libelle'] != null)
                    $data .= "<option value=\"" . $item['libelle'] . "\">" . $item['libelle'] . "</option>";
            }

        echo $data;
    }

    public function getFormLo() {
        $form = $this->twig->render('global/forms/contact.html.twig', array('justname' => true, 'edit' => true, 'prefix' => 'lo'));
        $adresse = $this->twig->render('global/forms/form_adresse.html.twig', array('edit' => true, 'prefix' => 'lo'));
        $data = <<<HTML
                <div class='col-md-12 lo'>
                    <div class='col-md-2'>
                        <button type='button' class='del_lo btn btn-sm btn-primary'>Supprimer</button>
                    </div>
                    <div class='col-md-9'>
                        <div class='col-md-12'  style='padding-bottom: 10px'>$form</div>
                    <div class='col-md-1'>Adresse</div>
                    <div class='col-md-11'>$adresse</div>
                    <div class='col-md-12'> <br><br></div>
                </div>
HTML;
        echo $data;
    }

    public function getContrats($id_sinistre) {
        $sinistre = $this->doctrine->em->getRepository('Entities\Sinistre')->findOneBy(array('id' => $id_sinistre));
        $contrats = $this->doctrine->em->getRepository('Entities\File')->getContrat($sinistre);
        $contrats_json = array();
        foreach ($contrats as $contrat) {
            $elem = array();
            $elem['nom'] = $contrat->getNom();
            $elem['uri'] = $contrat->getUri();
            $elem['id'] = $contrat->getId();
            $contrats_json[] = $elem;
        }

        echo json_encode($contrats_json);
    }

    public function getSage($id_service) {
        $this->load->model('Utils');
        $sage = $this->Utils->getSAGE($id_service);

        echo $sage;
    }

    public function get_utilisateurs_qperf_autocomp() {
        @$term = $_GET['term'];
        $this->load->model('Ajax');
        $r = $this->Ajax->get_utilisateurs_qperf_autocomp(strtolower($term));
        echo json_encode($r);
    }

    public function get_sv_qperf_autocomp() {
        @$term = $_GET['term'];
        $this->load->model('Ajax');
        $r = $this->Ajax->get_sv_qperf_autocomp(strtolower($term));
        echo json_encode($r);
    }

}
