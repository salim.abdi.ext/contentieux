<?php

class Support_c extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function post() {
        $code_affaire = null;
        $this->load->library('context');
        $date = new DateTime();
        $collaborateur = $this->context->getUser();
        $collaborateur_str = $collaborateur->prenom . " " . $collaborateur->nom;
        $collaborateur_id_art = $collaborateur->id_utilisateur_artemis;
        $affaire = $this->context->getAffaire();
        if ($affaire)
            $code_affaire = $affaire->getCode();
        $id_sv = $collaborateur->id_service;

        $type = $_POST["type"];
        $objet = $_POST["objet"];
        $urgence = $_POST["urgence"];
        $suggestion = $_POST["suggestion"];

        $this->load->model('Support');
        $this->Support->add($id_sv, $collaborateur_id_art, $collaborateur_str, $urgence, $type, $objet, $suggestion, $code_affaire);
    }

    public function test(){
        phpinfo();
    }

}
