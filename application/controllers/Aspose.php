<?php

/**
 * @property Aspose_model $Aspose_model
 * @property Edition_model $Edition_model
 * @property Sinistre_model $Sinistre_model
 * @property Affaire_model $Affaire_model
 * @property Documents_model $Documents_model
 * @property Historique_model $Historique_model
 */
class Aspose extends CI_Controller {

    public $upload_path;
    public $types_docs_affaire;

    public function __construct() {
        parent::__construct();
        $this->load->model("Aspose_model");
        $this->load->model("Edition_model");
        $this->load->model("Sinistre_model");
        $this->load->model("Affaire_model");
        $this->load->model("Documents_model");
        $this->upload_path = $this->config->item("path_aspose");
    }

    public function service_aspose2($id, $guid) {
        $this->load->library('AsposeLib');
        $this->asposelib->getAns($id, $guid);
        return;
    }

    public function service_aspose($id, $guid) {

//        ini_set("display_errors", 1);
//        error_reporting(E_ALL);

        $row_service_aspose = $this->Aspose_model->get_row_service_aspose($id);
        $row_edition = $this->Edition_model->get_row_edition($row_service_aspose->edition_id);
        $row_sinistre = $this->Sinistre_model->get_row_sinistre($row_edition->sinistre_id);
        $row_affaire = $this->Affaire_model->get_row_affaire($row_sinistre->affaire_id);

        // Test si le guid correspond
        if (!$row_service_aspose->guid == $guid) {
            log_message("ERROR", "Erreur, guid ne correspond pas");
            http_response_code(500);
            die("Erreur, guid ne correspond pas");
        }

        // Définition et tests des paths
        $path_actuel = $this->upload_path . $guid . ".pdf";
        $dossier_destination = BASEPATH . "../files/" . $row_affaire->code . "/sinistre" . $row_sinistre->num_chrono;
        $nom_fichier = $name = str_pad($row_service_aspose->type, 3, '0', STR_PAD_LEFT) . uniqid();
        $path_destination = $dossier_destination . "/" . $nom_fichier;
        $taille_fichier = null;

        // Le fichier est-il là ou on l'attend?
        if (!file_exists($path_actuel)) {
            log_message("ERROR", "Fichier introuvable");
            header("HTTP/1.0 500 Internal error");
            die("Fichier introuvable");
        } else {
            $taille_fichier = filesize($path_actuel);
        }

        // Le dossier de destination existe-t-il? si non on le créé
        if (!is_dir($dossier_destination)) {
            mkdir($dossier_destination, null, true);
            $dossier_destination = realpath($dossier_destination);
        }

        // On tente de déplacer le fichier
        try {
            $this->move_file($path_actuel, $path_destination);
        } catch (Exception $ex) {
            log_message("ERROR", "Erreur lors du déplacement du fichier");
            header("HTTP/1.0 500 Internal error");
            die("Erreur lors du déplacement du fichier");
        }


        // Si un document est déjà lié à l'édition, on le remplace :
        if ($row_edition->document_id > 0) {
            $document = $this->Documents_model->get_doc($row_edition->document_id);
            unlink($document->uri);
            $this->Documents_model->changer_uri($document->id, $path_destination);
        }
        // Sinon on le créé
        else {
            $document = new Document_dto();
            $document->date = new DateTime();
            $document->nom = $nom_fichier;
            $document->nom_public = $row_service_aspose->nom_fichier;
            $document->date_reception = new DateTime();
            $document->ext = "pdf";
            $document->num_type = (in_array($row_service_aspose->type, array(4, 8, 14, 18))) ? 109 : $row_service_aspose->type; // Les types 4,8,14 et 18 sont des CR réunion => type 109 dans l'appli
            $document->size = $taille_fichier;
            $document->uri = $path_destination;

            $document->id = $this->Documents_model->ajouter($document);
        }

        // On met à jour la table edition
        $etat_edition = 0;
        if ($row_service_aspose->preview == true) {
            $etat_edition = 1;
        } else {
            $etat_edition = 2;
        }
        $this->Edition_model->changer_etat($row_edition->id, $etat_edition);
        $this->Edition_model->set_document_id($row_edition->id, $document->id);

        // Si le document généré est le document final, on le lie au sinistre
        if ($etat_edition == 2) {
            $this->Documents_model->lier_document($document->id, null, $row_sinistre->id);

            // Et on le diffuse (pour les CR réunion, type aspose 4,8,14,18)
            if (in_array($row_edition->type_aspose, array(4, 8, 14, 18)) && $row_edition->diffusion != null) {
                $this->diffuser($row_edition->id);
            }
        }

        //Historisation : TODO
        // Si CR Reunion, diffusion par mail qui fera l'historisation
        if ($etat_edition == 2 && !in_array($row_edition->type_aspose, array(4, 8, 14, 18))) {
            $this->load->model('Historique_model');
            $historique = new Historique_dto();

            $historique->data = array(
                "cc" => null,
                "to" => $row_edition->diffusion,
                "mail" => null,
                "expediteur" => $row_edition->auteur,
                "objet" => $row_edition->nom_tmp,
                "attachements" => array($document->id));

            $historique->date = new DateTime();
            $historique->libelle = $row_edition->nom_tmp;
            $historique->num_chrono = $this->Historique_model->get_next_chrono($row_sinistre->id);
            $historique->sinistre_id = $row_sinistre->id;
            $historique->type = 2;
            $historique->type_mail = null;

            $this->Historique_model->add_row($historique);
        }
        //Suppression de la ligne service_aspose
        $this->Aspose_model->delete_row($row_service_aspose->id);
    }

    // "Safe move" si rename foire tente un copy+unlink, renvoie false si tout foire
    public function move_file($oldPath, $newPath) {
        if (!rename($oldPath, $newPath)) {
            if (copy($oldPath, $newPath)) {
                unlink($oldPath);
                return TRUE;
            }
            return FALSE;
        }
        return TRUE;
    }

    public function diffuser($id_edition) {

        $editions_repo = $this->doctrine->em->getRepository('Entities\Edition');
        $edition = $editions_repo->find($id_edition);

        $this->load->helper('mailer');

        $from = $edition->getMail_auteur();
        $to = $edition->getDiffusion();
        
        if (ENVIRONMENT == "demo") {
           $to = $from;
        }
        
        $pj = $edition->getDocument();
        $objet = "Diffusion de document : " . $pj->getNom_public();

        $data = $edition->getData();
        $data = json_decode($data);
        $date = $data->date_reunion;
        $redacteur = $data->redacteur;

        $view = $this->twig->render('mails/patrons/header.html.twig');
        $view .= $this->twig->render('mails/patrons/cr_reunion.html.twig', array('date' => $date, 'from' => $redacteur));
        $view .= $this->twig->render('mails/patrons/footer.html.twig');

        sendMail($view, $objet, $to, $from, null, $pj);

        $edition->setEtat(3);

        $statut = $edition->getStatut();

        $data_hist = array(
            "cc" => null,
            "to" => $to,
            "mail" => $view,
            "expediteur" => $edition->getAuteur(),
            "objet" => $objet,
            "attachements" => array($edition->getDocument()->getId()));

        $this->historiser($data_hist, $objet, $statut, 2);

        $this->doctrine->em->persist($edition);
        $this->doctrine->em->flush();
    }

    private function historiser($data, $libelle, $statut, $type) {
        $sinistre = $statut->getSinistre();
        $chrono = $this->doctrine->em->getRepository('Entities\Historique')->getNextChrono($type, $sinistre);

        $hist = new \Entities\Historique();
        $hist->setDate(new DateTime);
        $hist->setLibelle($libelle);
        $hist->setNum_chrono($chrono);
        $hist->setSinistre($sinistre);
        $hist->setStatut($statut);
        $hist->setType($type);
        $hist->setData($data);

        $this->doctrine->em->persist($hist);
        $this->doctrine->em->flush();
    }

}
