<?php

/**
 * @property Documents_model $Documents_model
 * @property APIQperf $apiqperf
 */
class Documents extends MY_Controller {
    /*
      Types gaïa :
      0 : Avis sur conception
      1 : RICT
      2 : BRED (titre dans rap_libelle)
      3 : CR visite
      4 : Liste récapitulative
      5 : Pré RFCT
      6 : RFCT
      7 : Attestation parasismique stade PC
      8 : Attestation parasismique finale
      9->19 : ATTHAND2
      15 : Rapport HAND2015
      16 : Attestation HAND2015
     */

    public $types_lies_affaire;

    public function __construct() {
        parent::__construct();
        $this->load->model('Documents_model');
        // nums des types de documents qui doivent être liés à l'affaire plutôt qu'au sinistre
        $this->types_lies_affaire = array(20, 0, 1, 2, 3, 4, 5, 6, 21, 7, 19, 110, 22, 111);
    }

    public function index() {
        $sinistre = $this->context->getSinistre();
        $affaire = $sinistre->getAffaire();


        $docs_sinistre = $this->Documents_model->get_docs_sinistre($sinistre->getId());
        $docs_affaire = $this->Documents_model->get_docs_affaire($affaire->getId());
        $docs_gaia = $this->Documents_model->get_docs_gaia($affaire->getId_g_affaire());
        $docs_qp = $this->Documents_model->get_docs_qp($affaire->getId_qp_contrat());

        $liste_categories = $this->Documents_model->get_liste_categories();
        $liste_types = $this->Documents_model->get_liste_types();
        $asso_cat_type = $this->Documents_model->get_asso_cat_types();

        $docs = array_merge($docs_sinistre, $docs_affaire, $docs_gaia, $docs_qp);

        $filtres = $this->session->userdata("filtres_documents");

        $this->twig->display("documents/list.html.twig", array("docs" => $docs, "types" => $liste_types, "categories" => $liste_categories, "asso_cat_type" => $asso_cat_type, "filtres" => $filtres, "topbar2" => "documents"));
    }

    public function changer_type($id, $num) {

        // Selon le type, le document peut être à rattacher au sinistre ou à l'affaire,
        // si c'est au sinistre, on passe son id en parametre, le model fait le reste
        $sinistre = $this->context->getSinistre();
        $affaire = $sinistre->getAffaire();
        if (in_array($num, $this->types_lies_affaire)) {
            $id_sinistre = null;
            $id_affaire = $affaire->getId();
        } else {
            $id_affaire = null;
            $id_sinistre = $sinistre->getId();
        }
        $this->Documents_model->changer_type_doc($id, $num, $id_sinistre, $id_affaire);
        die("ok");
    }

    public function enregistrer_filtres() {
        $this->session->set_userdata("filtres_documents", $_POST["data"]);
    }

    public function changer_date($id_doc) {
        $date = json_decode($_GET["date"]);
        $this->Documents_model->changer_date($id_doc, $date);
    }

    public function renommer($id_doc) {
        $nom = json_decode($_POST["nom"]);
        $this->Documents_model->renommer($id_doc, $nom);
        die("ok");
    }

    public function downloadQp($id_contrat, $id_doc) {
        $this->load->library("apiqperf");

        $infos_contrat = $this->apiqperf->get_infos_contrat($id_contrat);

        if ($infos_contrat["Convention_signée"] != null && $infos_contrat["Convention_signée"][0]["id"] == $id_doc) {
            $nom = $infos_contrat["Convention_signée"][0]["nom"];
        }
        if ($infos_contrat["Avenant_signé"] != null && $infos_contrat["Avenant_signé"][0]["id"] == $id_doc) {
            $nom = $infos_contrat["Avenant_signé"][0]["nom"];
        }

        $file = $this->apiqperf->get_document($id_doc);
        header("Content-type: text/plain");
        header("Content-Disposition: attachment; filename=" . $nom);
        echo $file;
    }

    public function download($id) {
        if (substr($id, 0, 1) == "g") {
            $id = str_replace("g", "", $id);
            $doc = $this->Documents_model->get_doc_gaia($id);
            $uri = $this->getByCurl($doc->uri);
        } else if (substr($id, 0, 1) == "q") {
            $id_doc = substr($id, 1, strpos($id, "c") - 1);
            $id_contrat = substr($id, strpos($id, "c") + 1);
            $this->downloadQp($id_contrat, $id_doc);
            return;
        } else {
            $doc = $this->Documents_model->get_doc($id);
            $uri = $doc->uri;
            $uri = FILES_PATH . "/" .$uri;
        }

        ignore_user_abort(true);
        set_time_limit(300); // disable the time limit for this script
        //Ni gaia ni qperf => doc local
        if ($fd = fopen($uri, "r")) {
            $fsize = filesize($uri);
            $path_parts = pathinfo($uri);

            header("Content-type: application/octet-stream");
            header("Content-Disposition: filename=\"" . $doc->nom_public . "." . $doc->ext . "\"");
            header("Content-length: $fsize");
            header("Cache-control: private");

            while (!feof($fd)) {
                $buffer = fread($fd, 1024);
                echo $buffer;
            }
            fclose($fd);
        }

        if ($doc->from_gaia || $doc->from_qp) {
            unlink($uri);
        }
    }

    public function getByCurl($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        $uniq = uniqid();
        $uri = FCPATH . "files/tmp/" . $uniq;
        file_put_contents($uri, $output);
        return $uri;
    }

    public function supprimer($id) {
        $doc = $this->Documents_model->get_doc($id);
        $this->Documents_model->supprimer_document($id);
        if (file_exists($doc->uri))
            unlink($doc->uri);
        die("ok");
    }

    public function acces_rapide() {

        $documents = false;
        $error = false;
        $result = array();
        if (isset($_POST['num_affaire'])) {
            $this->load->model('importDocuments');
            $this->load->model('importAffaire');

            $affaires = $this->importAffaire->checkInQualiperf($_POST['num_affaire']);
            if (!isset($affaires[0])) {
                $error = "Impossible de trouver l'affaire";
            } else {
                $affaire_tmp = $affaires[0];
                $affaire = $this->importAffaire->importQualiperf($affaire_tmp->code);
            }
            if (!isset($affaire) || !$affaire) {
                $error = "Erreur de récuperation de l'affaire";
            } else if ($affaire && !$affaire->getId_g_affaire()) {
                $error = "Affaire non trouvée dans Gaïa";
            } else {
                $documents = $this->importDocuments->getDocs($affaire, 'all');
                foreach ($documents as $doc) {
                    $type_g = $this->doctrine->em->getRepository('Entities\Type_docs')->findOneBy(array('num' => $doc["rap_type"]));
                    $date_d = date_create_from_format('Y-m-d H:i:s', $doc["rap_date"]);
                    $file = new Entities\File();
                    $file->setExt("pdf");
                    $file->setNom($doc["rap_filename"]);
                    $file->setUpload_date($date_d);
                    $file->setDate($date_d);
                    $file->setType($type_g);
                    $file->setUri("http://gaia.qualigroup.net/gaia/DownloadRapport?get=" . $doc["rap_publicKey"]);
                    $file->setId("g" . $doc["id_rapport"]);

                    $result[] = $file;
                }
            }
        }

        $this->twig->display('documents/acces_rapide.html.twig', array('error' => $error, 'documents' => $result));
    }

    public function upload() {

        ini_set('upload_max_filesize', '20M');

        $file = $_FILES["userfile"];

        $sinistre = $this->context->getSinistre();
        $affaire = $this->context->getAffaire();
        $date = date_create_from_format('Y-m-d', $_POST['date']);
        $nom_public = $_POST["nom"];

        $type_num = $_POST["type"];

        $date_reception = new DateTime();
        if (isset($_POST['date_reception']) && $_POST['date_reception'] != "")
            $date_reception = date_create_from_format('Y-m-d', $_POST['date_reception']);


        $uploaddir = $this->determiner_uri($affaire, $sinistre);

        $split = explode(".", $file['name']);
        $ext = end($split);
        $name = str_pad($type_num, 3, '0', STR_PAD_LEFT) . uniqid();

        $uri = $uploaddir . "/" . $name . '.' . $ext;

        if (move_uploaded_file($file['tmp_name'], FILES_PATH . "/" . $uri)) {

            $doc = new Document_dto();

            $doc->nom = $name;
            $doc->nom_public = $nom_public;
            $doc->date = $date;
            $doc->date_reception = $date_reception;
            $doc->ext = $ext;
            $doc->num_type = $type_num;
            $doc->size = $file["size"];
            $doc->uri = $uri;

            //Lié à l'affaire ou au sinistre
            if (in_array($type_num, $this->types_lies_affaire)) {
                $doc->affaire_id = $affaire->getId();
                $doc->sinistre_id = null;
            } else {
                $doc->affaire_id = null;
                $doc->sinistre_id = $sinistre->getId();
            }

            $this->Documents_model->ajouter($doc);
        }

        redirect(base_url("documents"));
    }

    public function determiner_uri($affaire, $sinistre = null) {
        $uploaddir = $affaire->getCode();
        if ($sinistre != null)
            $uploaddir .= "/sinistre" . $sinistre->getNum_chrono();
        if (!is_dir(FILES_PATH . "/" . $uploaddir)) {
            mkdir(FILES_PATH . "/" . $uploaddir, null, true);
        }

        return $uploaddir;
    }

    //poreviex editions
    public function preview($id, $byEdition = false) {
        if ($byEdition != false) {
            $edRepo = $this->doctrine->em->getRepository('Entities\Edition');
            $edition = $edRepo->find($id);
            $id = $edition->getDocument()->getId();
        }

        $fileR = $this->doctrine->em->getRepository('Entities\File');
        $file = $fileR->find($id);
        if (!$file) {
            echo 'erreur';     //exceptionme
            return;
        } else {
            $data = $file->getUri();
            var_dump($data);
            ignore_user_abort(true);
            set_time_limit(0); // disable the time limit for this script
            if ($fd = fopen($data, "r")) {
                $fsize = filesize($data);
                $path_parts = pathinfo($data);
                header("Content-type: application/pdf");
                header("Content-Disposition: inline; filename=\"" . $file->getNom() . "\"");
                header("Content-length: $fsize");
                header("Cache-control: private");
                while (!feof($fd)) {
                    $buffer = fread($fd, 2048);
                    echo $buffer;
                }
                fclose($fd);
            }
            exit;
        }
    }

    // test interfacage avec docs pj mails..
    // ca marche, mais il faudra refaire cette partie en + propre

    public function getTypesList() {
        $types = $this->Documents_model->get_liste_types();
        //var_dump($types);
        $r = array();
        foreach ($types as $type) {
            $r[] = array($type["num"], $type["type"]);
        }

        echo json_encode($r);
    }

    public function getType($num) {
        $type = $this->Documents_model->get_type($num);
        echo @$type->type;
    }

    public function filtrer() {

        $sinistre = $this->context->getSinistre();

        $recherche = @$_POST["recherche"];
        $type = @$_POST["type"];

        $docs = $this->Documents_model->get_all_docs($sinistre);
//        echo "<!--";
//        var_dump($docs);
//        echo "-->";

        $results = array();

        foreach ($docs as $doc) {
            if ($type) {
                if ($doc->num_type != $type) {
                    continue;
                }
            }
            if ($recherche) {
                if (strstr(strtolower($doc->nom_public), strtolower($recherche)) == false)
                    continue;
            }

            $doc->type = @$this->Documents_model->get_type($doc->num_type)->type;
            $results[$doc->id] = $doc;
        }

        echo json_encode($results);
    }

    public function get_documents_qp($id_contrat) {
        ini_set("display_errors", true);
        error_reporting(E_ALL);
        $this->load->library('apiqperf');
        $infos_contrat = $this->apiqperf->get_infos_contrat(786837);
        $file = $this->apiqperf->get_document(182154);

        return $file;
    }

}
