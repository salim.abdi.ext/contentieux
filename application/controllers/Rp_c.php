<?php

class Rp_c extends MY_Controller {

    public function check_access() {
        $user = $this->context->getUser();
        if ($user->role != 5 && $user->role != 10)
            redirect(base_url());
        return true;
    }

    public function index($id = null) {
        $edit = $this->check_access();
        $affaire = $this->context->getAffaire();
        $this->context->setSinistre(null);

        if ($id != null) {
            $rp = $this->doctrine->em->getRepository('Entities\Rp')->find($id);
            $expert = $rp->getExpert();
            $adresse = $expert->getAdresse();
            $topbar2 = 1;
            if (isset($rp)) {
                $this->context->setRp($rp);
                $this->context->setStatut("RP");
            }
        } else {
            $this->context->setStatut("NRP");
            $rp = new Entities\Rp();
            $expert = new Entities\Contact();
            $adresse = new Entities\Adresse();
            $chrono = $this->doctrine->em->getRepository('Entities\Rp')->get_next_chrono($affaire);
            $rp->setNum_chrono($chrono);
            $topbar2 = 2;
            
            $this->context->setRp(null);
        }
        if (isset($_POST['date_requete'])) {
            $rp->setDate_requete(date_create_from_format("Y-m-d", $_POST['date_requete']));
            $rp->setRef_asqua($_POST['ref_asqua']);
            $rp->setRef_expert($_POST['ref_expert']);

            $expert->setType(1);
            $expert->setEmail($_POST['expert_email_contact']);
            $expert->setNom($_POST['expert_nom_contact']);
            $expert->setPrenom($_POST['expert_prenom_contact']);
            $expert->setTelephone($_POST['expert_telephone_contact']);

            $adresse->setRue1($_POST['rue1']);
            $adresse->setRue2($_POST['rue2']);
            $adresse->setRue3($_POST['rue3']);
            $adresse->setCp($_POST['code_postal']);
            $adresse->setCommune($_POST['commune']);

            $expert->setAdresse($adresse);
            $rp->setExpert($expert);
            $affaire->addRp($rp);
            $rp->setAffaire($affaire);

            $this->doctrine->em->persist($adresse);
            $this->doctrine->em->persist($expert);
            $this->doctrine->em->persist($rp);
            $this->doctrine->em->persist($affaire);
            $this->doctrine->em->flush();
            $this->context->setRp($rp);
        }

        $this->twig->display('sinistre/rp.html.twig', array('rp' => $rp, 'edit' => $edit, 'topbar2' => $topbar2));
    }

    public function mails() {
        
    }

}
