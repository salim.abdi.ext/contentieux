<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Log
 *
 * @author martin.le-barbenchon
 */
class Log extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $user = $this->context->getUser();

        if ($user->role != 10 && $user->role != 8) {
            redirect(base_url());
            die();
        }
    }

    public function get_list() {

        $logs = scandir(APPPATH . "/logs");

        foreach ($logs as $log) {
            echo "<a href='display/" . $log . "'>$log</a><br>";
        }
    }

    public function display($log) {

        $file = file(APPPATH . "/logs/" . $log);

        foreach ($file as $ln) {
            echo $ln . "<br>";
        }
    }

}
