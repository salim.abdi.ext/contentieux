<?php

use Entities\Affaire;

class Recherche extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Recherche une affaire dans la base contentieux
     *
     * Renvoie la liste des resultats trouvés.
     *
     * @param String $num_affaire
     * @param String $recherche_libre
     * @return Array affaires trouvées dans la bdd contentieux
     */
    public function index()
    {

        $user = $this->session->userdata('user');

//        error_reporting(E_ALL);
//        ini_set('display_errors', 1);

        $recherche_libre = (isset($_POST["recherche_affaire_libre"])) ? str_replace("'", "''", $_POST["recherche_affaire_libre"]) : null;
        $agence = (isset($_POST["agence"])) ? $_POST["agence"] : null;

        $statuts = array(
            "do" => isset($_POST['statut_do']),
            "av1" => isset($_POST['statut_av1']),
            "jud" => isset($_POST['statut_jud']),
            "autre" => isset($_POST['statut_autre']),
            "rp" => isset($_POST['statut_rp'])
        );
        $allfalse = true;
        foreach ($statuts as $statut)
        {
            if ($statut)
                $allfalse = false;
        }
        if ($allfalse)
            $statuts = null;

        $statut_affaire_1 = (@$_POST["statut_affaire_1"] == 'on'); // archivée

        $oldsearch = $this->context->getSearch();

        $search = array(
            'recherche' => $recherche_libre,
            'agence' => $agence,
            'statuts' => $statuts,
            'num_affaire' => @$oldsearch['num_affaire'],
            'num_affaire_autocomp' => @$oldsearch['num_affaire_autocomp'],
            'statut_affaire_1' => $statut_affaire_1,
        );
        echo "<!--";
        var_dump($search);
        echo "-->";
        $this->context->setSearch($search);

        $em = $this->doctrine->em;
        $affaireRepo = $em->getRepository("Entities\Affaire");

        $affaires = $affaireRepo->search(null, $recherche_libre, $user, $agence, $statuts, $statut_affaire_1);

        $this->load->model('Utils');
        $agences = $this->Utils->getAgences($user);

        $this->twig->display("global/accueil.html.twig", array('agences' => $agences, 'affaires' => $affaires, 'topbar2' => 1));
    }

    public function quickAccess($code)
    {
        ini_set("display_errors", true);
        error_reporting(E_ALL);
        $user = $this->session->userdata('user');
        $em = $this->doctrine->em;
        $affaireRepo = $em->getRepository("Entities\Affaire");
        $affaire = $affaireRepo->findOneBy(array("code" => $code));
        if ($affaire)
        {
            $this->context->setAffaire($affaire);

            $this->load->library('context');
            $search = $this->context->getSearch();
            $search['num_affaire'] = $code;
            $search['num_affaire_autocomp'] = $affaire->getCode() . " " . $affaire->getLibelle();

            $this->context->setSearch($search);

            redirect("/affaire_c");
            return;
        } else
        {
            redirect("/");
        }
    }

}
