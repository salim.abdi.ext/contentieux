<?php

class Mails_amiable extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->context->setStatut('amiable');
        $this->load->helper('mailer');
    }

    public function index() {
        $this->declaration();
    }

    public function declaration() {

        $user = $this->context->getUser();
        $dest = getMailDest(15);
        $patron = 'amiable/declaration_asqua.html.twig';
        $sinistre = $this->context->getSinistre();

        if (!isset($_POST['objet'])) {
            $data_preview = array();
            $data_preview['sinistre'] = $sinistre;
            $data_preview['from'] = array('email' => $user->email, 'nom' => $user->prenom . " " . $user->nom, "role" => $user->role_libelle);
            $preview = getMailPreview($patron, $data_preview);

            $dest['destinataires'] = str_replace(",", ", ", $dest['destinataires']);
            $dest['CC'] = str_replace(",", ", ", $dest['CC']);
            $data = array();
            $data['mail_content'] = $preview;
            $data['title'] = "Declaration ASQUA";
            $data = array_merge($data, $dest);

            $alreadysent = $this->doctrine->em->getRepository('Entities\Historique')->findOneBy(array("sinistre" => $sinistre, "type_mail" => 15));
            if ($alreadysent)
                $data['alreadysent'] = $alreadysent;

            $data['pj'] = true;
            $data['active'] = 2;
            $data['topbar2'] = 2;
            $this->twig->display('mails/amiable.html.twig', $data);
        } else {
            $objet = $_POST['objet'];
            $to = $dest['destinataires'];
            $cc = $dest['CC'];
            $from = $user->email;
            $files = null;
            $docs = explode(",", $_POST['docs']);
            foreach ($docs as $doc) {
                $files[] = getFile($doc);
            }
            $_POST['sinistre'] = $sinistre;
            $_POST['from'] = array('email' => $user->email, 'nom' => $user->prenom . " " . $user->nom, "role" => $user->role_libelle);
            $_POST['docs'] = $files;
            $token_asqua = uniqid();
            $_POST['token_asqua'] = $token_asqua;
            $sinistre->setToken_asqua($token_asqua);
            $this->doctrine->em->persist($sinistre);
            $this->doctrine->em->flush();
            $mail = makeMail($patron, $_POST);
            try {
                sendMail($mail, $objet, $to, $from, $cc, $files);
                saveMail($sinistre->getAmiable(), "Déclaration ASQUA Amiable", $mail, $objet, $to, $from, $cc, $files, 15);
                if (ENVIRONMENT == "demo") {
                    $this->load->library('asqua_lib');
                    $this->asqua_lib->synchro_demo($sinistre);
                }
            } catch (Exception $e) {
                echo '<script>alert("Une erreur est survenue, l\'email n\'a pas été envoyé.")</script>';
                redirect('statuts/amiable/mails_amiable/declaration');
                return;
            }
            echo '<script>alert("Email envoyé")</script>';
            redirect('statuts/amiable/mails_amiable/declaration');
            return;
        }
    }

    public function docs2expert() {

        $user = $this->context->getUser();
        $dest = getMailDest(16);
        $patron = 'amiable/docs2expert.html.twig';
        $sinistre = $this->context->getSinistre();

        if (!$sinistre->getAmiable()->getExpert_compagnie()->getEmail()) {
            alert("L'adresse de l'expert cie n'a pas été renseignée");
            redirect(last_url());
        }

        $mail_expert = $sinistre->getAmiable()->getExpert_compagnie()->getEmail();
        if (!$mail_expert)
            $mail_expert = $sinistre->getAmiable()->getCabinet_expert_compagnie()->getEmail();

        if (!$mail_expert) {
            alert("L'adresse mail de l'expert est manquante");
            redirect(last_url());
        }
        $dest['destinataires'] = $mail_expert;

        if (!isset($_POST['objet'])) {
            $data_preview = array();
            $data_preview['sinistre'] = $sinistre;
            $data_preview['from'] = array('email' => $user->email, 'nom' => $user->prenom . " " . $user->nom, "role" => $user->role_libelle);
            $preview = getMailPreview($patron, $data_preview);
            $dest['CC'] = str_replace(",", ", ", $dest['CC']);
            $data = array();
            $data['mail_content'] = $preview;
            $data['title'] = "Transmission docs expert";
            $data = array_merge($data, $dest);

            $alreadysent = $this->doctrine->em->getRepository('Entities\Historique')->findOneBy(array("sinistre" => $sinistre, "type_mail" => 16));
            if ($alreadysent)
                $data['alreadysent'] = $alreadysent;

            $data['pj'] = true;
            $data['active'] = 3;
            $data['topbar2'] = 2;
            $this->twig->display('mails/amiable.html.twig', $data);
        } else {
            $objet = $_POST['objet'];
            $to = $dest['destinataires'];
            $cc = $dest['CC'];
            $from = $user->email;
            $files = null;
            $files = splitDocs($_POST['docs']);
            $_POST['sinistre'] = $sinistre;
            $_POST['from'] = array('email' => $user->email, 'nom' => $user->prenom . " " . $user->nom, "role" => $user->role_libelle);
            $token_asqua = uniqid();
            $_POST['token_asqua'] = $token_asqua;
            $sinistre->setToken_asqua($token_asqua);
            $this->doctrine->em->persist($sinistre);
            $this->doctrine->em->flush();
            $mail = makeMail($patron, $_POST);
            try {
                sendMail($mail, $objet, $to, $from, $cc, $files);
                saveMail($sinistre->getAmiable(), "Transmission docs expert", $mail, $objet, $to, $from, $cc, $files, 16);
            } catch (Exception $e) {
                echo '<script>alert("Une erreur est survenue, l\'email n\'a pas été envoyé.")</script>';
                redirect('statuts/amiable/mails_amiable/declaration');
                return;
            }
            echo '<script>alert("Email envoyé")</script>';
            redirect('statuts/amiable/mails_amiable/declaration');
            return;
        }
    }

    public function docs2rcr() {

        $user = $this->context->getUser();
        $dest = getMailDest(17);
        $patron = 'amiable/docs2rcr.html.twig';
        $sinistre = $this->context->getSinistre();

        if (!isset($_POST['objet'])) {
            $data_preview = array();
            $data_preview['sinistre'] = $sinistre;
            $data_preview['from'] = array('email' => $user->email, 'nom' => $user->prenom . " " . $user->nom, "role" => $user->role_libelle);
            $preview = getMailPreview($patron, $data_preview);

            $dest['destinataires'] = str_replace(",", ", ", $dest['destinataires']);
            $dest['CC'] = str_replace(",", ", ", $dest['CC']);
            $data = array();
            $data['mail_content'] = $preview;
            $data['title'] = "Transmission docs RCR";
            $data = array_merge($data, $dest);

            $alreadysent = $this->doctrine->em->getRepository('Entities\Historique')->findOneBy(array("sinistre" => $sinistre, "type_mail" => 17));
            if ($alreadysent)
                $data['alreadysent'] = $alreadysent;

            $data['pj'] = true;
            $data['active'] = 4;
            $data['topbar2'] = 2;
            $this->twig->display('mails/amiable.html.twig', $data);
        } else {
            $objet = $_POST['objet'];
            $to = $dest['destinataires'];
            $cc = $dest['CC'];
            $from = $user->email;
            $files = null;
            $_POST['docs'] = splitDocs($_POST['docs']);
            $_POST['sinistre'] = $sinistre;
            $_POST['from'] = array('email' => $user->email, 'nom' => $user->prenom . " " . $user->nom, "role" => $user->role_libelle);
            $token_asqua = uniqid();
            $_POST['token_asqua'] = $token_asqua;
            $sinistre->setToken_asqua($token_asqua);
            $this->doctrine->em->persist($sinistre);
            $this->doctrine->em->flush();
            $mail = makeMail($patron, $_POST);
            try {
                sendMail($mail, $objet, $to, $from, $cc, $files);
                saveMail($sinistre->getAmiable(), "Transmission docs RCR", $mail, $objet, $to, $from, $cc, $files, 17);
            } catch (Exception $e) {
                echo '<script>alert("Une erreur est survenue, l\'email n\'a pas été envoyé.")</script>';
                redirect('statuts/amiable/mails_amiable/declaration');
                return;
            }
            echo '<script>alert("Email envoyé")</script>';
            redirect('statuts/amiable/mails_amiable/declaration');
            return;
        }
    }

}
