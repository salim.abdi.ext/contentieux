<?php

use Entities\Statut_DO;
use Entities\Adresse;
use Entities\Cabinet_expert;
use Entities\Expert;

class Fiche_do extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->context->setStatut('DO');
    }

    public function index() {
        $sinistre = $this->context->getSinistre();
        
        if (!isset($_POST['cabinet_expert_nom_contact'])) {
            $this->twig->display('sinistre/do.html.twig', array('topbar2' => 1));
            return;
        }

        $do = $sinistre->getDO();
        if (!$do instanceof Statut_DO)
            $do = new Statut_DO();

        $do = $this->mapToEntity($_POST, $do);

        $sinistre->setDO($do);
        $this->doctrine->em->persist($sinistre);
        $this->doctrine->em->flush();

        $this->twig->display('sinistre/do.html.twig', array('topbar2' => 1));
    }

    public function mapToEntity($post, Statut_DO $do) {
        if ($post['date_premiere_reunion_expertise'] != "")
            $do->setDate_premiere_reunion_expertise(date_create_from_format("Y-m-d", $post['date_premiere_reunion_expertise']));
        else
            $do->setDate_premiere_reunion_expertise(null);

        $cabinet_expert = $do->getCabinet_expert();
        if (!$cabinet_expert instanceof Entities\Contact) {
            $cabinet_expert = new Entities\Contact();
        }

        $cabinet_expert->setNom($post['cabinet_expert_nom_contact']);
        $cabinet_expert->setEmail($post['cabinet_expert_email_contact']);
        $cabinet_expert->setTelephone($post['cabinet_expert_telephone_contact']);

        $do->setCabinet_expert($cabinet_expert);


        $adresse_expert = $cabinet_expert->getAdresse();
        if (!$adresse_expert instanceof Adresse) {
            $adresse_expert = new Adresse();
        }

        $adresse_expert->setRue1($post["expert_rue1"]);
        $adresse_expert->setRue2($post["expert_rue2"]);
        $adresse_expert->setRue3($post["expert_rue3"]);
        $adresse_expert->setCp($post["expert_code_postal"]);
        $adresse_expert->setCommune($post["expert_commune"]);


        $expert = $do->getContact_expert();
        if (!$expert instanceof Entities\Contact) {
            $expert = new Entities\Contact();
        }
        $expert->setNom($post["expert_nom_contact"]);
        $expert->setPrenom($post["expert_prenom_contact"]);
        $expert->setTelephone($post["expert_telephone_contact"]);
        $expert->setEmail($post["expert_email_contact"]);

        $do->setContact_expert($expert);

        $do->setRef_dossier_expert($post["ref_dossier_expert"]);




        return $do;
    }

}
