<?php

class Grille_analyse_av1 extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('Context');
    }

    public function index() {
        $sinistre = $this->context->getSinistre();
        $statut = $sinistre->getAV1();
        $file_r = $this->doctrine->em->getRepository('Entities\File');

        $rapports_expert_av1 = $file_r->getRapports_expert($statut);

        $this->twig->display("documents/grille_analyse_main.html.twig", array('statut' => 'av1', 'topbar3' => "courrier/av1/topbar_courrier_av1.html.twig", 'topbar2' => 3, 'active' => 7, 'rapports' => $rapports_expert_av1));
    }

    public function edit($id_doc, $diffuser = false) {

        $file_r = $this->doctrine->em->getRepository('Entities\File');
        $file = $file_r->find($id_doc);
        $grilleanalyse = null;
        $grilleanalyse = $file->getGrilleAnalyse();
        $analyses = array();
        $auteur = $this->context->getUser();
        $auteur_fullname = $auteur->prenom . " " . $auteur->nom;

        if ($grilleanalyse) {
            $grilleanalyse->setConclusion_contestee(false);
            $grilleanalyse->setAvis_rcr(false);
        }

        if (isset($_POST['implication_qc'])) {

            if ($grilleanalyse == null) {
                $grilleanalyse = new Entities\GrilleAnalyse();
                $file->setGrilleAnalyse($grilleanalyse);
                $grilleanalyse->setDoc($file);
                $grilleanalyse->setDiffusee(null);
            } else {
                $analyses = $grilleanalyse->getAnalyses();
            }
            $i = 1;
            if ($analyses) {
                foreach ($analyses as $row) {
                    if (!isset($_POST["dommage_$i"]) && !isset($_POST["conteste_$i"]) && !isset($_POST["arguments_interne_$i"]) && !isset($_POST["reponse_expert_$i"]) && !isset($_POST["avis_rcr_$i"])) {
                        unset($analyses[$i]);
                    } else {
                        $analyses[$i]['conteste'] = false;
                        $analyses[$i]['avis_rcr'] = false;
                    }
                    $i++;
                }
                $grilleanalyse->setAnalyses($analyses);
            }

            $grilleanalyse->setAuteur($auteur_fullname);
            $grilleanalyse->setMail_auteur($auteur->email);

            $grilleanalyse->setImplication_qc($_POST['implication_qc'] == "oui");
            @$grilleanalyse->setCommentaire_interne($_POST['commentaire_interne']);

            if ($grilleanalyse->getImplication_qc() == true)
                $grilleanalyse->setCaractere_isole($_POST['caractere_isole'] == "oui");
            else {
                $grilleanalyse->setCaractere_isole(null);
                if ($diffuser)
                    $this->sendMail1($grilleanalyse);
            }

            if ($grilleanalyse->getImplication_qc() == true && $grilleanalyse->getCaractere_isole() === false) {
                $grilleanalyse->setConclusion_qc($_POST['conclusion_qc']);
                $grilleanalyse->setConclusion_expert($_POST['conclusion_expert']);

                foreach ($_POST as $k => $i) {
                    if ($k == 'implication_qc' && $k == 'caractere_isole' && $k == 'conclusion')
                        continue;

                    if (substr($k, 0, 7) == "dommage") {
                        $analyses[substr($k, -1)]['dommage'] = $i;
                    }
                    if (substr($k, 0, 8) == "conteste") {
                        $analyses[substr($k, -1)]['conteste'] = $i;
                        if ($i == 'on')
                            $grilleanalyse->setConclusion_contestee(true);
                    }
                    if (substr($k, 0, 17) == "arguments_interne") {
                        $analyses[substr($k, -1)]['arguments_interne'] = $i;
                    }
                    if (substr($k, 0, 14) == "reponse_expert") {
                        $analyses[substr($k, -1)]['reponse_expert'] = $i;
                    }
                    if (substr($k, 0, 8) == "avis_rcr") {
                        $analyses[substr($k, -1)]['avis_rcr'] = $i;
                        if ($i == "on")
                            $grilleanalyse->setAvis_rcr(true);
                    }
                    if (substr($k, 0, 3) == "doc") {
                        $analyses[substr($k, -1)]['file'] = $i;
                    }
                    if (substr($k, 0, 7) == "nom_doc") {
                        $analyses[substr($k, -1)]['file_nom'] = $i;
                    }
                }
                $grilleanalyse->setAnalyses($analyses);
            } elseif ($grilleanalyse->getImplication_qc() == true) {
                $grilleanalyse->setAnalyses(null);
                $grilleanalyse->setConclusion_expert(null);
                $grilleanalyse->setConclusion_qc(null);
                if ($diffuser)
                    $this->sendMail2($grilleanalyse);
            }

            if ($analyses)
                $grilleanalyse->setAnalyses($analyses);

            $this->doctrine->em->persist($grilleanalyse);
            $this->doctrine->em->flush();

            if ($grilleanalyse->getCaractere_isole() == false && $grilleanalyse->getImplication_qc() == true && $grilleanalyse->getConclusion_contestee() == true) {
                if ($diffuser == true) {
                    if ($grilleanalyse->getAvis_rcr() == false)
                        $this->sendMail3($grilleanalyse);
                    else {
                        $grilleanalyse->setDifferee(new DateTime('+4days'));
                        $this->doctrine->em->persist($grilleanalyse);
                        $this->doctrine->em->flush();
                    }
                }
            }
        }

                if (isset($_POST['responsabilite_non_retenue']) && $_POST['responsabilite_non_retenue'] == "oui") {

            $sinistre = $this->context->getSinistre();
            $sinistre->setDate_fermeture(new DateTime);
            $statut = $grilleanalyse->getDoc()->getStatut();
            $user = $this->context->getUser();
            $username = $user->prenom . ' ' . $user->nom;

            $histRepo = $this->doctrine->em->getRepository('Entities\Historique');
            $nextChrono = $histRepo->getNextChrono(3, $sinistre);

            $historique = new Entities\Historique;
            $historique->setLibelle("Cloture du sinistre");
            $historique->setNum_chrono($nextChrono);
            $historique->setSinistre($sinistre);
            $historique->setData(array('expediteur' => $username));

            $this->doctrine->em->persist($historique);
            $this->doctrine->em->persist($sinistre);
            $this->doctrine->em->flush();
        }

        $types_docs = $this->doctrine->em->getRepository('Entities\Type_docs')->findAll();
        $categories_docs = $this->doctrine->em->getRepository('Entities\Categories_docs')->findAll();
        $this->twig->display('mails/av1/grilleAnalyseAv1.html.twig', array('user' => $auteur, 'grille' => $grilleanalyse, 'active' => 7, 'topbar2' => 3, 'types' => $types_docs, 'categories' => $categories_docs));
    }

    public function sendMail1(Entities\GrilleAnalyse $grille, $preview = false) {
        $this->load->helper('mailer');
        if ($preview)
            return getMailPreview('grille_analyse/mail1.twig', array('grille' => $grille, 'sinistre' => $this->context->getSinistre()));
        $mail = makeMail('grille_analyse/mail1.twig', array('grille' => $grille, 'sinistre' => $this->context->getSinistre()));
        $dests = getMailDest(19);
        $cc = $dests['CC'];
        $mail_expert = $grille->getDoc()->getStatut()->getContact_expert()->getEmail();
        if (!$mail_expert)
            $mail_expert = $grille->getDoc()->getStatut()->getCabinet_expert()->getEmail();
        sendMail($mail, "Analyse du rapport expert", $mail_expert, $grille->getMail_auteur(), $cc, $grille->getDoc());
        saveMail($grille->getDoc()->getStatut(), "Analyse de rapport expert AV1", $mail, "Analyse du rapport expert", $mail_expert, $grille->getMail_auteur(), $cc, $grille->getDoc(), 19);

        $grille->setDiffusee(new DateTime);
        $this->doctrine->em->persist($grille);
        $this->doctrine->em->flush();
    }

    public function sendMail2(Entities\GrilleAnalyse $grille, $preview = false) {
        $this->load->helper('mailer');
        if ($preview)
            return getMailPreview('grille_analyse/mail2.twig', array('grille' => $grille, 'sinistre' => $this->context->getSinistre()));
        $mail = makeMail('grille_analyse/mail2.twig', array('grille' => $grille, 'sinistre' => $this->context->getSinistre()));
        $dests = getMailDest(19);
        $cc = $dests['CC'];
        $mail_expert = $grille->getDoc()->getStatut()->getContact_expert()->getEmail();
        if (!$mail_expert)
            $mail_expert = $grille->getDoc()->getStatut()->getCabinet_expert()->getEmail();
        sendMail($mail, "Analyse du rapport expert", $mail_expert, $grille->getMail_auteur(), $cc, $grille->getDoc());
        saveMail($grille->getDoc()->getStatut(), "Analyse de rapport expert AV1", $mail, "Analyse du rapport expert", $mail_expert, $grille->getMail_auteur(), $cc, $files, 19);

        $grille->setDiffusee(new DateTime);
        $this->doctrine->em->persist($grille);
        $this->doctrine->em->flush();
    }

    public function sendMail3(Entities\GrilleAnalyse $grille, $preview = false) {
        $this->load->helper('mailer');
        $mail = makeMail('grille_analyse/mail3.twig', array('grille' => $grille, 'sinistre' => $this->context->getSinistre()));
        $dests = getMailDest(19);
        $cc = $dests['CC'];

        $this->filesToEntity($grille);

        $analyses = $grille->getAnalyses();
        $files = array($grille->getDoc());
        foreach ($analyses as $row) {
            if (isset($row['file']))
                $files[] = $row['file'];
        }
        $mail_expert = $grille->getDoc()->getStatut()->getContact_expert()->getEmail();
        if (!$mail_expert)
            $mail_expert = $grille->getDoc()->getStatut()->getCabinet_expert()->getEmail();

        if ($preview)
            return getMailPreview('grille_analyse/mail3.twig', array('grille' => $grille, 'sinistre' => $this->context->getSinistre()), $files);
        sendMail($mail, "Analyse du rapport expert", $mail_expert, $grille->getMail_auteur(), $cc, $files);
        saveMail($grille->getDoc()->getStatut(), "Analyse de rapport expert AV1", $mail, "Analyse du rapport expert", $mail_expert, $grille->getMail_auteur(), $cc, $grille->getDoc(), 19);

        $grille->setDiffusee(new DateTime('now'));
        $this->entityToFile($grille);
        $this->doctrine->em->persist($grille);
        $this->doctrine->em->flush();
    }

    public function previsualiser($id_doc) {
        $file_r = $this->doctrine->em->getRepository('Entities\File');
        $file = $file_r->find($id_doc);
        $grilleanalyse = null;
        $grilleanalyse = $file->getGrilleAnalyse();

        if ($grilleanalyse->getImplication_qc() == false)
            echo $this->sendMail1($grilleanalyse, true);
        if ($grilleanalyse->getCaractere_isole() == true)
            echo $this->sendMail2($grilleanalyse, true);
        if ($grilleanalyse->getConclusion_contestee() == true)
            echo $this->sendMail3($grilleanalyse, true);
    }

    private function filesToEntity(&$grille) {
        $file_r = $this->doctrine->em->getRepository('Entities\File');
        $analyses = $grille->getAnalyses();

        foreach ($analyses as &$elem) {
            if (isset($elem['file'])) {
                if (substr($elem['file'], 0, 1) != "q" && substr($elem['file'], 0, 1) != "g")
                    $elem['file'] = $file_r->find($elem['file']);

                else {
                    $file_tmp = new Entities\File();
                    $file_tmp->setId($elem['file']);
                    $file_tmp->setNom($elem['file_nom']);
                    $elem['file'] = $file_tmp;
                }
            }
        }

        $grille->setAnalyses($analyses);
    }

    private function entityToFile(&$grille) {
        $file_r = $this->doctrine->em->getRepository('Entities\File');
        $analyses = $grille->getAnalyses();

        foreach ($analyses as &$elem) {
            if (isset($elem['file'])) {
                $elem['file'] = $elem['file']->getId();
            }
        }

        $grille->setAnalyses($analyses);
    }

}
