<?php

class Mails_av1 extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->context->setStatut('AV1');
        $this->load->helper('mailer');
    }

    public function index() {
        $this->declaration();
    }

    public function declaration() {

        $user = $this->context->getUser();
        $sinistre = $this->context->getSinistre();
        $dest = getMailDest(5);
        $type = 5;
        $patron = 'av1/declaration_asqua.html.twig';



        if (!isset($_POST['objet'])) {
            $data_preview = array();
            $data_preview['sinistre'] = $sinistre;
            $data_preview['from'] = array('email' => $user->email, 'nom' => $user->prenom . " " . $user->nom, "role" => $user->role_libelle);
            $preview = getMailPreview($patron, $data_preview);
            $dest['destinataires'] = str_replace(",", ", ", $dest['destinataires']);
            $dest['CC'] = str_replace(",", ", ", $dest['CC']);
            $data = array();
            if ($sinistre->getStatut_initial() != 1)
                $data['pj'] = true;
            $data['mail_content'] = $preview;
            $data['title'] = "Declaration ASQUA";
            $data = array_merge($data, $dest);

            $alreadysent = $this->doctrine->em->getRepository('Entities\Historique')->findOneBy(array("sinistre" => $sinistre, "type_mail" => $type));
            if ($alreadysent)
                $data['alreadysent'] = $alreadysent;

            $data['active'] = 2;
            $data['topbar2'] = 2;
            $this->twig->display('mails/av1.html.twig', $data);
        } else {

            if ($sinistre->getStatut_initial() != 1) {
                $token_asqua = uniqid();
                $sinistre->setToken_asqua($token_asqua);
                $this->doctrine->em->persist($sinistre);
                $this->doctrine->em->flush();
                $_POST['token_asqua'] = $token_asqua;
            }
            $objet = $_POST['objet'];
            $to = $dest['destinataires'];
            $cc = $dest['CC'];
            $from = $user->email;
            $files = null;
            if (isset($_POST['docs']))
                $files = splitDocs($_POST['docs']);
            $_POST['sinistre'] = $sinistre;
            $_POST['from'] = array('email' => $user->email, 'nom' => $user->prenom . " " . $user->nom, "role" => $user->role_libelle);
            $mail = makeMail($patron, $_POST);
            try {
                sendMail($mail, $objet, $to, $from, $cc, $files);
                saveMail($sinistre->getAv1(), "Déclaration ASQUA Av1", $mail, $objet, $to, $from, $cc, $files, $type);
                if (ENVIRONMENT == "demo") {
                    $this->load->library('asqua_lib');
                    $this->asqua_lib->synchro_demo($sinistre);
                }
            } catch (Exception $e) {
                echo '<script>alert("Une erreur est survenue, l\'email n\'a pas été envoyé.")</script>';
                redirect('statuts/av1/mails_av1/declaration');
                return;
            }
            echo '<script>alert("Email envoyé")</script>';
            redirect('statuts/av1/mails_av1/declaration');
            return;
        }
    }

    public function docs2expert() {

        $user = $this->context->getUser();
        $sinistre = $this->context->getSinistre();

//        if (!$sinistre->is_synchro()) {
//            alert("Le sinistre doit être synchronisé avec Asqua pour obtenir l'adresse de l'expert");
//            redirect(last_url());
//        }
        if (!$sinistre->getAv1()->getCabinet_expert_compagnie()->getEmail() && !$sinistre->getAv1()->getContact_expert_compagnie()->getEmail()) {
            alert("L'adresse du cabinet expert cie ou du contact est manquante");
            redirect(last_url());
        }

        $dest = getMailDest(7);
        $patron = 'av1/docs2expert.html.twig';
        $mail_expert = $sinistre->getAv1()->getContact_expert_compagnie()->getEmail();
        if (!$mail_expert)
            $mail_expert = $sinistre->getAv1()->getCabinet_expert_compagnie()->getEmail();
        $dest['destinataires'] = $mail_expert;

        if (!isset($_POST['objet'])) {
            $data_preview = array();
            $data_preview['sinistre'] = $sinistre;
            $data_preview['from'] = array('email' => $user->email, 'nom' => $user->prenom . " " . $user->nom, "role" => $user->role_libelle);
            $preview = getMailPreview($patron, $data_preview);
            $dest['CC'] = str_replace(",", ", ", $dest['CC']);
            $data = array();
            $data['mail_content'] = $preview;
            $data['title'] = "Transmission documents expert cie";
            $data = array_merge($data, $dest);

            $data['pj'] = true;

            $alreadysent = $this->doctrine->em->getRepository('Entities\Historique')->findOneBy(array("sinistre" => $sinistre, "type_mail" => 7));
            if ($alreadysent)
                $data['alreadysent'] = $alreadysent;

            $data['active'] = 3;
            $data['topbar2'] = 2;
            $this->twig->display('mails/av1.html.twig', $data);
        } else {
            $objet = $_POST['objet'];
            $to = $dest['destinataires'];
            $cc = $dest['CC'];
            $from = $user->email;
            $files = null;
            $_POST['sinistre'] = $sinistre;
            $_POST['from'] = array('email' => $user->email, 'nom' => $user->prenom . " " . $user->nom, "role" => $user->role_libelle);
            $i = 1;
            $files = array();
            if (isset($_POST['docs'])) {
                $files = splitDocs($_POST['docs']);
                $_POST['docs'] = $files;
            }
            $mail = makeMail($patron, $_POST);

            try {
                sendMail($mail, $objet, $to, $from, $cc, $files);
                saveMail($sinistre->getAv1(), "Transmission documents expert", $mail, $objet, $to, $from, $cc, $files, 7);
            } catch (Exception $e) {
                echo '<script>alert("Une erreur est survenue, l\'email n\'a pas été envoyé.")</script>';
                redirect('statuts/av1/mails_av1/declaration');
                return;
            }
            echo '<script>alert("Email envoyé")</script>';
            redirect('statuts/av1/mails_av1/declaration');
            return;
        }
    }

    public function docs2rcr() {

        $user = $this->context->getUser();
        $sinistre = $this->context->getSinistre();
        $dest = getMailDest(8);
        $patron = 'av1/docs2rcr.html.twig';


        if (!isset($_POST['objet'])) {
            $data_preview = array();
            $data_preview['sinistre'] = $sinistre;
            $data_preview['from'] = array('email' => $user->email, 'nom' => $user->prenom . " " . $user->nom, "role" => $user->role_libelle);
            $preview = getMailPreview($patron, $data_preview);

            $dest['destinataires'] = str_replace(",", ", ", $dest['destinataires']);
            $dest['CC'] = str_replace(",", ", ", $dest['CC']);
            $data = array();
            $data['mail_content'] = $preview;
            $data['title'] = "Transmission documents rcr";
            $data = array_merge($data, $dest);

            $alreadysent = $this->doctrine->em->getRepository('Entities\Historique')->findOneBy(array("sinistre" => $sinistre, "type_mail" => 8));
            if ($alreadysent)
                $data['alreadysent'] = $alreadysent;

            $data['pj'] = true;

            $data['active'] = 4;
            $data['topbar2'] = 2;
            $this->twig->display('mails/av1.html.twig', $data);
        } else {
            $objet = $_POST['objet'];
            $to = $dest['destinataires'];
            $cc = $dest['CC'];
            $from = $user->email;
            $files = null;
            $_POST['sinistre'] = $sinistre;
            $_POST['from'] = array('email' => $user->email, 'nom' => $user->prenom . " " . $user->nom, "role" => $user->role_libelle);
            $i = 1;
            $files = array();
            if (isset($_POST['docs'])) {
                $docs = splitDocs($_POST['docs']);
                $_POST['docs'] = $docs;
            }
            $mail = makeMail($patron, $_POST);

            try {
                sendMail($mail, $objet, $to, $from, $cc, $files);
                saveMail($sinistre->getAv1(), "Transmission documents rcr", $mail, $objet, $to, $from, $cc, $files, 8);
            } catch (Exception $e) {
                echo '<script>alert("Une erreur est survenue, l\'email n\'a pas été envoyé.")</script>';
                redirect('statuts/av1/mails_av1/declaration');
                return;
            }
            echo '<script>alert("Email envoyé")</script>';
            redirect('statuts/av1/mails_av1/declaration');
            return;
        }
    }

}
