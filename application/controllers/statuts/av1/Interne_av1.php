<?php

class Interne_av1 extends MY_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->statut = "av1";
        $this->context->setStatut('AV1');
    }
    
    public function index() {
        $this->memo();
    }

    public function memo() {
        $this->title = "Memos";
        $sinistre = $this->context->getSinistre();
        $statut = $sinistre->getAv1();
        $user = $this->session->userdata('user');
        $user = $user->prenom . " " . $user->nom;

        if (isset($_POST['memo'])) {
            $memo = new Entities\Memo();
            $memo->setDate(new DateTime());
            $memo->setAuteur($user);
            $memo->setMemo($_POST['memo']);

            $statut->addMemo($memo);

            $this->doctrine->em->persist($statut);
            $this->doctrine->em->persist($memo);
            $this->doctrine->em->flush();
        }

        $memos = $statut->getMemos();
        $this->twig->display('interne/av1/memo.html.twig', array('memos' => $memos, 'user' => $user, 'topbar2' => 5, 'active' => 1));
    }

}
