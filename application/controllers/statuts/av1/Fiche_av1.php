<?php

use Entities\Statut_AV1;
use Entities\Adresse;

class Fiche_av1 extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->context->setStatut('AV1');
    }

    public function index() {
        $sinistre = $this->context->getSinistre();
        $id = $sinistre->getId();

        if (!isset($_POST['date_premiere_reunion_expertise'])) {
            $this->twig->display('sinistre/av1.html.twig', array('topbar2' => 1));
            return;
        }

        $av1 = $sinistre->getAv1();
        if (!$av1 instanceof Statut_AV1)
            $av1 = new Statut_AV1();

        $av1 = $this->mapToEntity($_POST, $av1);

        $sinistre->setAV1($av1);
        $this->doctrine->em->persist($sinistre);
        $this->doctrine->em->flush();


        $this->twig->display('sinistre/av1.html.twig', array('topbar2' => 1));
    }

    public function mapToEntity($post, Statut_AV1 $av1) {

        if ($post['date_premiere_reunion_expertise'] != "")
            $av1->setDate_premiere_reunion_expertise(date_create_from_format("Y-m-d", $post['date_premiere_reunion_expertise']));
        else
            $av1->setDate_premiere_reunion_expertise(null);


        $cabinet_expert = $av1->getCabinet_expert();
        if (!$cabinet_expert instanceof Entities\Contact) {
            $cabinet_expert = new Entities\Contact();
        }

        $cabinet_expert->setNom($post['cabinet_expert_nom_contact']);
        $cabinet_expert->setEmail($post['cabinet_expert_email_contact']);
        $cabinet_expert->setTelephone($post['cabinet_expert_telephone_contact']);

        $av1->setCabinet_expert($cabinet_expert);

        $contact_expert = $av1->getContact_expert();
        if (!$contact_expert instanceof Entities\Contact) {
            $contact_expert = new Entities\Contact();
        }
        $contact_expert->setNom($post["expert_nom_contact"]);
        $contact_expert->setPrenom($post["expert_prenom_contact"]);
        $contact_expert->setTelephone($post["expert_telephone_contact"]);
        $contact_expert->setEmail($post["expert_email_contact"]);

        $av1->setContact_expert($contact_expert);

        $adresse_expert = $cabinet_expert->getAdresse();
        if (!$adresse_expert instanceof Adresse) {
            $adresse_expert = new Adresse();
        }

        $adresse_expert->setRue1($post["expert_rue1"]);
        $adresse_expert->setRue2($post["expert_rue2"]);
        $adresse_expert->setRue3($post["expert_rue3"]);
        $adresse_expert->setCp($post["expert_code_postal"]);
        $adresse_expert->setCommune($post["expert_commune"]);


        $cabinet_expert_cie = $av1->getCabinet_expert_compagnie();
        if (!$cabinet_expert_cie instanceof Entities\Contact) {
            $cabinet_expert_cie = new Entities\Contact();
        }

        $cabinet_expert_cie->setNom($post['compagnie_nom_contact']);
        $cabinet_expert_cie->setEmail($post['compagnie_email_contact']);
        $cabinet_expert_cie->setTelephone($post['compagnie_telephone_contact']);

        $av1->setCabinet_expert_compagnie($cabinet_expert_cie);

        $expert_cie = $av1->getContact_expert_compagnie();

        if (!$expert_cie instanceof Entities\Contact) {
            $expert_cie = new Entities\Contact();
        }
        $expert_cie->setNom($post["expert_compagnie_nom_contact"]);
        $expert_cie->setPrenom($post["expert_compagnie_prenom_contact"]);
        $expert_cie->setTelephone($post["expert_compagnie_telephone_contact"]);
        $expert_cie->setEmail($post["expert_compagnie_email_contact"]);

        $av1->setContact_expert_compagnie($expert_cie);

        $av1->setRef_dossier_expert($post["ref_dossier_expert"]);
        $av1->setRef_dossier_expert_compagnie($post["ref_dossier_expert_cie"]);

        return $av1;
    }

}
