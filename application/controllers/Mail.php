<?php

class Mail extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function mail_libre()
    {

        ini_set("display_errors", true);
        error_reporting(E_ALL);

        $sin = $this->context->getSinistre();
        $statut = $sin->getStatut();

        switch ($statut)
        {
            case 1:
                $topbar3 = "mails/do/topbar_mails_do.html.twig";
                $active = 8;
                $myStatut = $sin->getDo();
                break;
            case 2:
                $topbar3 = "mails/av1/topbar_mails_av1.html.twig";
                $active = 5;
                $myStatut = $sin->getAv1();
                break;
            case 3:
                $topbar3 = "mails/judiciaire/topbar_mails_judiciaire.html.twig";
                $active = 6;
                $myStatut = $sin->getJudiciaire();
                break;
            case 4:
                $topbar3 = "mails/amiable/topbar_mails_amiable.html.twig";
                $active = 5;
                $myStatut = $sin->getAmiable();
                break;
            case 5:
                $topbar3 = "mails/rp/topbar_mails_rp.html.twig";
                $active = 8;
                $myStatut = $sin->getRp();
                break;
        }

        if (!isset($_POST['destinataires']))
        {
            $this->twig->display("mails/mail_libre.html.twig", array('topbar3' => $topbar3, "active" => $active));
            return;
        }

        $this->load->helper('mailer');

        $dest = $_POST['destinataires'];
        $cc = @$_POST['CC'];
        $objet = $_POST['objet'];
        $mail = $_POST['mail']; //str_replace("\n", "<br>",$_POST['mail']);
        $files = @$_POST['docs'];
        $files_array = null;

        if ($files)
            $files_array = explode(",", $files);
        $files_array_e = null;
        if (is_array($files_array))
        {
            $file_r = $this->doctrine->em->getRepository('Entities\File');
            $files_array_e = array();
            $this->load->model('ImportDocuments');
            foreach ($files_array as $f)
            {
                if ($f[0] == "q")
                    $f_e = $this->ImportDocuments->fetch("qp", $f);
                elseif ($f[0] == "g")
                    $f_e = $this->ImportDocuments->fetch("gaia", str_replace("g", "", $f));
                else
                {
                    $f_e = $file_r->find($f);
                }
                $files_array_e[] = $f_e;
            }
        }

        $mail_htm = makeMail("mail_libre.html.twig", array("objet" => $objet, "mail_content" => $mail, "sinistre" => $sin, "affaire" => $this->context->getAffaire()));
        try
        {
            sendMail($mail_htm, $objet, $dest, $this->context->getUser()->email, $cc, $files_array_e);
        } catch (Exception $exc)
        {
            redirect('mail/mail_libre');
        }
        saveMail($myStatut, "Mail libre", $mail_htm, $objet, $dest, $this->context->getUser()->email, $cc, $files_array_e, 0);

        redirect('sinistre_c');
    }

    public function getContactsMailLibre()
    {

//        ini_set("display_errors", true);
//        error_reporting(E_ALL);

        $sinistre = $this->context->getSinistre();
        $contacts = array();

        //RCR
        $id_rcr = $sinistre->getAffaire()->getRcr();
        $this->load->model('User');
        $rcr = $this->User->getBaseUser($id_rcr);
        $contacts[] = $rcr->email;

        //ACA/RCA/DA
        $id_agence = $sinistre->getAffaire()->getId_agence();
        $id_service = $sinistre->getAffaire()->getId_service_contentieux();
        $this->load->model('Utils');
        $da = $this->Utils->getDa($id_service);
        if ($da->email && !in_array($da->email, $contacts))
            $contacts[] = $da->email;
        //var_dump($id_service);
        $rcrRepo = $this->doctrine->em->getRepository('Entities\Rcr');
        $rcr_row = $rcrRepo->findOneBy(array("id_agence" => $id_agence));
        if ($rcr_row->getMail_rca() && !in_array($rcr_row->getMail_rca(), $contacts))
            $contacts[] = $rcr_row->getMail_rca();
        if ($rcr_row->getMail_aca() && !in_array($rcr_row->getMail_aca(), $contacts))
            $contacts[] = $rcr_row->getMail_aca();

        //Asqua
        $contacts[] = "contact@asqua.fr";

        //DJ
        $contacts[] = "laurent.ferhani@qualiconsult.fr";

        //Sinistre
        $statut = $sinistre->get_current_statut();
        if ($statut)
            switch ($sinistre->getStatut())
            {
                case 1:
                    if ($statut->getCabinet_expert() && $statut->getCabinet_expert()->getEmail())
                        $contacts[] = $statut->getCabinet_expert()->getEmail();
                    if ($statut->getContact_expert() && $statut->getContact_expert()->getEmail())
                        $contacts[] = $statut->getContact_expert()->getEmail();
                    break;
                case 2:
                    if ($statut->getCabinet_expert() && $statut->getCabinet_expert()->getEmail())
                        $contacts[] = $statut->getCabinet_expert()->getEmail();
                    if ($statut->getContact_expert() && $statut->getContact_expert()->getEmail())
                        $contacts[] = $statut->getContact_expert()->getEmail();
                    if ($statut->getCabinet_expert_compagnie() && $statut->getCabinet_expert_compagnie()->getEmail())
                        $contacts[] = $statut->getCabinet_expert_compagnie()->getEmail();
                    if ($statut->getContact_expert_compagnie() && $statut->getContact_expert_compagnie()->getEmail())
                        $contacts[] = $statut->getContact_expert_compagnie()->getEmail();
                    break;
                case 3:
                    if ($statut->getExpert_judiciaire() && $statut->getExpert_judiciaire()->getEmail())
                        $contacts[] = $statut->getExpert_judiciaire()->getEmail();
                    if ($statut->getCabinet_expert_compagnie_judiciaire() && $statut->getCabinet_expert_compagnie_judiciaire()->getEmail())
                        $contacts[] = $statut->getCabinet_expert_compagnie_judiciaire()->getEmail();
                    if ($statut->getExpert_compagnie_judiciaire() && $statut->getExpert_compagnie_judiciaire()->getEmail())
                        $contacts[] = $statut->getExpert_compagnie_judiciaire()->getEmail();
                    if ($statut->getCabinet_avocat() && $statut->getCabinet_avocat()->getEmail())
                        $contacts[] = $statut->getCabinet_avocat()->getEmail();
                    if ($statut->getAvocat_dossier() && $statut->getAvocat_dossier()->getEmail())
                        $contacts[] = $statut->getAvocat_dossier()->getEmail();
                    if ($statut->getCollaborateur_avocat_dossier() && $statut->getCollaborateur_avocat_dossier()->getEmail())
                        $contacts[] = $statut->getCollaborateur_avocat_dossier()->getEmail();
                    break;
                case 4:
                    if ($statut->getCabinet_expert_compagnie() && $statut->getCabinet_expert_compagnie()->getEmail())
                        $contacts[] = $statut->getCabinet_expert_compagnie()->getEmail();
                    if ($statut->getExpert_compagnie() && $statut->getExpert_compagnie()->getEmail())
                        $contacts[] = $statut->getExpert_compagnie()->getEmail();
                    break;
                case 5:
                    if ($statut->getExpert_judiciaire() && $statut->getExpert_judiciaire()->getEmail())
                        $contacts[] = $statut->getExpert_judiciaire()->getEmail();
                    break;
            }


        echo json_encode($contacts);
    }

}
