<?php

/**
 * @property Reporting_model $Reporting_model
 */
class Reporting extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("Reporting_model");
    }

    public function index() {

        $regions = $this->Reporting_model->get_regions();
        $services = $this->Reporting_model->get_services();

        $this->twig->display("reporting.html.twig", array("regions" => $regions, "services" => $services));
    }

    public function test() {
        $this->twig->display("reporting_data.html.twig");
    }

    public function get_reporting($annee, $portee) {

        $data = array();

        $data["annee"] = $annee;

        if ($portee == "global") {
            $data["entite"] = "Pôle construction";
        } else {
            $split = explode("_", $portee);
            if ($split[0] == "r") {
                $data["entite"] = $this->get_nom_region($split[1]);
            } else {
                $data["entite"] = $this->get_nom_service($split[1]);
            }
        }
        
        $reporting = $this->Reporting_model->get_reporting($annee, $portee);
        
        $data = array_merge($data, $reporting);
//        echo "<pre>";
//        var_dump($data);
//        echo "</pre>";
        $this->twig->display("reporting_data.html.twig", $data);
    }
    
    public function get_nom_region($id_entite) {
        return $this->Reporting_model->get_nom_region($id_entite);
    }
    
    public function get_nom_service($id_entite) {
        return $this->Reporting_model->get_nom_service($id_entite);
    }

}
