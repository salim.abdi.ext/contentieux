<?php

/**
 * @property Roles_model $Roles_model
 */
class Roles extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("Roles_model");
    }

    public function index() {
        $roles = $this->Roles_model->get_roles();
        $this->twig->display('admin/newRoles.html.twig', compact("roles"));
    }

    public function autocomplete_agences() {
        $term = $_GET["term"];
        return json_encode($this->Roles_model->autocomplete_agence($term));
    }

    public function update_role() {
        $type = $_POST["type"];
        $id_agence = $_POST["id_agence"];
        $fullname = $_POST["fullname"];
        $id_utilisateur = $_POST["id_utilisateur"];
        
        $this->Roles_model->maj_role($type, $id_agence, $fullname, $id_utilisateur);
    }

    public function remplir_roles() {
        $this->Roles_model->remplir_roles();
    }
    
    public function appliquer_anciens_roles() {
        $this->Roles_model->appliquer_anciens_roles();
    }
    
    public function maj_affaires() {
        $this->Roles_model->maj_affaires();
    }

}
