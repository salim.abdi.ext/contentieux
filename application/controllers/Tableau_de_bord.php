<?php

class Tableau_de_bord extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {

        $this->load->model("Tableau_de_bord_model");

        $user = $this->context->getUser();

        $liste = $this->Tableau_de_bord_model->getList($user->id_utilisateur);
        $this->twig->display("tableau_de_bord.html.twig", array("liste" => $liste));
    }
}
