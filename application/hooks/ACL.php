<?php

/**
 * This class will be called by the post_controller_constructor hook and act as ACL
 *
 * @author ChristianGaertner
 *
 * Modifications :
 *  0 => acces refuse
 *  1 => acces consultation
 *  2 => acces edition
 *
 * Acces refuse ==> renvoie sur la page d'accueil
 *
 * Par defaut : acces en consultation
 *
 * Edition/consultation ==> | modifie la variable edit en session ( $_SESSION['context']['edit']
 *                          | grace a la methode setEditable(Bool $edit) de la librairie Context
 */
class ACL {

    /**
     * Array to hold the rules
     * Keys are the role_id and values arrays
     * In this second level arrays the key is the controller and value an array with key method and value boolean
     * @var Array
     */
    private $perms = array();

    /**
     * The field name, which holds the role_id
     * @var string
     */
    private $role_field;
    private $ci;

    /**
     * Contstruct in order to set rules
     * @author ChristianGaertner
     */
    public function __construct() {
        $this->role_field = "user";
        $this->ci = &get_instance();
        $acces = $this->ci->doctrine->em->getRepository('Entities\Acces')->findAll();

        foreach ($acces as $row) {
            $row->setClasse(strtolower($row->getClasse()));
            $row->setMethode(strtolower($row->getMethode()));
            $this->perms[1][$row->getClasse()][$row->getMethode()] = $row->getGeneraliste();
            $this->perms[2][$row->getClasse()][$row->getMethode()] = $row->getAca();
            $this->perms[3][$row->getClasse()][$row->getMethode()] = $row->getRca();
            $this->perms[4][$row->getClasse()][$row->getMethode()] = $row->getDa();
            $this->perms[5][$row->getClasse()][$row->getMethode()] = $row->getRcr();
            $this->perms[6][$row->getClasse()][$row->getMethode()] = $row->getDr();
            $this->perms[7][$row->getClasse()][$row->getMethode()] = $row->getDcn();
            $this->perms[8][$row->getClasse()][$row->getMethode()] = $row->getDj();
            $this->perms[9][$row->getClasse()][$row->getMethode()] = $row->getAsqua();
        }
    }


    /**
     * The main method, determines if the a user is allowed to view a site
     * @author ChristianGaertner
     */
    public function auth() {
        $CI = &get_instance();
        $user = $CI->session->userdata('user');

        if (!isset($CI->session)) { # Sessions are not loaded
            $CI->load->library('session');
        }

        if (!isset($CI->router)) { # Router is not loaded
            $CI->load->library('router');
        }

        if (isset($user->role)) {
            $role = $user->role;
            $roles = array($role);
            @$supplees = $user->remplace;
            if ($supplees)
                foreach ($supplees as $supplee) {
                    $roles[] = $supplee->role;
                }
        } else {
            return;
        }

        rsort($roles);
        $class = strtolower($CI->router->fetch_class());
        $method = strtolower($CI->router->fetch_method());

// Is rule defined?
        foreach ($roles as $role) {
            $is_ruled = false;

            if (is_array($this->perms)) {
                if (isset($this->perms[$role][$class][$method]))
                    $is_ruled = "method";
                else if (isset($this->perms[$role][$class]['global']))
                    $is_ruled = "class";
                else
                    $is_ruled = false;
            }

            if (!$is_ruled) { # No rule defined for this route
                $CI->context->setEditable(false);
                if ($role == 10)
                    $CI->context->setEditable(true);
                return;
            }

            if ($is_ruled == "method") {
                if ($this->perms[$role][$class][$method] == 0) { # The user is not allowed to enter the site
                    $url = $_SERVER['HTTP_REFERER'];
                    echo "<script>alert('Vous n\'avez pas acces à cette partie du site'); document.location = '$url'</script>";
                    die();
                }
                if ($this->perms[$role][$class][$method] == 1) { # The user is allowed to view
                    $CI->context->setEditable(false);
                    return true;
                }
                if ($this->perms[$role][$class][$method] == 2) { # The user is allowed to edit
                    $CI->context->setEditable(true);
                    return true;
                }
            } else if ($is_ruled == "class") {
                if ($this->perms[$role][$class]['global'] == 0) { # The user is not allowed to enter the site
                    $url = $_SERVER['HTTP_REFERER'];
                    echo "<script>alert('Vous n\'avez pas acces à cette partie du site'); document.location = '$url'</script>";
                    die();
                }
                if ($this->perms[$role][$class]['global'] == 1) { # The user is allowed to view
                    $CI->context->setEditable(false);
                    return true;
                }
                if ($this->perms[$role][$class]['global'] == 2) { # The user is allowed to edit
                    $CI->context->setEditable(true);
                    return true;
                }
            }
        }
    }

}
