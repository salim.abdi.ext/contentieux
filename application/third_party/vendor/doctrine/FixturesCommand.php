<?php

define('APPPATH', realpath(dirname(__FILE__) . "/../../..") . DIRECTORY_SEPARATOR);
define('BASEPATH', realpath(APPPATH . '..') . DIRECTORY_SEPARATOR);
require APPPATH . 'libraries/Doctrine.php';

$doctrine = new Doctrine();


$em = $doctrine->em;

$loader = new Doctrine\Common\DataFixtures\Loader();
$loader->loadFromDirectory(APPPATH . "models/fixtures");


$purger = new Doctrine\Common\DataFixtures\Purger\ORMPurger();
$executor = new Doctrine\Common\DataFixtures\Executor\ORMExecutor($em, $purger);

$executor->execute($loader->getFixtures());
