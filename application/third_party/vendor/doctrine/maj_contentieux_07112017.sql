--===========
--production
--===========

ALTER TABLE affaire ADD statut_affaire INT;
ALTER TABLE sinistre ADD sinistre_clos_date DATE;
ALTER TABLE sinistre ADD sinistre_clos_cout INT;
ALTER TABLE sinistre ADD sinistre_clos_cout_qc INT;
ALTER TABLE sinistre ADD sinistre_clos_mission NVARCHAR(255);
ALTER TABLE sinistre ADD sinistre_clos_destination_ouvrage NVARCHAR(255);
ALTER TABLE sinistre ADD sinistre_clos_partie_ouvrage NVARCHAR(255);
ALTER TABLE sinistre ADD sinistre_clos_phase_concernee NVARCHAR(255);
ALTER TABLE sinistre ADD date_cloture_suppression DATETIME2(6);
ALTER TABLE sinistre ALTER COLUMN flag_sinistre_desactive INT;
