<?php

$mails_libelles = array(
    1 => 'Declaration ASQUA DO',
    2 => 'Indisponibilite reunion DO',
    3 => 'Grille analyse sinistre DO',
    4 => 'Compte rendu réunion DO',
    5 => 'Declaration ASQUA av1',
    6 => 'Transmission docs expert av1',
    7 => 'Transmission docs RCR av1',
    8 => 'Compte rendu réunion av1',
    9 => 'Declaration asqua Judiciaire',
    10 => 'Declaration asqua Judiciaire',
    11 => 'Mail avocat Judiciaire',
    12 => 'Transmission docs avocat',
    13 => 'Transmission docs RCR Judiciaire',
    14 => 'Compte rendu réunion Judiciaire',
    15 => 'Declaration ASQUA Amiable',
    16 => 'Transmission docs expert Amiable',
    17 => 'Transmission docs RCR Amiable',
    18 => 'Compte rendu réunion Amiable',
    19 => 'Grille d\'analyse rapport expert DO',
    20 => 'Grille d\'analyse rapport expert Judiciaire',
    21 => 'Déclaration Asqua Référé préventif'
);

// Renvoi une preview du mail, formulaire apparent, inclus dans une div avec bordure mais pas dans un tableau ni dans un doc html a part entière
function getMailPreview($patron, $data = null) {
    $ci = &get_instance();
    $ci->load->model('Utils');
    $footer = $ci->Utils->getFooter($data["sinistre"]->getAffaire()->getId_service_contentieux());
    $data["footer"] = $footer;
    $data['preview'] = true;
    $mail = $ci->twig->render('mails/patrons/' . $patron, $data);
    $header = "<div id=\"mail\" style=\"width:21cm; border: 1px solid lightgray; padding: 20px;\"><img style=\"float: left; {% if post %}margin-left: 0.7cm{% endif %}\" src=\"http://www.groupe-qualiconsult.fr/wp-content/themes/QualiconsultGroup/library/images/logo.png\">";
    return $header . $mail . "</div>";
}

// Renvoie le mail dans un document html a part entière
function makeMail($patron, $data) {
    $ci = &get_instance();
    $ci->load->model('Utils');
    $footer = $ci->Utils->getFooter($data["sinistre"]->getAffaire()->getId_service_contentieux());
    $data["footer"] = $footer;
    $header = $ci->twig->render('mails/patrons/header.html.twig');
    $footer = $ci->twig->render('mails/patrons/footer.html.twig');
    $mail = $ci->twig->render('mails/patrons/' . $patron, $data);

    return $header . $mail . $footer;
}

// Envoie le mail
function sendMail($mail, $objet, $to, $from, $cc, $files) {

    $ci = &get_instance();
    $ci->load->library('email');
    $ci->email->initialize(array('mailtype' => 'html'));
    $ci->email->from($from);
    if (ENVIRONMENT == "production") {
        $ci->email->to($to);
        $ci->email->cc($cc);
    } else {
        $ci->email->to($from);
    }
    $ci->email->subject($objet);
    $ci->email->message($mail);

    $size = 0;

    if ($files) {
        if (!is_array($files)) {
            $id_file = $files->getId();
            $ci->email->attach($files->getUri(), null, $files->getNom_public() . "." . $files->getExt());
        } else if (is_array($files)) {
            $nb_qp = 0;
            foreach ($files as $file) {
                if (!$file instanceof \Entities\File) {
                    die("Erreur de pièce jointe, contactez la DSI");
                }
                $uri = (substr($file->getId(), 0, 1) == "g") ? $file->getUri() : FILES_PATH . "/" . $file->getUri();
                $ci->email->attach($uri, null, $file->getNom_public() . "." . $file->getExt());
                $size += $file->getSize();
            }
            if ($size >= 10000000) {
                // 12487233
                echo "<script>alert(\"Fichiers joints trop volumineux, essayez d'envoyer les fichiers en plusieurs mails en utilisant le mail libre\");</script>";
                throw new Exception("Fichiers joints trop volumineux");
            }
        }
    }
    $ci->email->send();
    
    if (isset($nb_qp)) {
        for ($i = 1; $i <= $nb_qp; $i++) {
            unlink('./tmp' . $i);
        }
    }
}

function metaDests(&$adresse) {
    $ci = &get_instance();
    $affaire = $ci->context->getAffaire();
    $ci->load->model('User');
    if ($adresse == '[rcr]') {
        //Récupération du RCR lié à l'affaire
        $id_rcr_aff = $affaire->getRcr();
        if ($id_rcr_aff == null) {
            $rcr_aff = $ci->doctrine->em->getRepository('Entities\Rcr')->findOneBy(array('id_agence' => $affaire->getId_agence()));
            if ($rcr_aff)
                $id_rcr_aff = $rcr_aff->getId_rcr();
        }
        $rcr = $ci->User->getById($id_rcr_aff);

        if ($rcr != null)
            $adresse = $rcr->email;
        else
            $adresse = '';

        //Thierry Samson adjoint de laurent abert.
        if ($id_rcr_aff == 5785) {
            $adresse .= ",thierry.samson@qualiconsult.fr";
        }
    }
    if ($adresse == '[rca]') {
        $rcr_aff = $ci->doctrine->em->getRepository('Entities\Rcr')->findOneBy(array('id_agence' => $affaire->getId_agence()));
        //var_dump($affaire->getId_agence());
        if ($rcr_aff)
            $adresse = $rcr_aff->getMail_rca();
        else
            $adresse = '';
    }

    if ($adresse == '[aca]') {
        $rcr_aff = $ci->doctrine->em->getRepository('Entities\Rcr')->findOneBy(array('id_agence' => $affaire->getId_agence()));
        if ($rcr_aff)
            $adresse = $rcr_aff->getMail_aca();
        else
            $adresse = '';
    }

    if ($adresse == '[da]') {
        $id_svc = $affaire->getId_service_contentieux();
        $ci->load->model("Utils");
        $da = $ci->Utils->getDa($id_svc);
        if ($da) {
            $adresse = $da->email;
        } else {
            $adresse = '';
        }
    }
}

// Recupère la liste de diffusion pour un type donné
function getMailDest($type) {
    $ci = &get_instance();
    $mailingList = $ci->doctrine->em->getRepository('Entities\MailingList');
    $list = $mailingList->findOneBy(array('type' => $type));
    $to = array();
    $cc = array();
    $dests = $list->getDestinataires();

    $split = explode(",", $dests);
    foreach ($split as $adresse) {
        metaDests($adresse);
        if ($adresse != "")
            $to[] = $adresse;
    }

    $dests = $list->getCcs();
    $split = explode(",", $dests);
    foreach ($split as $adresse) {
        metaDests($adresse);
        if ($adresse != "")
            $cc[] = $adresse;
    }

    return array('destinataires' => implode(',', $to), 'CC' => implode(',', $cc));
}

function saveMail($statut, $libelle, $mail, $objet, $to, $from, $cc, $files, $type) {
    $ci = &get_instance();
    $user = $ci->session->userdata('user');
    $user_id = $user->id_utilisateur;
    $user = $user->prenom . " " . $user->nom;

    $histRepo = $ci->doctrine->em->getRepository('Entities\Historique');
    if ($statut instanceof Entities\Statut) {
        $sinistre = $statut->getSinistre();
        $nextChrono = $histRepo->getNextChrono(1, $sinistre);
    }

    $data = array(
        "cc" => $cc,
        "to" => $to,
        "mail" => $mail,
        "expediteur" => $user,
        "expediteur_id" => $user_id,
        "objet" => $objet);

    if ($files) {
        if (!is_array($files)) {
            $data['attachements'][] = $files->getId();
        } else if (is_array($files)) {
            foreach ($files as $file) {
                $data['attachements'][] = $file->getId();
            }
        }
    }

    $log = new Entities\Historique();
    $log->setType(1);
    $log->setType_mail($type);
    $log->setData($data);
    $log->setNum_chrono($nextChrono);
    $log->setLibelle($libelle);
    if ($statut instanceof Entities\Statut) {
        $log->setStatut($statut);
        $log->setSinistre($sinistre);
    }
    $statut->addHistorique($log);

    $ci->doctrine->em->persist($log);
    $ci->doctrine->em->persist($statut);
    if (isset($sinistre))
        $ci->doctrine->em->persist($sinistre);
    $ci->doctrine->em->flush();
}

function getFile($id) {
    $ci = &get_instance();
    if ($id[0] != "g" && $id[0] != "q") {
        $repo = $ci->doctrine->em->getRepository('Entities\File');
        $file = $repo->find($id);
        if (!$file)
            return false;
        return $file;
    }

    if ($id[0] == "g") {
        $ci->load->model('ImportDocuments');
        $file = $ci->ImportDocuments->getDoc(substr($id, 1));
        if (!$file)
            return false;
        return $file;
    }

    if ($id[0] == "q") {
        $ci = &get_instance();
        $ci->load->model('ImportDocuments');
        $id_file = str_replace("q", "", $id);
        $file = $ci->ImportDocuments->getDocQp($id_file);

        return $file;
    }
}

function splitDocs($post_docs) {
    $docs = explode(",", $post_docs);
    $files = array();

    foreach ($docs as $doc) {
        $files[] = getFile($doc);
    }

    return $files;
}

function mailSupport($stack, $message) {
    sendMail($stack . "<br>" . $message, "Erreur Gaïa sinistre", "martin.le-barbenchon@qualiconsult.fr", "support_contentieux@donotreply.fr", "", null);
}
