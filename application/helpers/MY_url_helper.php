<?php

if (!defined("last_url")) {
    function last_url() {
        if (isset($_SERVER['HTTP_REFERER'])) {
            return $_SERVER['HTTP_REFERER'];
        }
    }
}