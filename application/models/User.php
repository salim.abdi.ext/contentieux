<?php

class User extends Dao
{

    private $roles_libelles_h = array(
        1 => "Généraliste",
        2 => "Assistant contentieux agence",
        3 => "Responsable contentieux agence",
        4 => "Directeur d'agence",
        5 => "Responsable contentieux région",
        6 => "Directeur régional",
        7 => "Direction contentieux nationale",
        8 => "Direction juridique",
        9 => "Collaborateur Asqua",
        10 => "Administrateur"
    );
    private $roles_libelles_f = array(
        1 => "Généraliste",
        2 => "Assistante contentieux agence",
        3 => "Responsable contentieux agence",
        4 => "Directrice d'agence",
        5 => "Responsable contentieux région",
        6 => "Directrice régionale",
        7 => "Direction contentieux nationale",
        8 => "Direction juridique",
        9 => "Collaboratrice Asqua",
        10 => "Administrateur"
    );

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Récupère les informations de l'utilisateur dans Qualiperf
     *
     * @param String $login
     * @return User $user
     */
    public function get($login, $id_utilisateur = null)
    {
        //Verification des informations d'authentification
        if ($id_utilisateur == null)
        {
            $id_utilisateur = $this->get_id_utilisateur($login);
        }
        if (!$id_utilisateur)
            return false;

        //Récuperation des infos de base
        $user = $this->getBaseUser($id_utilisateur);

        //Definition & Verification du role
        $test_role = $this->check_access($user);
        if (!$test_role)
        {
            //Si pas d'acces au site, verification de la suppleance
            $test_suppleance = $this->check_suppleance($id_utilisateur);
            if (!$test_suppleance)
                return false;
            else
                $supplee = $this->getBaseUser($test_suppleance);
        }
        //Informations complémentaires
        $user->agence = $this->getUserAgences($user->id_utilisateur);
        $user->region = $this->getRegions($user->id_utilisateur);
        $role = $this->get_role($user);
        if (@$supplee)
        {
            $role_supplee = $this->get_role($supplee);
            if ($role_supplee > $role)
            {
                $role = $role_supplee;
            }
        }
        $user->role = $role['id'];
        $user->role_str = $role['str'];
        if ($user->civilite == 2)
        {
            $user->role_libelle = $this->roles_libelles_f[$role['id']];
        } else
        {
            $user->role_libelle = $this->roles_libelles_h[$role['id']];
        }

        $user->remplace = $this->get_suppleance($user);

//        var_dump($user);
//        die();
        return $user;
    }

    private function check_access($user)
    {
        if ($user->id_utilisateur == 829)
            return true;
        if ($user->id_profil != 6 && $user->id_profil != 3 && $user->id_profil != 27 && $user->id_profil != 22)
        {
            $test_role_gaia = $this->get_role_gaia($user);
            if ($test_role_gaia == 0)
                return false;
        }

        return true;
    }

    private function get_role_gaia($user)
    {
        $query = <<<SQL
                    SELECT profil_contentieux FROM collaborateur_profil
                    WHERE id_collaborateur_qperf = ?
SQL;
        $role = $this->contentieux->query($query, array($user->id_utilisateur))->row_array();
        return $role['profil_contentieux'];
    }

    public function getBaseUser($id_utilisateur)
    {
        $query = <<<SQL
                SELECT u.id_utilisateur, u.nom, u.prenom, u.email, u.id_utilisateur, u.id_utilisateur_artemis, u.id_profil, u.civilite, u.initiales, us.id_service
                FROM utilisateurs u
                INNER JOIN utilisateurs_services us ON u.id_utilisateur = us.id_utilisateur AND us.ordre = 1
                WHERE u.id_utilisateur = ?
SQL;
        $user = $this->qualiperf->query($query, array($id_utilisateur))->row();

        return $user;
    }

    private function get_id_utilisateur($login)
    {
        $query = "SELECT id_utilisateur from utilisateurs where login = ? and (date_sortie is null or date_sortie > getDate())";
        $r = $this->qualiperf->query($query, array($login))->row_array();

        if (isset($r["id_utilisateur"]))
            return $r["id_utilisateur"];
        else
            return false;
    }

    public function getUserAgences($id)
    {
        $agence = array();

        $query = <<<SQL
        SELECT DISTINCT id_Agence FROM vue_UO_Service v
        INNER JOIN utilisateurs_services us ON us.id_service = v.id_service_QP AND us.ordre = 1
        WHERE id_utilisateur = $id;
SQL;
        $result = $this->qualiperf->query($query)->result_array();

        foreach ($result as $row)
            $agence[] = $row['id_Agence'];
        return $agence;
    }

    public function getRegions($id)
    {

        $region = array();
        $query = <<<SQL
                SELECT DISTINCT e.id_entite
                FROM utilisateurs u
                INNER JOIN utilisateurs_services us ON u.id_utilisateur = us.id_utilisateur AND us.ordre = 1
                INNER JOIN association a ON us.id_service = a.id_service
                INNER JOIN entites e ON a.id_entite = e.id_entite AND e.id_type_entite=3
                WHERE u.id_utilisateur = '$id'
SQL;
        $result = $this->qualiperf->query($query)->result_array();

        foreach ($result as $row)
            $region[] = $row['id_entite'];

        return $region;
    }

    public function getById($id_utilisateur)
    {
        $user = $this->getBaseUser($id_utilisateur);
        if (!$user)
            return false;
        $user->agence = $this->getUserAgences($user->id_utilisateur);
        $user->region = $this->getRegions($user->id_utilisateur);
        $role = $this->get_role($user);
        $user->role = $role['id'];
        $user->role_str = $role['str'];
        $user->remplace = $this->get_suppleance($user);

        return $user;
    }

    public function get_role($user)
    {
        $role = array();
        $role['id'] = 0;

        switch ($user->id_profil)
        {
            case 6:
                $role['id'] = 1;
                break;
            case 3:
                $role['id'] = 4;
                break;
            case 27:
                $role['id'] = 6;
                break;
            case 22:
                $role['id'] = 7;
                break;
        }

        // Verification du rôle dans gaia
        $query = <<<SQL
                    SELECT profil_contentieux FROM collaborateur_profil
                    WHERE id_collaborateur_qperf = $user->id_utilisateur
SQL;
        $role_gaia = $this->contentieux->query($query)->row();

        if (@$role_gaia->profil_contentieux != null)
        {
            $role['id'] = 0 + $role_gaia->profil_contentieux;
        }

        if ($user->id_utilisateur == 2769)
        {
            $role['id'] = 5;
        } else if ($user->id_utilisateur == 829)
        {
            $role['id'] = 8;
        }

        //'Nom' du rôle
        $roles = array(null, 'CA', 'ACA', 'RCA', 'DA', 'RCR', 'DR', 'DCN', 'DJ', '', 'Admin');
        $role['str'] = $roles[$role['id']];

        return $role;
    }

    /**
     * Récupère la liste des utilisateurs déclarés RCR
     *
     * @return array
     */
    public function getRcr()
    {

        $contentieux = $this->load->database('contentieux', true);
        $query = "SELECT distinct id_rcr as id_utilisateur from rcr";
        $stmt = $contentieux->query($query)->result();
        $rcrs = array();
        foreach ($stmt as $user)
        {
            $rcrs[] = $this->getById($user->id_utilisateur);
        }
        return $rcrs;
    }

    public function getGenre($id)
    {
        if ($id != null && $id != "")
        {
            $query = <<<SQL
                SELECT u.civilite
                FROM utilisateurs u
                WHERE u.id_utilisateur = '$id'
SQL;

            $user = $this->qualiperf->query($query)->result();

            return $user[0]->civilite;
        } else
        {
            return 1;
        }
    }

    public function update_role($id, $role)
    {
        if ($role == null)
            $query = "UPDATE collaborateur_profil SET profil_contentieux = NULL where id_collaborateur_qperf = '$id'";
        else
            $query = "UPDATE collaborateur_profil SET profil_contentieux = '$role' where id_collaborateur_qperf = '$id'";
        $this->contentieux->query($query);
    }

    private function get_suppleance($user)
    {
        $supplees = array();
        $result = $this->doctrine->em->getRepository('Entities\Suppleance')->findBy(array('id_suppleant' => $user->id_utilisateur));
        if (count($result) > 0)
        {
            foreach ($result as $row)
            {
                if ($row->getDate_debut()->getTimestamp() <= date('U') && ($row->getDate_fin() == null || $row->getDate_fin()->getTimestamp() >= date('U')))
                {
                    $absent = $this->getById($row->getId_utilisateur());
                    $supplees[] = $absent;
                }
            }
        } else
            $supplees = null;

        return $supplees;
    }

    private function check_suppleance($id_utilisateur)
    {
        $query = "select * from suppleance where id_suppleant = ? AND date_debut < getDate() AND date_fin > getDate()";
        $r = $this->contentieux->query($query, array($id_utilisateur))->row_array();

        if (isset($r['id_utilisateur']))
            return $r['id_utilisateur'];
        else
            return false;
    }

}
