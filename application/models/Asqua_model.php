<?php

class Asqua_model extends CI_Model {

    private $db;

    public function __construct() {
        parent::__construct();
        $this->db = $this->load->database('contentieux', true);
    }

    private function vider() {
        $query = "TRUNCATE TABLE asqua";
        $this->db->query($query);
    }

    public function refresh($liste) {
        $this->vider();
        $this->toDB($liste);
    }

    public function toDB($liste) {
        $querybase = "INSERT INTO asqua ( date_edition, base_asqua, statut_sinistre, ref_asqua, ref_provisoire, libelle_sinistre, description, etat, date_survenance, DROC,
            type_garantie, est_judiciaire, expertise_do, assureur_do, prenom_expert_do, nom_expert_do, rue1_expert_do, rue2_expert_do, rue3_expert_do, cp_expert_do, commune_expert_do 
           , tel_expert_do, mail_expert_do, ref_expert_do, cabinet_expert_cie, prenom_expert_cie, nom_expert_cie, rue1_expert_cie, rue2_expert_cie , rue3_expert_cie, cp_expert_cie, commune_expert_cie, tel_expert_cie 
           , mail_expert_cie, ref_expert_cie, cabinet_expert_jud , rue1_expert_jud, rue2_expert_jud, rue3_expert_jud, cp_expert_jud, commune_expet_jud, prenom_expert_jud 
           , nom_expert_jud, tel_expert_jud, mail_expert_jud, cabinet_avocat, rue1_avocat , rue2_avocat, rue3_avocat, cp_avocat, commune_avocat, tel_avocat , prenom_avocat 
           , nom_avocat, mail_avocat, ref_avocat ) VALUES ";

        $j = -1;
        foreach ($liste as $row) {
            $j++;
            $listeln = count($liste);
            $query = "";
            if ($row[0] != "date derniere modif") {
                $query .= "(";
                for ($i=0;$i<56;$i++) {
                    if (isset($row[$i]))
                        $cell = $row[$i];
                    else
                        $cell = "";
                    $txt = str_replace("'", "''", $cell);
                    $query .= "'" . $txt . "'";
                    if ($i < 55)
                        $query .= ",";
                }
                $query .= ")";
                $query = $querybase . $query;
                echo $query;

                $this->db->query($query);
            }
        }
    }

}
