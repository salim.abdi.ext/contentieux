<?php

class Utils extends CI_Model {

    private $gaia;
    private $qualiperf;
    private $contentieux;

    public function __construct() {

        parent::__construct();

        $this->qualiperf = $this->load->database('qualiperf', true);
        $this->contentieux = $this->load->database('contentieux', true);
    }

    public function getAgences($user) {
        $data = null;
        switch ($user->role) {
            //admin / asqua / DCN / Dir Juridique
            case 7:
            case 8:
            case 9:
            case 10:
                $query = <<<SQL
                    SELECT id_entite, nom
                    FROM entites
                    WHERE id_type_entite = 1
                    AND nom NOT LIKE '%HUB%'
                    ORDER BY nom
SQL;
                $query = <<<SQL
                    SELECT DISTINCT e2.id_entite, e2.nom FROM services sv
                    JOIN association a ON a.id_service = sv.id_service
                    JOIN entites e ON e.id_entite = a.id_entite and e.id_type_entite = 3
                    JOIN association a2 ON a2.id_service = sv.id_service
                    JOIN entites e2 ON e2.id_entite = a2.id_entite AND e2.id_type_entite = 1
                    WHERE
                        sv.nom NOT LIKE '%U2E%'
                        AND sv.nom NOT LIKE '%HUB%'
                        AND sv.id_societe = 1
                        ORDER BY e2.nom ASC
SQL;
                $data = $this->qualiperf->query($query)->result();
                break;
            case 5:
                $id_rcr = $user->id_utilisateur;
                $query = <<<SQL
                    SELECT id_agence as id_entite, nom_agence as nom
                    FROM rcr
                    WHERE id_rcr = $id_rcr
                    ORDER BY nom_agence
SQL;
                $data = $this->contentieux->query($query)->result();

                break;

            case 6:
                // DR
                $id_user = $user->id_utilisateur;
                $query = <<<SQL
                    SELECT DISTINCT e2.id_entite, e2.nom FROM services sv
                    JOIN association a ON a.id_service = sv.id_service
                    JOIN entites e ON e.id_entite = a.id_entite and e.id_type_entite = 3
                    JOIN association a2 ON a2.id_service = sv.id_service
                    JOIN entites e2 ON e2.id_entite = a2.id_entite AND e2.id_type_entite = 1
                    WHERE
                        e.id_utilisateur_manager = $id_user
                        AND sv.nom NOT LIKE '%U2E%'
                        AND sv.nom NOT LIKE '%HUB%'
                        AND sv.id_societe = 1
SQL;
                $data = $this->qualiperf->query($query)->result();

                break;

        }

        return $data;
    }

    public function getSAGE($id_service) {

        $query = "select analytique_SAGE from services where id_service = $id_service";

        @$result = $this->qualiperf->query($query)->result();

        if (isset($result[0]))
            return $result[0]->analytique_SAGE;
        else
            return false;
    }

    public function getNomService($id_service) {

        if ($id_service) {

            $query = "select nom from services where id_service = $id_service";

            $result = $this->qualiperf->query($query)->result();

            if (isset($result[0]))
                return $result[0]->nom;
        }

        return false;
    }

    public function getFooter($id_service) {

        $query = <<<SQL
                SELECT s.nom as nom_service, s.telephone as tel_service, s.mail as mail_service, e.* from etablissements e
                inner join services s on s.id_etablissement = e.id_etablissement
                where s.id_service = $id_service
SQL;

        $result = $this->qualiperf->query($query)->result();

        if (isset($result[0])) {

            $etablissement = $result[0];
            $entite = $etablissement->nom_service;
            $contact = $etablissement->adresse_1 . " " . $etablissement->adresse_2 . " " . $etablissement->adresse_3 . " " . $etablissement->code_postal . " " . $etablissement->ville . " - " . $etablissement->tel_service . " - " . $etablissement->mail_service;

            return array('entite' => $entite, 'contact_entite' => $contact);
        }
        else {

            return false;
        }
    }

    public function getVille($id_service) {

        $query = <<<SQL
                SELECT e.ville from etablissements e
                inner join services s on s.id_etablissement = e.id_etablissement
                where s.id_service = $id_service
SQL;

        $result = $this->qualiperf->query($query)->result();

        if (isset($result[0])) {

            return $result[0]->ville;
        }
        else {

            return false;
        }
    }

    public function get_infos_entite($id_service) {

        $query = <<<SQL
            select s.nom as nom_service, s._id_agence as id_agence, s._id_region as id_region, e.nom as nom_agence from services s
            inner join entites e on e.id_entite = s."_id_agence"
            where s.id_service = $id_service
SQL;

        $result = $this->qualiperf->query($query)->result_array();

        return $result[0];
    }

    public function getDa($id_service) {

        $query = "select * from utilisateurs u
                  where id_utilisateur = (
                    SELECT id_utilisateur_manager FROM
                    services s where id_service = $id_service
                )";

        $r = $this->qualiperf->query($query)->row();

        if ($r->id_utilisateur)
            return $r;
        else
            return false;
    }

    public function get_refs_asqua() {
        $query = "SELECT id, ref_asqua FROM sinistre";
        return $this->contentieux->query($query)->result_array();
    }

    public function update_ref_asqua($id, $ref) {
        $query = "UPDATE sinistre SET ref_asqua = ? WHERE id = ?";
        $this->contentieux->query($query, array($ref, $id));
    }

    public function get_missions_cloture() {
        $query = "SELECT * FROM missions where abrege IN ('L','P1','LE','AV','PS','PSE','S','ENV','HAND','BRD','PH','TH','F','HYS','GTB')";
        $r = $this->qualiperf->query($query)->result_array();
        
        return $r;
    }
}
