<?php

/**
 * @property APIQperf $apiqperf
 */
class ImportDocuments extends CI_Model
{

    private $gaia;
    private $qualiperf;

    public function __construct()
    {
        parent::__construct();
        $this->gaia = $this->load->database('gaia', true);
        $this->qualiperf = $this->load->database('qualiperf', true);
    }

    /*
      Types :
      0 : Avis sur conception
      1 : RICT
      2 : BRED (titre dans rap_libelle)
      3 : CR visite
      4 : Liste récapitulative
      5 : Pré RFCT
      6 : RFCT
      7 : Attestation parasismique stade PC
      8 : Attestation parasismique finale
      9->19 : ATTHAND2
      15 : Rapport HAND2015
      16 : Attestation HAND2015
     */

    public function getDocs($affaire, $type)
    {
        $id = $affaire->getId_g_affaire();
        if ($id)
        {
            $query = "SELECT * FROM gaia_rapport WHERE rap_id_affaire = $id";
            if ($type != 'all')
            {
                $query .= " AND rap_type = $type";
            }
            $query .= " ORDER BY id_rapport";

            return $this->gaia->query($query)->result_array();
        }
    }

    public function fetch($from, $id)
    {
        switch ($from)
        {
            case "gaia":
                return $this->getDoc($id);
                break;
            case "qp":
                return $this->getDocQp($id);
                break;
        }
    }

    // Gaia
    public function getDoc($id)
    {
        $query = "SELECT * FROM gaia_rapport WHERE id_rapport = $id";
        $stmt = $this->gaia->query($query)->result();
        $doc = $stmt[0];
        $ci = &get_instance();

        $file = new Entities\File();
        $file->setExt("pdf");
        $file->setNom($doc->rap_filename . ".pdf");
        $file->setNom_public($doc->rap_filename);
        $file->setUpload_date($doc->rap_date);
        $file->setType($ci->doctrine->em->getRepository('Entities\Type_docs')->findOneBy(array('num' => $doc->rap_type)));
        $file->setUri("http://gaia.qualigroup.net/gaia/DownloadRapport?get=" . $doc->rap_publicKey);
        $file->setId("g" . $doc->id_rapport);
        $file->setSize($doc->rap_taille_octets);

        return $file;
    }

    public function getContratQp(Entities\Affaire $affaire)
    {
        $id_qp = $affaire->getId_qp_contrat();
        if ($id_qp)
        {
            $query = "SELECT * FROM documents where id_objet_secondaire = $id_qp";
            $result = $this->qualiperf->query($query)->result();

            return $result;
        } else
        {
            return false;
        }
    }

    public function getDocQp($id)
    {

        $this->load->library("apiqperf");

        $id_doc = substr($id, 1, strpos($id, "c") - 1);
        $id_contrat = substr($id, strpos($id, "c") + 1);

        $infos_contrat = $this->apiqperf->get_infos_contrat($id_contrat);

        $uri = $infos_contrat["IdContrat"] . "/" . $id_doc;
        $path_contrat = FILES_PATH . "/" . $infos_contrat["IdContrat"];
        $path_doc = FILES_PATH . "/" . $uri;
        
        if ($infos_contrat["Convention_signée"] != null && count($infos_contrat["Convention_signée"]) > 0)
        {
            $conv = array_filter($infos_contrat["Convention_signée"], function ($elem) use ($id_doc)
            {
                return ($elem['id'] == $id_doc);
            });
            var_dump($conv);
            if (!empty($conv))
            {
                $nom = $conv[0]["nom"];
            }
        }
        if (@$infos_contrat["Avenant_signé"] != null && count($infos_contrat["Avenant_signé"]) > 0)
        {
            $avt = array_filter($infos_contrat["Avenant_signé"], function ($elem) use ($id_doc)
            {
                return ($elem['id'] == $id_doc);
            });
            if (!empty($avt))
            {
                $nom = $avt[0]["nom"];
            }
        }
        
        if (!is_dir($path_contrat))
        {
            mkdir($path_contrat);
        }

        if (!file_exists($path_doc))
        {
            $doc = $this->apiqperf->get_document($id_doc);
            file_put_contents($path_doc, $doc);
        }

        $file = new \Entities\File();
        $file->setDate(new DateTime("now"));
        $file->setNom($nom);
        $file->setNom_public($nom);
        $file->setType(100);
        $split = explode(".", $nom);
        $file->setExt(end($split));
        $file->setUri($uri);
        $file->setUpload_date(new DateTime("now"));
        $file->setId($id);
        $file->setDate(new DateTime("now"));

        return $file;
    }

}
