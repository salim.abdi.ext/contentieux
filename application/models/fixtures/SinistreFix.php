<?php

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class SinistreFix extends AbstractFixture implements OrderedFixtureInterface {
    
    public function load(ObjectManager $em) {
        
        for ($i=5;$i<=10;$i++) {
        
        $sinistre = new Sinistre();
        
        $em->persist($sinistre);
        $em->flush();
        
        $this->addReference('sinistre_' . $i, $sinistre);
        }
        
    }

    public function getOrder() {
        return 1;
    }

    
}
