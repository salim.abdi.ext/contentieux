<?php

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class AffaireFix extends AbstractFixture implements OrderedFixtureInterface {

    public function load(ObjectManager $em) {

        for ($i = 0; $i <= 20; $i++) {

            $affaire = new Affaire();
            $affaire->setCode($i+1);
            $affaire->setLibelle('libelle');
            $affaire->setRue1('rue1');
            $affaire->setCode_postal('75000');
            $affaire->setCommune('Paris');
            $affaire->setId_service('1234');
            $affaire->setId_ca('4567');
            $affaire->setMissions('miss');
            $affaire->setMo("momo");
            $affaire->setId_qp_contrat('78910');
            $affaire->setId_qp_affaire('12345');

            if ($i >= 5 && $i <= 10) {
                $affaire->addSinistre($this->getReference('sinistre_' . $i));
            }
            $em->persist($affaire);
            $em->flush();
        }
    }

    public function getOrder() {
        return 2;
    }

}
