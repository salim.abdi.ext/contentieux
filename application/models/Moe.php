<?php

class Moe extends CI_Model {

    private $gaia;
    private $qualiperf;

    public function __construct() {
        parent::__construct();
        $this->gaia = $this->load->database('gaia', true);
        $this->qualiperf = $this->load->database('qualiperf', true);
    }

    public function getMoe($affaire) {
        $moes = false;
        $data = array();

        if ($affaire->getId_g_affaire()) {
            $moes = $this->getMoe_g($affaire);
        } else if ($affaire->getId_qp_contrat()) {
            $moes = $this->getMoe_qp($affaire);
        } else if ($affaire->getMoe()) {
            $data = $affaire->getMoe();
            if (!is_array($data))
                @$data = unserialize($data);
            $moes = array();
            foreach ($data as $key => $moe) {
                $moes[$key]['entreprise'] = $key;
                $moes[$key]['nom'] = $moe;
            }
        }
        return $moes;
    }

    public function getMoe_g($affaire) {

        $id_g = $affaire->getId_g_affaire();

        $query = <<<SQL
                SELECT e.ent_nom as entreprise, e.ent_adresse1 as rue1, e.ent_adresse2 as rue2, e.ent_adresse3 as rue3, e.ent_code_postal as code_postal, e.ent_commune as ville, c.cct_nom as nom, c.cct_prenom as prenom, c.cct_tel as telephone, c.cct_email as email, i.int_type
                from dbo.gaia_intervenant i
                INNER JOIN GaiaGWT.dbo.gaia_annu_entreprise e on e.id_entreprise = i.int_id_entreprise
                INNER JOIN GaiaGWT.dbo.gaia_annu_contact c on c.id_contact = i.int_id_contact
                WHERE i.int_id_affaire = $id_g
SQL;

        $results = $this->gaia->query($query)->result();
        foreach ($results as $result) {
            switch ($result->int_type) {
                case 1:
                    $result->int_type = "Architecte";
                    break;
                case 2:
                    $result->int_type = "Maître d'Oeuvre Exécution";
                    break;
                case 3:
                    $result->int_type = "Economiste";
                    break;
                case 4:
                    $result->int_type = "BET Généraliste";
                    break;
                case 5:
                    $result->int_type = "BET Acoustique";
                    break;
                case 6:
                    $result->int_type = "BET Charpente";
                    break;
                case 7:
                    $result->int_type = "BET Clos Couvert";
                    break;
                case 8:
                    $result->int_type = "BET Coordination SSI";
                    break;
                case 9:
                    $result->int_type = "BET CVC - PB";
                    break;
                case 10:
                    $result->int_type = "BET Electricité";
                    break;
                case 11:
                    $result->int_type = "BET Géotechniques";
                    break;
                case 12:
                    $result->int_type = "BET Gros Oeuvre";
                    break;
                case 13:
                    $result->int_type = "BET Thermique";
                    break;
                case 14:
                    $result->int_type = "BET VRD";
                    break;
                case 15:
                case 0:
                    $result->int_type = "Autre";
                    break;
            }
        }

        return $results;
    }

    public function getMoe_qp($affaire) {

        $id_qp = $affaire->getId_qp_contrat();

        $query = <<<SQL
                SELECT t.nom as entreprise, t.telephone, t.email
                FROM contrats c
                INNER JOIN affaires a on a.id_affaire = c.id_affaire
                INNER JOIN tiers t ON t.id_tiers = a.id_maitre_oeuvre
                WHERE c.id_contrat = $id_qp
SQL;

        $results = $this->qualiperf->query($query)->result();

        return $results;
    }

}
