<?php

class Document_dto {

    public $id;
    public $nom;
    public $uri;
    public $nom_public;
    public $num_type;
    public $type;
    public $ext;
    public $size;
    public $upload_date;
    public $date_reception;
    public $date;
    public $sinistre_id;
    public $affaire_id;
    public $grilleAnalyse_id;
    public $from_gaia = false;
    public $from_qp = false;

    public function get_size_ko() {
        return bcdiv($this->size / 1000,1,2);
    }

}
