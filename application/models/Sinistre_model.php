<?php

class Sinistre_model extends Dao {

    public function __construct() {
        parent::__construct();
    }

    public function get_row_sinistre($id) {
        $query = "SELECT * FROM sinistre WHERE id = ?";
        return $this->contentieux->query($query, array($id))->row();
    }

}
