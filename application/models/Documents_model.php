<?php

/**
 * @property APIQperf $apiqperf
 */
class Documents_model extends Dao {

    public function __construct() {
//        ini_set("display_errors", true);
//        error_reporting(E_ALL);
        parent::__construct();
    }

    public function determiner_source_et_get($id_doc) {
        if (substr($id_doc, 0, 1) == "g") {
            return $this->get_doc_gaia(str_replace("g", "", $id_doc));
        }
        if (substr($id_doc, 0, 1) == "q") {
            return $this->get_doc_qp(str_replace("q", "", $id_doc));
        }

        return $this->get_doc($id_doc);
    }

    public function get_doc($id) {
        $query = "SELECT * FROM document WHERE id = ?";
        return $this->mapDatas($this->contentieux->query($query, array($id))->row_array(), "Document_dto");
    }

    public function get_doc_gaia($id) {
        $query = "SELECT id_rapport as id, rap_filename as nom, rap_publicKey as uri, rap_libelle as nom_public, rap_type as num_type, 'pdf' as ext, rap_taille_octets as size, rap_date as date
                  FROM gaia_rapport
                  WHERE id_rapport = ?";
        $doc_arr = $this->gaia->query($query, array($id))->row_array();
        $doc_arr["from_gaia"] = true;
        if ($doc_arr["num_type"] >= 7 && $doc_arr["num_type"] <= 19) {
            $doc_arr["num_type"] = 7;
        }
        $doc_arr["uri"] = "http://gaia.qualigroup.net/gaia/DownloadRapport?get=" . $doc_arr["uri"];
        $doc = $this->mapDatas($doc_arr, "Document_dto");

        return $doc;
    }

    public function get_doc_qp($id) {
        $query = "SELECT id_document as id, nom_fichier as nom, libelle as nom_public, date_creation as date, id_objet FROM documents where id_document = ?";
        $doc_arr = $this->qualiperf->query($query, array($id))->row_array();
        $doc_arr["type"] = "Convention signée";
        $doc_arr["num_type"] = 19;
        $xpl = explode(".", $doc_arr["nom"]);
        $doc_arr["ext"] = end($xpl);
        $doc_arr["uri"] = "http://qualiperf.qualigroup.net/data/affaires/" . rawurlencode($doc_arr["id_objet"]) . "/" . rawurlencode($doc_arr["nom"]);
        $doc = $this->mapDatas($doc_arr, "Document_dto");

        return $doc;
    }

    public function get_docs_affaire($id_affaire) {
        $query = "SELECT d.*, t.type FROM document d inner join type_document t on t.num = d.num_type WHERE affaire_id = ?";
        $docs = $this->contentieux->query($query, array($id_affaire))->result_array();
        $result = array();
        foreach ($docs as $doc) {
            $result[] = $this->mapDatas($doc, "Document_dto");
        }
        return $result;
    }

    public function get_docs_sinistre($id_sinistre) {
        $query = "SELECT d.*, t.type FROM document d inner join type_document t on t.num = d.num_type WHERE sinistre_id = ?";
        $docs = $this->contentieux->query($query, array($id_sinistre))->result_array();
        $result = array();
        foreach ($docs as $doc) {
            $result[] = $this->mapDatas($doc, "Document_dto");
        }
        return $result;
    }

    public function get_docs_gaia($id_affaire) {

        $query = "SELECT 'g' + CAST(id_rapport AS VARCHAR(MAX)) as id, rap_filename as nom, rap_publicKey as uri, rap_libelle as nom_public, rap_type as num_type, 'pdf' as ext, rap_taille_octets as size, rap_date as date
                  FROM gaia_rapport
                  WHERE rap_id_affaire = ?";
        $docs = $this->gaia->query($query, array($id_affaire))->result_array();
        $result = array();
        foreach ($docs as $doc) {
            if ($doc["num_type"] >= 7 && $doc["num_type"] <= 19) {
                $doc["num_type"] = 7;
            }
            @$doc["type"] = $this->contentieux->query("SELECT type FROM type_document WHERE num = " . $doc["num_type"])->row()->type;
            $doc["from_gaia"] = true;
            $result[] = $this->mapDatas($doc, "Document_dto");
        }
        return $result;
    }

    public function get_docs_qp($id_contrat) {
        $this->load->library("apiqperf");
        $liste_docs = $this->apiqperf->get_infos_contrat($id_contrat);
        $result = array();

        if (@$liste_docs["Avenant_signé"] != null) {
            foreach ($liste_docs["Avenant_signé"] as $avt) {
                $doc = array();
                $doc["type"] = "Avenant";
                $doc["num_type"] = 111;
                $doc["from_qp"] = true;
                $doc["nom_public"] = $avt["nom"];
                $doc["id"] = "q" . $avt["id"] . "c" . $id_contrat;
                $result[] = $this->mapDatas($doc, "Document_dto");
            }
        }

        if (@$liste_docs["Convention_signée"] != null) {
            foreach ($liste_docs["Convention_signée"] as $cs) {
                $doc = array();
                $doc["type"] = "Convention signée";
                $doc["num_type"] = 19;
                $doc["from_qp"] = true;
                $doc["nom_public"] = $cs["nom"];
                $doc["id"] = "q" . $cs["id"] . "c" . $id_contrat;
                $result[] = $this->mapDatas($doc, "Document_dto");
            }
        }

        return $result;
    }

    public function get_liste_categories() {
        $query = "SELECT * FROM categorie_doc ORDER BY libelle";
        return $this->contentieux->query($query)->result_array();
    }

    public function get_liste_types() {
        $query = "SELECT * FROM type_document ORDER BY type";
        return $this->contentieux->query($query)->result_array();
    }

    public function changer_type_doc($id, $num, $id_sinistre = null, $id_affaire = null) {
        $query = "UPDATE document SET num_type = ? WHERE id = ?";
        $this->contentieux->query($query, array($num, $id));

        if ($id_sinistre) {
            $this->lier_document($id, null, $id_sinistre);
        } else {

            $this->lier_document($id, $id_affaire);
        }
    }

    public function changer_date($id, $date) {
        $query = "UPDATE document SET date = ? WHERE id = ?";
        $this->contentieux->query($query, array($date, $id));
    }

    public function renommer($id, $nom) {
        $query = "UPDATE document SET nom_public = ? WHERE id = ?";
        $this->contentieux->query($query, array($nom, $id));
    }

    public function supprimer_document($id) {
        $query = "DELETE FROM edition WHERE document_id= ?";
        $this->contentieux->query($query, array($id));

        $query = "DELETE FROM document WHERE id= ?";
        $this->contentieux->query($query, array($id));
    }

    public function get_asso_cat_types() {
        return $this->contentieux->query("SELECT * FROM asso_categorie_type_doc")->result_array();
    }

    public function ajouter(Document_dto $doc) {
        $query = "INSERT INTO document (sinistre_id, nom, uri, ext, size, upload_date, date, date_reception, affaire_id, nom_public, num_type)
                  VALUES (?,?,?,?,?,getDate(),?,?,?,?,?)";

        $this->contentieux->query($query, array(
            $doc->sinistre_id,
            $doc->nom,
            $doc->uri,
            $doc->ext,
            $doc->size,
            $doc->date->format("d-m-Y"),
            $doc->date_reception->format("d-m-Y"),
            $doc->affaire_id,
            $doc->nom_public,
            $doc->num_type
        ));

        return $this->contentieux->insert_id();
    }

    public function get_type($num) {
        if ($num >= 7 && $num <= 18) {
            $num = 7;
        }
        $query = "SELECT * FROM type_document WHERE num = ?";
        return $this->contentieux->query($query, array($num))->row();
    }

    public function get_all_docs($sinistre) {

        $affaire = $sinistre->getAffaire();

        $docs_sinistre = $this->get_docs_sinistre($sinistre->getId());
        $docs_affaire = $this->get_docs_affaire($affaire->getId());
        $docs_gaia = $this->get_docs_gaia($affaire->getId_g_affaire());
        $docs_qp = $this->get_docs_qp($affaire->getId_qp_contrat());

        $docs = array_merge($docs_sinistre, $docs_affaire, $docs_gaia, $docs_qp);

        return $docs;
    }

    public function changer_uri($id, $uri) {
        $query = "UPDATE document SET uri = ? WHERE id = ?";
        $this->contentieux->query($query, array($uri, $id));
    }

    public function lier_document($id, $affaire_id = null, $sinistre_id = null) {
        if ($affaire_id) {
            $query = "UPDATE document SET sinistre_id = NULL, affaire_id = ? WHERE id = ?";
            $this->contentieux->query($query, array($affaire_id, $id));
            return;
        }
        if ($sinistre_id) {
            $query = "UPDATE document SET affaire_id = NULL, sinistre_id = ? WHERE id = ?";
            $this->contentieux->query($query, array($sinistre_id, $id));
        }
    }

}
