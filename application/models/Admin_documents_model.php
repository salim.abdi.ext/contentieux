<?php

Class Admin_documents_model extends Dao {

    public function __construct() {
        parent::__construct();
    }

    public function get_categories_docs() {
        $query = "SELECT * FROM categorie_doc";
        return $this->contentieux->query($query)->result_array();
    }

    public function update_categorie_doc($id, $col, $val) {
        $query = "UPDATE categorie_doc SET $col = ? WHERE id = ?";
        $this->contentieux->query($query, array($val, $id));
    }

    public function del_categorie_doc($id) {

        $query_unbind = "UPDATE document SET categorie_id = NULL where categorie_id = ?";
        $this->contentieux->query($query_unbind, array($id));

        $query_unbind = "UPDATE type_document SET categorie_id = NULL where categorie_id = ?";
        $this->contentieux->query($query_unbind, array($id));

        $query = "DELETE FROM categorie_doc WHERE id = ?";
        $this->contentieux->query($query, array($id));
    }

    public function add_categorie_doc() {

        $num = $this->contentieux->query("SELECT max(num) + 1 as num FROM categorie_doc")->row()->num;
        if ($num == null) {
            $num = 1;
        }

        $query = "INSERT INTO categorie_doc (libelle, num) VALUES ('',$num);";
        $this->contentieux->query($query);

        $id = $this->contentieux->insert_id();

        return array("id" => $id, "num" => $num);
    }

    public function get_types_docs() {
        $query = "SELECT id, type, num FROM type_document t";
        $types = $this->contentieux->query($query)->result_array();

        foreach ($types as &$type) {
            $query = "SELECT a.id as id_asso, * FROM asso_categorie_type_doc a left join categorie_doc c ON c.num = a.num_categorie WHERE a.num_type = ?";
            $type["categories"] = $this->contentieux->query($query, array($type["num"]))->result_array();
        }
        return $types;
    }

    public function lier_type_categorie($num_type, $num_categorie) {
        $query = "INSERT INTO asso_categorie_type_doc (num_type, num_categorie) VALUES (?,?)";
        $this->contentieux->query($query, array($num_type, $num_categorie));
    }

    public function supprimer_asso_type_cat($id_asso) {
        $query = "DELETE FROM asso_categorie_type_doc WHERE id = ?";
        $this->contentieux->query($query, array($id_asso));
    }

    public function change_libelle_type_doc($id, $libelle) {
        $query = "UPDATE type_document SET type = ? WHERE id =?";
        $this->contentieux->query($query, array($libelle, $id));
    }

    public function del_type($id) {
        $q1 = "DELETE FROM asso_categorie_type_doc WHERE id_type = $id";
        $this->contentieux->query($q1);

        $q2 = "DELETE FROM type_document WHERE id = $id";
        $this->contentieux->query($q2);
    }

    public function add_type_doc() {
        $query = "INSERT INTO type_document (type, num) VALUES ('', (SELECT isNull(max(num) + 1,1) FROM type_document))";
        $this->contentieux->query($query);
        return $this->contentieux->insert_id();
    }

    public function change_num_type_doc($id, $num) {
        $query = "UPDATE type_document SET num =? WHERE id = ?";
        $this->contentieux->query($query, array($num, $id));
    }

    public function rechercher_doublons() {

        $types_affaire = array(20, 0, 1, 2, 3, 4, 5, 6, 21, 7, 19, 110, 22, 111);

        ini_set("client_buffer_max_kb_size", '50240');
        ini_set("sqlsrv.ClientBufferMaxKBSize", '50240');

// #1 Recup toutes les lignes avec type
        $q1 = "SELECT d.id as id_document, d.* FROM document d LEFT JOIN type_document td ON td.num = d.num_type WHERE d.num_type IN (20, 0, 1, 2, 3, 4, 5, 6, 21, 7, 19, 110, 22, 111);";
        $r1 = $this->contentieux->query($q1)->result_array();

        $ids_traites = array();
        $results = array();

        foreach ($r1 as &$ligne) {
            if (in_array($ligne["num_type"], $types_affaire)) {
                $ligne["affaire"] = true;
            }
            if (!file_exists($ligne["uri"])) {
                $ligne["missing_file"] = true;
            }
            if (in_array($ligne["id"], $ids_traites)) {
                continue;
            }
            if ($ligne["sinistre_id"] > 0) {
                if ($ligne["affaire_id"] == null) {
                    $q_aff_id = "SELECT affaire_id FROM sinistre WHERE id = ?";
                    $ligne["affaire_id"] = $this->contentieux->query($q_aff_id, array($ligne["sinistre_id"]))->row()->affaire_id;
                }
            }
            $query2 = "SELECT d.id as id_document, * FROM document d left join type_document td on td.num = d.num_type WHERE d.num_type IN (20, 0, 1, 2, 3, 4, 5, 6, 21, 7, 19, 110, 22, 111) AND d.size = ? AND d.id <> ? AND (d.sinistre_id = ? OR d.affaire_id = ?)";
            $r_doublons = $this->contentieux->query($query2, array($ligne["size"], $ligne["id"], $ligne["sinistre_id"], $ligne["affaire_id"]))->result_array();
            if (count($r_doublons) > 0) {
                array_unshift($r_doublons, $ligne);
                foreach ($r_doublons as &$elem) {
                    $elem["nb_historique"] = $this->trouver_histo_doc($elem["id"]);
                    $ids_traites[] = $elem["id"];
                    if (in_array($elem["num_type"], $types_affaire)) {
                        $elem["affaire"] = true;
                    }
                    if (!file_exists($elem["uri"])) {
                        $elem["missing_file"] = true;
                    }
                }
                $results[] = $r_doublons;
            } else {
                $ids_traites[] = $ligne["id"];
            }
        }

        return $results;
    }

    public function trouver_histo_doc($id) {
        return $this->contentieux->query("SELECT count(*) as cnt FROM asso_historique_document WHERE id_document = '$id'")->row()->cnt;
    }

    public function remplir_asso_histo_doc() {

        ini_set("client_buffer_max_kb_size", '50240');
        ini_set("sqlsrv.ClientBufferMaxKBSize", '50240');

        //truncate
        $q_truncate = "TRUNCATE TABLE asso_historique_document";
        $this->contentieux->query($q_truncate);

        $query_histo = "SELECT * FROM historique";
        $r_histo = $this->contentieux->query($query_histo)->result_array();
        foreach ($r_histo as $row_histo) {
            $data = unserialize($row_histo["data"]);
            @$pjs = $data["attachements"];
            echo $row_histo["id"] . "<br>";
            if ($pjs && count($pjs) > 0) {
                foreach ($pjs as $pj) {
                    $q = "INSERT INTO asso_historique_document (id_historique, id_document) VALUES (?,?)";
                    $this->contentieux->query($q, array($row_histo["id"], $pj));
                }
            }
        }
    }

    public function fix_serialized() {


        ini_set("client_buffer_max_kb_size", '50240');
        ini_set("sqlsrv.ClientBufferMaxKBSize", '50240');
        // header("Content-type:Text/plain");
        $query = "SELECT * FROM historique WHERE data IS NOT NULL";
        $hist = $this->contentieux->query($query)->result_array();
        foreach ($hist as $row) {
            @$data = $row["data"];
            if ($data && @unserialize($data) == null) {

                echo $row["id"] . "<br><br>";

                $test = preg_replace_callback(
                        '/s\:(\d+)\:\"(.*?)\";/s', function ($matches) {
                    return 's:' . strlen($matches[2]) . ':"' . $matches[2] . '";';
                }, $data)
                ;
                if (@unserialize($test) != null) {
                    var_dump(unserialize($test));
                    $query = "UPDATE historique SET data = ? WHERE id = ?";
                    $this->contentieux->query($query, array($test, $row["id"]));
                }

                // $this->findSerializeError($data);

                echo "<br>";
                echo "=========================================================================================================";
                echo "<br>";
            }
        }
    }

}
