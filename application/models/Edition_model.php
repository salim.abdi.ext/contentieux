<?php

class Edition_model extends Dao {

    public function __construct() {
        parent::__construct();
    }

    public function get_row_edition($id) {
        $query = "SELECT * FROM edition WHERE id = ?";
        return $this->contentieux->query($query, array($id))->row();
    }

    public function changer_etat($id, $etat) {
        $query = "UPDATE edition SET etat = ? WHERE id = ?";
        $this->contentieux->query($query, array($etat, $id));
    }

    public function set_document_id($id, $document_id) {
        $query = "UPDATE edition SET document_id = ? WHERE id = ?";
        $this->contentieux->query($query, array($document_id, $id));
    }

}
