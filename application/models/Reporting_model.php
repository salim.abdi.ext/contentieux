<?php

class Reporting_model extends Dao {

    public function __construct() {
        parent::__construct();
    }

    public function get_regions() {
        $query = "SELECT DISTINCT entites.nom, entites.id_entite FROM services s
                INNER JOIN association asso ON s.id_service = asso.id_service
                JOIN entites ON asso.id_entite = entites.id_entite
                WHERE id_societe = 1
                AND (date_cloture IS NULL OR date_cloture > getDate())
                AND flag_centre_profit = 1
                AND tmp_cle_unique IN (1, 5, 7)
                AND code_gepat != ''
                AND entites.id_type_entite = 3
                AND entites.id_entite NOT IN (316, 319, 515)
                ORDER BY nom ASC";

        $r = $this->qualiperf->query($query)->result_array();

        return $r;
    }

    public function get_services() {
        $query = "SELECT s.id_service, s.nom, e.id_entite
                FROM services s
                INNER JOIN association a ON a.id_service = s.id_service
                INNER JOIN entites e ON (e.id_entite = a.id_entite and e.id_type_entite = 3)
                WHERE id_societe = 1
                AND (date_cloture is null or date_cloture > getDate())
                and flag_centre_profit = 1
                and tmp_cle_unique IN (1,5,7)
                and code_GEPAT != ''
                order by nom";

        $services = $this->qualiperf->query($query)->result_array();

        return $services;
    }

    public function get_nom_region($id_entite) {
        $query = "SELECT nom FROm entites WHERE id_entite = ?";
        return $this->qualiperf->query($query, array($id_entite))->row()->nom;
    }
    
    public function get_nom_service($id_service) {
        $query = "SELECT nom FROM services WHERE id_service = ?";
        return $this->qualiperf->query($query, array($id_service))->row()->nom;
    }

    public function get_reporting($annee, $portee) {
        
        $split = explode("_", $portee);

        if ($portee == "global") {
        $query = "with sinistres as (SELECT s.* FROM sinistre s WHERE flag_sinistre_desactive = 2 AND sinistre_clos_date LIKE '$annee-%') ";
        } else if ($split[0] == "r") {
            $id_region = $this->qualiperf->escape($split[1]);
            $query = "with sinistres as (SELECT s.* FROM sinistre s INNER JOIN affaire a ON a.id = s.affaire_id WHERE flag_sinistre_desactive = 2 AND a.id_region = $id_region  AND sinistre_clos_date LIKE '$annee-%') ";
        } else if ($split[0] == "sv") {
            $id_service = $this->qualiperf->escape($split[1]);
            $query = "with sinistres as (SELECT s.* FROM sinistre s INNER JOIN affaire a ON a.id = s.affaire_id WHERE flag_sinistre_desactive = 2 AND a.id_service_contentieux = $id_service  AND sinistre_clos_date LIKE '$annee-%') ";
        }
        
        $query .= "SELECT
            
                 --- synthese
                 count(*) as synthese_nsc, 
                 SUM(sinistre_clos_cout) as synthese_cts, 
                 SUM(sinistre_clos_cout_qc) as synthese_pqc,
                 
                 --- destination
                 (SELECT count(*) FROM sinistres WHERE sinistre_clos_destination_ouvrage = 'hi') as dest_hi_nsc,
                 (SELECT count(*) FROM sinistres WHERE sinistre_clos_destination_ouvrage = 'hc') as dest_hc_nsc,
                 (SELECT count(*) FROM sinistres WHERE sinistre_clos_destination_ouvrage = 'la') as dest_la_nsc,
                 (SELECT SUM(sinistre_clos_cout) FROM sinistres WHERE sinistre_clos_destination_ouvrage = 'hi') as dest_hi_cts,
                 (SELECT SUM(sinistre_clos_cout) FROM sinistres WHERE sinistre_clos_destination_ouvrage = 'hc') as dest_hc_cts,
                 (SELECT SUM(sinistre_clos_cout) FROM sinistres WHERE sinistre_clos_destination_ouvrage = 'la') as dest_la_cts,
                 
                 --- missions
                 (SELECT count(*) FROM sinistres WHERE sinistre_clos_mission = 'L+LP') as mis_l_nsc,
                 (SELECT SUM(sinistre_clos_cout) FROM sinistres WHERE sinistre_clos_mission = 'L+LP') as mis_l_cts,
                 
                 (SELECT count(*) FROM sinistres WHERE sinistre_clos_mission = 'LE') as mis_le_nsc,
                 (SELECT SUM(sinistre_clos_cout) FROM sinistres WHERE sinistre_clos_mission = 'LE') as mis_le_cts,
                 
                 (SELECT count(*) FROM sinistres WHERE sinistre_clos_mission = 'AV') as mis_av_nsc,
                 (SELECT SUM(sinistre_clos_cout) FROM sinistres WHERE sinistre_clos_mission = 'AV') as mis_av_cts,
                 
                 (SELECT count(*) FROM sinistres WHERE sinistre_clos_mission = 'PS') as mis_ps_nsc,
                 (SELECT SUM(sinistre_clos_cout) FROM sinistres WHERE sinistre_clos_mission = 'PS') as mis_ps_cts,
                 
                 (SELECT count(*) FROM sinistres WHERE sinistre_clos_mission = 'SH+SEI+STI+ENV') as mis_sh_nsc,
                 (SELECT SUM(sinistre_clos_cout) FROM sinistres WHERE sinistre_clos_mission = 'SH+SEI+STI+ENV') as mis_sh_cts,
                 
                 (SELECT count(*) FROM sinistres WHERE sinistre_clos_mission = 'HAND') as mis_hand_nsc,
                 (SELECT SUM(sinistre_clos_cout) FROM sinistres WHERE sinistre_clos_mission = 'HAND') as mis_hand_cts,
                 
                 (SELECT count(*) FROM sinistres WHERE sinistre_clos_mission = 'BRD') as mis_brd_nsc,
                 (SELECT SUM(sinistre_clos_cout) FROM sinistres WHERE sinistre_clos_mission = 'BRD') as mis_brd_cts,
                 
                 (SELECT count(*) FROM sinistres WHERE sinistre_clos_mission = 'PHA+PHH') as mis_pha_nsc,
                 (SELECT SUM(sinistre_clos_cout) FROM sinistres WHERE sinistre_clos_mission = 'PHA+PHH') as mis_pha_cts,
                 
                 (SELECT count(*) FROM sinistres WHERE sinistre_clos_mission = 'TH') as mis_th_nsc,
                 (SELECT SUM(sinistre_clos_cout) FROM sinistres WHERE sinistre_clos_mission = 'TH') as mis_th_cts,
                 
                 (SELECT count(*) FROM sinistres WHERE sinistre_clos_mission = 'F') as mis_f_nsc,
                 (SELECT SUM(sinistre_clos_cout) FROM sinistres WHERE sinistre_clos_mission = 'F') as mis_f_cts,
                 
                 (SELECT count(*) FROM sinistres WHERE sinistre_clos_mission = 'HYSa+HYSh') as mis_hysa_nsc,
                 (SELECT SUM(sinistre_clos_cout) FROM sinistres WHERE sinistre_clos_mission = 'HYSa+HYSh') as mis_hysa_cts,
                 
                 (SELECT count(*) FROM sinistres WHERE sinistre_clos_mission = 'GTB') as mis_gtb_nsc,
                 (SELECT SUM(sinistre_clos_cout) FROM sinistres WHERE sinistre_clos_mission = 'GTB') as mis_gtb_cts,
                 
                 (SELECT count(*) FROM sinistres WHERE sinistre_clos_mission = 'Autres') as mis_autres_nsc,
                 (SELECT SUM(sinistre_clos_cout) FROM sinistres WHERE sinistre_clos_mission = 'Autres') as mis_autres_cts,
                 
                 -- Types ouvrage
                 
                 (SELECT count(*) FROM sinistres WHERE sinistre_clos_partie_ouvrage = 'fondations') as part_fondations_nsc,
                 (SELECT SUM(sinistre_clos_cout) FROM sinistres WHERE sinistre_clos_partie_ouvrage = 'fondations') as part_fondations_cts,
                 
                 (SELECT count(*) FROM sinistres WHERE sinistre_clos_partie_ouvrage = 'structure_charpente') as part_struct_nsc,
                 (SELECT SUM(sinistre_clos_cout) FROM sinistres WHERE sinistre_clos_partie_ouvrage = 'structure_charpente') as part_struct_cts,
                 
                 (SELECT count(*) FROM sinistres WHERE sinistre_clos_partie_ouvrage = 'couverture_etancheite') as part_couverture_nsc,
                 (SELECT SUM(sinistre_clos_cout) FROM sinistres WHERE sinistre_clos_partie_ouvrage = 'couverture_etancheite') as part_couverture_cts,
                 
                 (SELECT count(*) FROM sinistres WHERE sinistre_clos_partie_ouvrage = 'facades') as part_facades_nsc,
                 (SELECT SUM(sinistre_clos_cout) FROM sinistres WHERE sinistre_clos_partie_ouvrage = 'facades') as part_facades_cts,
                 
                 (SELECT count(*) FROM sinistres WHERE sinistre_clos_partie_ouvrage = 'menuiseries') as part_menuiseries_nsc,
                 (SELECT SUM(sinistre_clos_cout) FROM sinistres WHERE sinistre_clos_partie_ouvrage = 'menuiseries') as part_menuiseries_cts,
                 
                 (SELECT count(*) FROM sinistres WHERE sinistre_clos_partie_ouvrage = 'partitions') as part_partitions_nsc,
                 (SELECT SUM(sinistre_clos_cout) FROM sinistres WHERE sinistre_clos_partie_ouvrage = 'partitions') as part_partitions_cts,
                 
                 (SELECT count(*) FROM sinistres WHERE sinistre_clos_partie_ouvrage = 'equipements') as part_equipements_nsc,
                 (SELECT SUM(sinistre_clos_cout) FROM sinistres WHERE sinistre_clos_partie_ouvrage = 'equipements') as part_equipements_cts,
                 
                 (SELECT count(*) FROM sinistres WHERE sinistre_clos_partie_ouvrage = 'vrd') as part_vrd_nsc,
                 (SELECT SUM(sinistre_clos_cout) FROM sinistres WHERE sinistre_clos_partie_ouvrage = 'vrd') as part_vrd_cts,
                 
                 (SELECT count(*) FROM sinistres WHERE sinistre_clos_partie_ouvrage = 'autres') as part_autres_nsc,
                 (SELECT SUM(sinistre_clos_cout) FROM sinistres WHERE sinistre_clos_partie_ouvrage = 'autres') as part_autres_cts,
                 
                 -- phase
                 
                 (SELECT count(*) FROM sinistres WHERE sinistre_clos_phase_concernee = 'conception') as phase_conception_nsc,
                 (SELECT SUM(sinistre_clos_cout) FROM sinistres WHERE sinistre_clos_phase_concernee = 'conception') as phase_conception_cts,
                 
                 (SELECT count(*) FROM sinistres WHERE sinistre_clos_phase_concernee = 'execution') as phase_execution_nsc,
                 (SELECT SUM(sinistre_clos_cout) FROM sinistres WHERE sinistre_clos_phase_concernee = 'execution') as phase_execution_cts,
                 
                 (SELECT count(*) FROM sinistres WHERE sinistre_clos_phase_concernee = 'incident') as phase_incident_nsc,
                 (SELECT SUM(sinistre_clos_cout) FROM sinistres WHERE sinistre_clos_phase_concernee = 'incident') as phase_incident_cts,
                 
                 (SELECT count(*) FROM sinistres WHERE sinistre_clos_phase_concernee = 'materiau') as phase_materiau_nsc,
                 (SELECT SUM(sinistre_clos_cout) FROM sinistres WHERE sinistre_clos_phase_concernee = 'materiau') as phase_materiau_cts,
                 
                 (SELECT count(*) FROM sinistres WHERE sinistre_clos_phase_concernee = 'exploitation') as phase_exploitation_nsc,
                 (SELECT SUM(sinistre_clos_cout) FROM sinistres WHERE sinistre_clos_phase_concernee = 'exploitation') as phase_exploitation_cts,
                 
                 (SELECT count(*) FROM sinistres WHERE sinistre_clos_phase_concernee = 'autres') as phase_autres_nsc,
                 (SELECT SUM(sinistre_clos_cout) FROM sinistres WHERE sinistre_clos_phase_concernee = 'autres') as phase_autres_cts
                 
                 ";
        
                $query .= " FROM sinistres";
        
                return $this->contentieux->query($query)->row_array();
    }

}
