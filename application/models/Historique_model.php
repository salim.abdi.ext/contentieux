<?php

class Historique_model extends Dao {

    public function __construct() {
        parent::__construct();
    }

    public function add_row(Historique_dto $historique) {
        $query = "INSERT INTO historique (sinistre_id, statut_id, type, date,data,num_chrono, libelle, type_mail) VALUES (?,?,?,?,?,?,?,?)";
        $this->contentieux->query($query, array(
            $historique->sinistre_id,
            $historique->statut_id,
            $historique->type,
            $historique->date,
            $historique->data,
            $historique->num_chrono,
            $historique->libelle,
            $historique->type_mail
        ));
    }

    public function get_next_chrono($sinistre_id) {
        $query = "SELECT isNull(MAX(num_chrono) + 1,1) as num_chrono FROM historique WHERE sinistre_id = ?";
        return $this->contentieux->query($query, array($sinistre_id))->row()->num_chrono;
    }

}
