<?php

class Tableau_de_bord_model extends Dao {

    public function __construct() {
        parent::__construct();
    }

    public function getList($id_utilisateur) {
        $query = "SELECT * FROM tableau_de_bord WHERE id_collaborateur = $id_utilisateur";

        $r = $this->contentieux->query($query)->result_array();

        return $r;
    }

    public function add_element($id_sinistre, $id_collaborateur, $date_maj, $libelle) {
        $query = "INSERT INTO tableau_de_bord (id_sinistre, id_collaborateur, date_maj, libelle) VALUES (?, ?, ?, ?)";
        $this->contentieux->query($query, array($id_sinistre, $id_collaborateur, $date_maj, $libelle));
    }
}
