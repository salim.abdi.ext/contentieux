<?php

class Contentieux extends CI_Model {
    
    public $database;
    
    public function __construct() {
        parent::__construct();
        $this->database = $this->load->database('contentieux', true);
    }
}
