<?php

use Entities\Affaire;
use Entities\Adresse;

class ImportAffaire extends CI_Model {

    private $gaia;
    private $qualiperf;

    public function __construct() {
        parent::__construct();
        $this->gaia = $this->load->database('gaia', true);
        $this->qualiperf = $this->load->database('qualiperf', true);
    }

    public function getId_agence($id_service) {
        
        $query = 'SELECT id_Agence FROM vue_UO_Service WHERE id_service_QP = ' . $id_service;

        $r = $this->qualiperf->query($query)->result_array();

        if (isset($r[0]))
            return $r[0]["id_Agence"];
        else
            return false;
    }

    public function getLoc($id_ca) {

        $query = <<<SQL
                SELECT DISTINCT s.id_service as aff_id_service, u.prenom + ' ' + u.nom as aff_collaborateur_ca,
                s.nom as aff_nom_service, e.id_entite as aff_id_region, e.nom as aff_nom_region,
                (SELECT TOP 1 e.id_entite
                from utilisateurs u
                LEFT JOIN utilisateurs_services us on u.id_utilisateur = us.id_utilisateur AND us.ordre = 1
                LEFT JOIN services s on s.id_service = us.id_service
                LEFT JOIN association a ON s.id_service = a.id_service
                LEFT JOIN entites e ON a.id_entite = e.id_entite
                WHERE u.id_utilisateur = '$id_ca' AND e.id_type_entite = 1
                ) as aff_id_agence
                from utilisateurs u
                LEFT JOIN utilisateurs_services us on u.id_utilisateur = us.id_utilisateur AND us.ordre = 1
                LEFT JOIN services s on s.id_service = us.id_service
                LEFT JOIN association a ON s.id_service = a.id_service
                LEFT JOIN entites e ON a.id_entite = e.id_entite
                WHERE u.id_utilisateur = '$id_ca'
                AND e.id_type_entite = 3
SQL;

        $result = $this->qualiperf->query($query)->result();

        if (isset($result[0]))
            return $result[0];

        return false;
    }

    public function checkInQualiperf($num_affaire) {
        $query = <<<SQL
                SELECT DISTINCT num_contrat as code, libelle_avenant as libelle, u.prenom + ' ' + u.nom as ca, liste_missions as missions,
                s.adresse_1 as rue1, s.adresse_2 as rue2, s.adresse_3 as rue3, s.code_postal as cp, s.ville as commune
                FROM contrats
                LEFT JOIN utilisateurs u ON u.id_utilisateur = id_responsable_affaire
                LEFT JOIN affaires_sites_missions asm ON contrats.id_contrat = asm.id_contrat
                LEFT JOIN affaires_sites ON asm.id_affaire_site = affaires_sites.id_site
                LEFT JOIN sites s ON affaires_sites.id_site = s.id_site
                WHERE num_contrat LIKE '%$num_affaire'
SQL;
        return $this->qualiperf->query($query)->result();
    }

    public function importQualiperf($num_affaire) {

        $query = <<<SQL
                SELECT DISTINCT num_contrat as aff_code, libelle_avenant as aff_libelle, id_service_producteur as aff_id_service, id_responsable_affaire as aff_id_collaborateur_ca,
                u.prenom + ' ' + u.nom as aff_collaborateur_ca, liste_missions as aff_desc_missions, contrats.id_contrat as aff_q_id_contrat, s.adresse_1 as aff_rue1, s.adresse_2 as aff_rue2,
                s.adresse_3 as aff_rue3, s.code_postal as aff_code_postal, s.ville as aff_commune, s.id_site as aff_id_site, sv.nom as aff_nom_service, e.id_entite as aff_id_region,
                e.nom as aff_nom_region, sv.analytique_SAGE as sage, contrats.id_affaire,
                (
                SELECT TOP 1 e.id_entite
                from contrats
                LEFT JOIN association a ON id_service_producteur = a.id_service and a.type_association != 1
                LEFT JOIN entites e ON a.id_entite = e.id_entite
                WHERE num_contrat = '$num_affaire'
                 AND e.id_type_entite = 1
                )
                as aff_id_agence
                from contrats
                LEFT JOIN utilisateurs u ON u.id_utilisateur = id_responsable_affaire
                LEFT JOIN affaires_sites_missions asm ON contrats.id_contrat = asm.id_contrat
                LEFT JOIN affaires_sites ON asm.id_affaire_site = affaires_sites.id_affaire_site
                LEFT JOIN sites s ON affaires_sites.id_site = s.id_site
                LEFT JOIN services sv ON id_service_producteur = sv.id_service
                LEFT JOIN association a ON sv.id_service = a.id_service
                LEFT JOIN entites e ON a.id_entite = e.id_entite
                WHERE num_contrat = '$num_affaire' --AND e.id_type_entite = 3
SQL;
        $result = $this->qualiperf->query($query)->result();
        if (isset($result[0])) {
            $affaire = $this->mapToEntity($result[0]);
            $affaire = $this->extraFromGaia($affaire);
            return $affaire;
        }
        return false;
    }

    private function extraFromGaia(Affaire $affaire) {

        $id_q = $affaire->getId_qp_contrat();

        $query = <<<SQL
                SELECT id_affaire FROM dbo.gaia_affaire a
                WHERE aff_q_id_contrat = '$id_q'
SQL;
        $result = $this->gaia->query($query)->result();
        if (isset($result[0]))
            $affaire->setId_g_affaire($result[0]->id_affaire);

        return $affaire;
    }

    public function updateAffaire($affaire) {

        $id_contrat = $affaire->getId_qp_contrat();

        $query = <<<SQL
                SELECT DISTINCT num_contrat as aff_code, libelle_avenant as aff_libelle, id_service_producteur as aff_id_service, id_responsable_affaire as aff_id_collaborateur_ca,
                u.prenom + ' ' + u.nom as aff_collaborateur_ca, liste_missions as aff_desc_missions, contrats.id_contrat as aff_q_id_contrat, s.adresse_1 as aff_rue1, s.adresse_2 as aff_rue2,
                s.adresse_3 as aff_rue3, s.code_postal as aff_code_postal, s.ville as aff_commune, s.id_site as aff_id_site, sv.nom as aff_nom_service, e.id_entite as aff_id_region,
                e.nom as aff_nom_region, sv.analytique_SAGE as sage, contrats.id_affaire,
                (
                SELECT e.id_entite
                from contrats
                LEFT JOIN association a ON id_service_producteur = a.id_service and a.type_association != 1
                LEFT JOIN entites e ON a.id_entite = e.id_entite
                WHERE contrats.id_contrat = '$id_contrat'
                AND e.id_type_entite = 1
                )
                as aff_id_agence
                from contrats
                LEFT JOIN utilisateurs u ON u.id_utilisateur = id_responsable_affaire
                LEFT JOIN affaires_sites_missions asm ON contrats.id_contrat = asm.id_contrat
                LEFT JOIN affaires_sites ON asm.id_affaire_site = affaires_sites.id_affaire_site
                LEFT JOIN sites s ON affaires_sites.id_site = s.id_site
                LEFT JOIN services sv ON id_service_producteur = sv.id_service
                LEFT JOIN association a ON sv.id_service = a.id_service
                LEFT JOIN entites e ON a.id_entite = e.id_entite
                WHERE contrats.id_contrat = '$id_contrat' AND e.id_type_entite = 3
SQL;
        $result = $this->qualiperf->query($query)->result();
        if (isset($result[0])) {
            $affaire = $this->mapToEntity($result[0], $affaire);
            $affaire = $this->extraFromGaia($affaire);
            if ($affaire->getId_service_contentieux()) {
                $id_agence = $this->getId_agence($affaire->getId_service_contentieux());
                $affaire->setId_agence($id_agence);
            }
            $ci = &get_instance();
            $ci->doctrine->em->persist($affaire);
            $ci->doctrine->em->flush();
        }
    }

    private function mapToEntity($import_affaire, $affaire = null) {
        if ($affaire == null) {
            $affaire = new Affaire();
            $adresse = new Entities\Adresse();
        } else {
            $adresse = $affaire->getAdresse();
        }

        $adresse->setRue1($import_affaire->aff_rue1);
        $adresse->setRue2($import_affaire->aff_rue2);
        $adresse->setRue3($import_affaire->aff_rue3);
        $adresse->setCp($import_affaire->aff_code_postal);
        $adresse->setCommune($import_affaire->aff_commune);

        $affaire->setAdresse($adresse);

        $affaire->setCode($this->formatCode($import_affaire->aff_code));
        $affaire->setLibelle($import_affaire->aff_libelle);
        $affaire->setId_service($import_affaire->aff_id_service);
        $affaire->setId_ca($import_affaire->aff_id_collaborateur_ca);
        $affaire->setCa($import_affaire->aff_collaborateur_ca);
        $affaire->setMissions(str_replace(",", ", ", $import_affaire->aff_desc_missions));
        $affaire->setId_qp_contrat($import_affaire->aff_q_id_contrat);
        $affaire->setNom_service($import_affaire->aff_nom_service);
        $affaire->setId_region($import_affaire->aff_id_region);
        $affaire->setNom_region($import_affaire->aff_nom_region);
        $affaire->setId_agence($import_affaire->aff_id_agence);
        $affaire->setSage($import_affaire->sage);
        $affaire->setId_operation($import_affaire->id_affaire);

        return $affaire;
    }

    public function getDocs(Affaire &$affaire) {
        $num_affaire = $affaire->getCode();
        if (strpos($num_affaire, "AVT")) {
            $num_affaire = substr($num_affaire, 0, strpos($num_affaire, "AVT"));
            $num_avt = substr($num_affaire, strpos($num_affaire, "AVT") + 3);
        }

        $query = <<<SQL
                SELECT * FROM dbo.gaia_rapport r
                INNER JOIN gaia_affaire a on a.id_affaire = r.rap_id_affaire
                WHERE a.aff_code = '$num_affaire'
                AND r.rap_type IN (1,2,4,6)
SQL;

        $results = $this->gaia->query($query)->result();
        $types = array(0 => "Avis sur conception", 1 => "RICT", 2 => "BRED", 3 => "CR visite", 4 => "Liste recapitulative", 5 => "Pré RFCT", 6 => "RFCT", 7 => "Attestation parasismique stade PC", 8 => "Attestation parasismique finale", 9 => "ATTHAND2", 15 => "Rapport HAND2015", 16 => "attestation HAND2015");
        foreach ($results as $result) {
            $file = new Entities\File();
            $file->setNom($result->rap_filename . ".pdf");
            $file->setExt("pdf");
            $file->setUpload_date(date_create($result->rap_date_rapport));
            $file->setType($types[$result->rap_type]);
            $file->setUri("http://gaia.qualigroup.net/gaia/DownloadRapport?get=" . $result->rap_publicKey);

            $affaire->addDocument($file);
        }
    }

    private function formatCode($num_affaire) {
        $avt = "";
        if (strpos($num_affaire, "AVT")) {
            $avt = substr($num_affaire, strpos($num_affaire, "AVT"));
            $num_affaire = substr($num_affaire, 0, strpos($num_affaire, "AVT"));
        }
        if (strlen($num_affaire) > 12) {
            $num_affaire = ltrim($num_affaire, "0");
            $num_affaire = str_pad($num_affaire, 12, "0", STR_PAD_LEFT);
        }

        return $num_affaire . $avt;
    }

    public function getAffairesOp($affaire) {
        $id_operation = $affaire->getId_operation();
        $query = "select id_contrat, num_contrat from contrats where id_affaire = '$id_operation' and date_passage_sans_suite is null";

        $result = $this->qualiperf->query($query)->result();
        $affaires_op = array();
        foreach ($result as $aff) {
            $aff_mappee = null;
            if ($aff->id_contrat != $affaire->getId_qp_contrat()) {
                $aff_mappee = $this->importQualiperf($aff->num_contrat);
                $affaires_op[] = clone $aff_mappee;
            }
        }
        return $affaires_op;
    }

    public function getTranches($affaire) {
        $id_aff = $affaire->getId_g_affaire();

        $query = <<<SQL
            select id_tranche, tra_index, tra_libelle, tra_stade,
            replace(
            replace(
            replace(
            (
                select mis_id_mission
                from gaia_mission
                where mis_id_tranche = id_tranche
                for xml path('')
            ),'</mis_id_mission><mis_id_mission>',','), '<mis_id_mission>', ''), '</mis_id_mission>','')  as missions
            from gaia_tranche
            where tra_id_affaire = '$id_aff'
SQL;
        $result = $this->gaia->query($query)->result();

        foreach ($result as $tranche) {
            $missions = $tranche->missions;
            $missions_array = explode(",", $missions);
            $missions = $this->getMission($missions_array);
            $missions_str = implode(", ", $missions);
            $tranche->missions = $missions_str;
        }

        return $result;
    }

    public function getMission($ids) {
        if (!is_array($ids))
            $ids = array($ids);
        $missions = array();
        foreach ($ids as $id) {
            $query = "select abrege from missions where id_mission = '$id'";
            $result = $this->qualiperf->query($query)->result();
            $missions[] = $result[0]->abrege;
        }
        return $missions;
    }
    
    public function get_region($id_service) {
        $query = "SELECT id_Region, Region FROM vue_UO_Service WHERE id_service_QP = ?";
        return $this->qualiperf->query($query, array($id_service))->row_array();
    }

}
