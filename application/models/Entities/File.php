<?php

namespace Entities;

/**
 * @Entity(repositoryClass="Entities\FileRepository")
 * @Table(name="document")
 */
class File {

    /**
     * @Column(type="integer", length=32, unique=true, nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Column(type="string", length=150, unique=false, nullable=true)
     */
    private $nom;

    /**
     * @Column(type="string", length=300, unique=false, nullable=true)
     */
    private $nom_public;

    /**
     * @Column(type="integer", length=32, nullable=false)
     */
    private $num_type;

    /**
     * @Column(type="string", length=200, unique=false, nullable=false)
     */
    private $uri;

    /**
     * @Column(type="string", length=32, unique=false, nullable=true)
     */
    private $ext;

    /**
     * @Column(type="integer", length=32, unique=false, nullable=true)
     */
    private $size;

    /**
     * @Column(type="date", length=32, unique=false, nullable=false)
     */
    private $upload_date;

    /**
     * @Column(type="date", length=32, unique=false, nullable=true)
     */
    private $date;

    /**
     * @Column(type="date", length=32, unique=false, nullable=true)
     */
    private $date_reception;

    /**
     * @ManyToOne(targetEntity="Entities\Statut", inversedBy="documents", fetch="LAZY")
     */
    private $statut;

    /**
     * @ManyToOne(targetEntity="Entities\Sinistre", inversedBy="documents", fetch="LAZY")
     */
    private $sinistre;

    /**
     * @ManyToOne(targetEntity="Entities\Affaire", inversedBy="documents", fetch="LAZY")
     */
    private $affaire;

    /**
     * @OneToOne(targetEntity="Entities\GrilleAnalyse", inversedBy="doc", cascade={"persist"})
     */
    private $grilleAnalyse;

    function getId() {
        return $this->id;
    }

    function getNom() {
        return $this->nom;
    }

    function getUri() {
        return $this->uri;
    }

    function getUpload_date() {
        return $this->upload_date;
    }

    function getStatut() {
        return $this->statut;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNom($nom) {
        $this->nom = $nom;
    }

    function setUri($dir) {
        $this->uri = $dir;
    }

    function setUpload_date($upload_date) {
        $this->upload_date = $upload_date;
    }

    function setStatut($statut) {
        $this->statut = $statut;
    }

    function getType() {
        return $this->num_type;
    }

    function setType($type) {
        $this->num_type = $type;
    }

    function getExt() {
        return $this->ext;
    }

    function setExt($ext) {
        $this->ext = $ext;
    }

    function getSinistre() {
        return $this->sinistre;
    }

    function setSinistre($sinistre) {
        $this->sinistre = $sinistre;
    }

    function getSize() {
        return $this->size;
    }

    function setSize($size) {
        $this->size = $size;
    }

    function getDate() {
        return $this->date;
    }

    function setDate($date) {
        $this->date = $date;
    }

    public function toArray() {
        $date_f = null;
        $date_upload_f = null;
        $date_reception_f = null;

        if ($this->date != null)
            $date_f = $this->date;
        if ($this->upload_date != null)
            $date_upload_f = $this->upload_date;
        if ($this->date_reception != null)
            $date_reception_f = $this->date_reception;
        return array(
            "id" => $this->id,
            "nom" => $this->nom,
            "uri" => $this->uri,
            "upload_date" => $date_upload_f,
            "date" => $date_f,
            "ext" => $this->ext,
            "size" => $this->size,
            "type" => $this->getType()->getNum(),
            "type_str" => $this->getType()->getType(),
            "date_reception" => $date_reception_f
        );
    }

    function getDate_reception() {
        return $this->date_reception;
    }

    function setDate_reception($date_reception) {
        if ($date_reception != null)
            $this->date_reception = $date_reception;
    }

    function getGrilleAnalyse() {
        return $this->grilleAnalyse;
    }

    function setGrilleAnalyse($grilleAnalyse) {
        $this->grilleAnalyse = $grilleAnalyse;
    }

    function getAffaire() {
        return $this->affaire;
    }

    function setAffaire($affaire) {
        $this->affaire = $affaire;
    }

    function getNom_public() {
        return $this->nom_public;
    }

    function setNom_public($nom_public) {
        $this->nom_public = $nom_public;
    }

    function get_size_ko() {
        return round($this->size / 1000, 1);
    }

}
