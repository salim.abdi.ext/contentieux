<?php

/*
 * catégories :
 *
 * ================================= DO ===== AV1 ====== JUD ====== Autre
 *
 * 1 - expert                        x        x
 * 2 - defense qc                    x        x                     x
 * 3 - asqua                         x        x          x          x
 * 4 - qc (public) "dires qc"        x        x                     x
 * 5 - qc (interne)                  x        x          x          x
 * 6 - expert cie                             x          x          x
 * 7 - pieces adverses "dires adverse"        x          x
 * 8 - Expert judiciaire                                 x
 */

namespace Entities;

/**
 * @Entity
 * @Table(name="type_document")
 */
class Type_docs {

    /**
     * @Column(type="integer", length=32, unique=true, nullable=false)
     * @Id
     * @GeneratedValue
     */
    private $id;

    /**
     * @Column(type="string", length=50, unique=false, nullable=false)
     */
    private $type;

    /**
     * @Column(type="integer", length=50, unique=true, nullable=false)
     */
    private $num;


    function getId() {
        return $this->id;
    }

    function getType() {
        return $this->type;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setType($type) {
        $this->type = $type;
    }

    function getNum() {
        return $this->num;
    }

    function setNum($num) {
        $this->num = $num;
    }
}
