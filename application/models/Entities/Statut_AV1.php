<?php

namespace Entities;

use Entities\Statut;

/**
 * @Entity(repositoryClass="Statut_AV1Repository")
 */
Class Statut_AV1 extends Statut {

    /**
     * @Column(type="string", length=50, unique=false, nullable=false)
     */
    private $ref_dossier_expert;

    /**
     * @Column(type="date", length=32, unique=false, nullable=false)
     */
    private $date_declaration_sinistre;

    /**
     * @Column(type="date", length=32, unique=false, nullable=false)
     */
    private $date_premiere_reunion_expertise;

    /**
     * @Column(type="string", length=50, unique=false, nullable=true)
     */
    private $ref_dossier_expert_compagnie;

    /**
     * @ManyToOne(targetEntity="Entities\Contact", cascade={"all"})
     */
    private $cabinet_expert;

    /**
     * @ManyToOne(targetEntity="Entities\Contact", cascade={"all"})
     */
    private $contact_expert;

    /**
     * @ManyToOne(targetEntity="Entities\Contact", cascade={"all"})
     */
    private $cabinet_expert_compagnie;

    /**
     * @ManyToOne(targetEntity="Entities\Contact", cascade={"all"})
     */
    private $contact_expert_compagnie;

    /**
     * @OneToOne(targetEntity="Entities\Sinistre", mappedBy="av1")
     */
    private $sinistre;


    function getRef_dossier_expert() {
        return $this->ref_dossier_expert;
    }

    function getDate_declaration_sinistre() {
        return $this->date_declaration_sinistre;
    }

    function getDate_premiere_reunion_expertise() {
        return $this->date_premiere_reunion_expertise;
    }

    function getRef_dossier_expert_compagnie() {
        return $this->ref_dossier_expert_compagnie;
    }

    function getCabinet_expert() {
        return $this->cabinet_expert;
    }

    function getContact_expert() {
        return $this->contact_expert;
    }

    function getCabinet_expert_compagnie() {
        return $this->cabinet_expert_compagnie;
    }

    function getContact_expert_compagnie() {
        return $this->contact_expert_compagnie;
    }

    function getSinistre() {
        return $this->sinistre;
    }

    function setRef_dossier_expert($ref_dossier_expert) {
        $this->ref_dossier_expert = $ref_dossier_expert;
    }

    function setDate_declaration_sinistre($date_declaration_sinistre) {
        $this->date_declaration_sinistre = $date_declaration_sinistre;
    }

    function setDate_premiere_reunion_expertise($date_premiere_reunion_expertise) {
        $this->date_premiere_reunion_expertise = $date_premiere_reunion_expertise;
    }

    function setRef_dossier_expert_compagnie($ref_dossier_expert_compagnie) {
        $this->ref_dossier_expert_compagnie = $ref_dossier_expert_compagnie;
    }

    function setCabinet_expert($cabinet_expert) {
        $this->cabinet_expert = $cabinet_expert;
    }

    function setContact_expert($contact_expert) {
        $this->contact_expert = $contact_expert;
    }

    function setCabinet_expert_compagnie($cabinet_expert_compagnie) {
        $this->cabinet_expert_compagnie = $cabinet_expert_compagnie;
    }

    function setContact_expert_compagnie($contact_expert_compagnie) {
        $this->contact_expert_compagnie = $contact_expert_compagnie;
    }

    function setSinistre($sinistre) {
        $this->sinistre = $sinistre;
    }

}
