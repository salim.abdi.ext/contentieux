<?php

/*
 * catégories :
 *
 * ================================= DO ===== AV1 ====== JUD ====== Autre
 *
 * 1 - expert do                     x
 * 2 - expert do av1                          x
 * 3 - defense qc                    x        x                     x
 * 4 - asqua                         x        x          x          x
 * 5 - qc (public)                   x        x                     x
 * 6 - qc (interne)                  x        x          x          x
 * 7 - expert cie                             x          x          x
 * 8 - pieces adverses                        x                     x
 * 9 - Expert judiciaire                                 x
 * 10 - Procédure                                        x
 * 11 - Dires QC                                         x
 * 12 - Dires asverse                                    x
 * 13 - Fait générateur                                             x
 * 14 - Expert Pilote                                               x
 * 15 - Avocat QC                                        *
 */

namespace Entities;

/**
 * @Entity
 * @Table(name="categorie_doc")
 */
class Categories_docs {

    /**
     * @Column(type="integer", length=32, unique=true, nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Column(type="string", length=150, unique=false, nullable=false)
     */
    private $libelle;

    /**
     * @Column(type="integer", unique=true, nullable=false)
     */
    private $num;

    /**
     * @Column(type="boolean")
     */

    function getId() {
        return $this->id;
    }

    function getLibelle() {
        return $this->libelle;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setLibelle($libelle) {
        $this->libelle = $libelle;
    }

    function getNum() {
        return $this->num;
    }

    function setNum($num) {
        $this->num = $num;
    }

}
