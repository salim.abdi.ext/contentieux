<?php

namespace Entities;

use Doctrine\ORM\EntityRepository;

class EditionRepository extends EntityRepository {

    public function getEditions($statut) {
        $qb = $this->createQueryBuilder('e');
        $qb->where($qb->expr()->eq('e.statut', '?1'))
                ->andWhere($qb->expr()->not($qb->expr()->eq('e.etat', '?2')))
                ->setParameter(1, $statut)
                ->setParameter(2, -1);

        return $qb->getQuery()->getResult();
    }

    public function canEdit($statut) {
        $qb = $this->createQueryBuilder('e');
        $qb->where($qb->expr()->eq('e.statut', '?1'))
                ->andWhere($qb->expr()->in('e.etat', array(0, 1, 2)))
                ->andWhere($qb->expr()->in('e.type_aspose', array(4, 8, 14, 18)))
                ->setParameter(1, $statut);

        $result = $qb->getQuery()->getResult();

        if (count($result) >= 1)
            return false;
        return true;
    }

    /**
     * Types : "cr_reunion","courrier","be"
     */
    public function getSaved($statut, $type = "cr_reunion") {
        $qb = $this->createQueryBuilder('e');
        $qb->where($qb->expr()->eq('e.statut', '?1'))
                ->andWhere($qb->expr()->eq('e.etat', -1));
        switch ($type) {
            case "cr_reunion":
                $qb->andWhere($qb->expr()->in('e.type_aspose', array(4, 8, 14, 18)));
                break;
            case "courrier":
                $qb->andWhere($qb->expr()->in('e.type_aspose', array(108)));
                break;
            case "be":
                $qb->andWhere($qb->expr()->in('e.type_aspose', array(107)));
                break;
        }
        $qb->setParameter(1, $statut);

        $result = $qb->getQuery()->getResult();
        if (count($result) >= 1)
            return $result[0];
    }

}
