<?php

namespace Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Entities\Moe;
use Entities\Moa;

/**
 * @Entity(repositoryClass="Entities\AffaireRepository")
 * @Table(name="affaire")
 */
class Affaire {

    /**
     * @Column(type="integer", length=32, unique=true, nullable=false)
     * @Id
     * @GeneratedValue
     */
    private $id;

    /**
     * @OneToMany(targetEntity="Entities\Sinistre", mappedBy="affaire", cascade={"persist"})
     */
    private $sinistres;

    /**
     * @OneToMany(targetEntity="Entities\File", mappedBy="affaire", cascade={"persist"})
     */
    private $documents;

    /**
     * @ManyToOne(targetEntity="Entities\Adresse", cascade={"persist"})
     */
    private $adresse;

    /**
     * @Column(type="string", length=32, unique=false, nullable=false)
     */
    private $code;

    /**
     * @Column(type="string", length=255, unique=false, nullable=true)
     */
    private $libelle;

    /**
     * @Column(type="integer", length=32, unique=false, nullable=true)
     */
    private $id_service;

    /**
     * @Column(type="integer", length=32, unique=false, nullable=true)
     */
    private $id_service_contentieux;

    /**
     * @Column(type="integer", length=32, unique=false, nullable=true)
     */
    private $id_ca;

    /**
     * @Column(type="string", length=50, unique=false, nullable=true)
     */
    private $ca;

    /**
     * @Column(type="string", length=200, unique=false, nullable=true)
     */
    private $missions;

    /**
     * @Column(type="string", length=32, unique=false, nullable=true)
     */
    private $id_qp_contrat;

    /**
     * @Column(type="string", length=32, unique=false, nullable=true)
     */
    private $id_g_affaire;

    /**
     * @Column(type="string", length=32, unique=false, nullable=true)
     */
    private $nom_service;

    /**
     * @Column(type="string", length=32, unique=false, nullable=true)
     */
    private $nom_service_contentieux;

    /**
     * @Column(type="string", length=32, unique=false, nullable=true)
     */
    private $id_region;

    /**
     * @Column(type="string", length=50, unique=false, nullable=true)
     */
    private $nom_region;

    /**
     * @Column(type="datetime")
     */
    private $date_import;

    /**
     * @Column(type="string", unique=false, nullable=true)
     */
    private $id_agence;

    /**
     * @Column(type="string", length=150, unique=false, nullable=true)
     */
    private $nom_mo;

    /**
     * @Column(type="text", unique=false, nullable=true)
     */
    private $mo;

    /**
     * @Column(type="text", unique=false, nullable=true)
     */
    private $moe;

    /**
     * @Column(type="string", unique=false, nullable=true)
     */
    private $sage;

    /**
     * @Column(type="integer", unique=false, nullable=true)
     */
    private $rcr;

    /**
     * @Column(type="integer", unique=false, nullable=true)
     */
    private $id_operation;

    /**
     * @Column(type="date", unique=false, nullable=true)
     */
    private $droc;

    /**
     * @Column(type="integer", unique=false, nullable=true)
     */
    private $statut_affaire;

    public function __construct() {
        $this->sinistres = new ArrayCollection();
        $this->documents = new ArrayCollection();
        $this->date_import = new \DateTime();
    }

    function getId() {
        return $this->id;
    }

    function getCode() {
        return $this->code;
    }

    function getLibelle() {
        return $this->libelle;
    }

    function getId_service() {
        return $this->id_service;
    }

    function getId_ca() {
        return $this->id_ca;
    }

    function getCa() {
        return $this->ca;
    }

    function getMissions() {
        return $this->missions;
    }

    function getId_qp_contrat() {
        return $this->id_qp_contrat;
    }

    function getId_g_affaire() {
        return $this->id_g_affaire;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setCode($code) {
        $this->code = $code;
    }

    function setLibelle($libelle) {
        $this->libelle = $libelle;
    }

    function setId_service($id_service) {
        $this->id_service = $id_service;
    }

    function setId_ca($id_ca) {
        $this->id_ca = $id_ca;
    }

    function setCa($ca) {
        $this->ca = $ca;
    }

    function setMissions($missions) {
        $this->missions = $missions;
    }

    function setId_qp_contrat($id_qp_contrat) {
        $this->id_qp_contrat = $id_qp_contrat;
    }

    function setId_g_affaire($id_g_affaire) {
        $this->id_g_affaire = $id_g_affaire;
    }

    function getSinistres() {
        $r = array();
        foreach ($this->sinistres as $sinistre) {
            if ($sinistre->getFlag_sinistre_desactive() != 1 && $sinistre->getRp() == null)
                $r[] = $sinistre;
        }
        return $r;
    }

    function getRps() {
        $r = array();
        foreach ($this->sinistres as $sinistre) {
            if ($sinistre->getFlag_sinistre_desactive() != true && $sinistre->getRp() != null)
                $r[] = $sinistre;
        }
        return $r;
    }

    function addSinistre($sinistre) {
        $this->sinistres[] = $sinistre;
    }

    function getAdresse() {
        return $this->adresse;
    }

    function setAdresse($adresse) {
        $this->adresse = $adresse;
    }

    function getDocuments() {
        return $this->documents;
    }

    function addDocument($document) {
        $this->documents->add($document);
        $document->setAffaire($this);
    }

    function getNom_service() {
        return $this->nom_service;
    }

    function getId_region() {
        return $this->id_region;
    }

    function getNom_region() {
        return $this->nom_region;
    }

    function setNom_service($nom_service) {
        $this->nom_service = $nom_service;
    }

    function setId_region($id_region) {
        $this->id_region = $id_region;
    }

    function setNom_region($nom_region) {
        $this->nom_region = $nom_region;
    }

    function getId_agence() {
        return $this->id_agence;
    }

    function setId_agence($id_agence) {
        $this->id_agence = $id_agence;
    }

    function getDate_import() {
        return $this->date_import;
    }

    function getNom_mo() {
        return $this->nom_mo;
    }

    function setDate_import($date_import) {
        $this->date_import = $date_import;
    }

    function setNom_mo($nom_mo) {
        $this->nom_mo = substr($nom_mo, 0, 300);
    }

    function getMo() {
        return $this->mo;
    }

    function setMo($mo) {
        $this->mo = $mo;
    }

    function getMoe() {
        return unserialize($this->moe);
    }

    function setMoe($moe) {
        $this->moe = serialize($moe);
    }

    function getSage() {
        return $this->sage;
    }

    function setSage($sage) {
        $this->sage = $sage;
    }

    function getRcr() {
        return $this->rcr;
    }

    function setRcr($rcr) {
        $this->rcr = $rcr;
    }

    function getId_operation() {
        return $this->id_operation;
    }

    function setId_operation($id_operation) {
        $this->id_operation = $id_operation;
    }

    function getDroc() {
        return $this->droc;
    }

    function setDroc($droc) {
        $this->droc = $droc;
    }

    function getId_service_contentieux() {
        return $this->id_service_contentieux;
    }

    function setId_service_contentieux($id) {
        $this->id_service_contentieux = $id;
    }

    function getNom_service_contentieux() {
        return $this->nom_service_contentieux;
    }

    function setNom_service_contentieux($id) {
        $this->nom_service_contentieux = $id;
    }

    function getStatut_affaire() {
        return $this->statut_affaire;
    }

    function setStatut_affaire($statut_affaire) {
        $this->statut_affaire = $statut_affaire;
    }

    public function checkAcces() {
        $_ci = &get_instance();
        $user = $_ci->context->getUser();


        // -----
        $users = array($user);
        @$suppleance = $user->remplace;
        if ($suppleance)
            $users = array_merge($users, $user->remplace);

        foreach ($users as $user) {

            if ($this->id_agence == null && $this->id_service_contentieux) {
                $_ci->load->model('Utils');
                $agence = $_ci->Utils->get_infos_entite($this->id_service_contentieux);
                $this->id_agence = $agence['id_agence'];
            }

            if (in_array($this->id_agence, $user->agence))
                return true;

            if ($user->role == 10 || $user->role == 8 || $user->role == 7 || $this->rcr == $user->id_utilisateur || $this->id_ca == $user->id_utilisateur) {
                return true;
            }
            if ($user->role == 5 || $user->role == 6) {
                if (in_array($this->getId_region(), $user->region))
                    return true;
                if (in_array($this->id_agence, $user->agence))
                    return true;
                if ($this->getId_service_contentieux() == $user->id_service)
                    return true;
                if ($user->role == 5) {
                    $rcr = $_ci->doctrine->em->getRepository('Entities\Rcr')->findBy(array('id_rcr' => $user->id_utilisateur));
                    foreach ($rcr as $r) {
                        if ($r->getId_agence() == $this->id_agence)
                            return true;
                    }
                }
            }
            if ($user->role == 2 || $user->role == 3 || $user->role == 4) {
                if (in_array($this->id_agence, $user->agence))
                    return true;
                if ($this->getId_service_contentieux() == $user->id_service)
                    return true;
            }
        }

        // Acces speciel Virginie BOUCHER
        if ($user->id_utilisateur == 496 || $user->id_utilisateur == 929)
            if ($this->getId_agence() == 49 || $this->getId_agence() == 165 || $this->getId_agence() == 83)
                return true;
        // JS Musseta
        if ($user->id_utilisateur == 993 && $this->getId_agence() == 83) {
            return true;
        }
        // Elis vincent
        if ($user->id_utilisateur == 2856 && $this->getId_agence() == 53) {
            return true;
        }

        return false;
    }

    function getNbSinistres($flag_desactive = false) {
        if ($flag_desactive === false) {
            return count($this->getSinistres());
        }
        else {
            $nb = 0;
            foreach ($this->getSinistres() as $sinistre) {
                if ($sinistre->getFlag_sinistre_desactive() == $flag_desactive) {
                    $nb++;
                }
            }
            
            return $nb;
        }
    }
    
}
