<?php

namespace Entities;

use Entities\Adresse;
use Entities\Statut;

/**
 * @Entity(repositoryClass="Statut_amiableRepository")
 */
Class Statut_amiable extends Statut {

    /**
     * @Column(type="string", length=50, unique=false, nullable=false)
     */
    private $ref_dossier_expert_compagnie;

    /**
     * @OneToOne(targetEntity="Entities\Contact", cascade={"all"})
     */
    private $cabinet_expert_compagnie;

    /**
     * @OneToOne(targetEntity="Entities\Contact", cascade={"all"})
     */
    private $expert_compagnie;

    /**
     * @OneToOne(targetEntity="Entities\Sinistre", mappedBy="amiable")
     */
    private $sinistre;

    /**
     * @OneToMany(targetEntity="Entities\Contact", mappedBy="amiable", cascade={"all"})
     */
    private $lo;

    public function __construct() {
        parent::__construct();
        $this->lo = new \Doctrine\Common\Collections\ArrayCollection();
    }

    function getRef_dossier_expert_compagnie() {
        return $this->ref_dossier_expert_compagnie;
    }

    function getCabinet_expert_compagnie() {
        return $this->cabinet_expert_compagnie;
    }

    function getExpert_compagnie() {
        return $this->expert_compagnie;
    }

    function getSinistre() {
        return $this->sinistre;
    }

    function setRef_dossier_expert_compagnie($ref_dossier_expert_compagnie) {
        $this->ref_dossier_expert_compagnie = $ref_dossier_expert_compagnie;
    }

    function setCabinet_expert_compagnie($cabinet_expert_compagnie) {
        $this->cabinet_expert_compagnie = $cabinet_expert_compagnie;
    }

    function setExpert_compagnie($expert_compagnie) {
        $this->expert_compagnie = $expert_compagnie;
    }

    function setSinistre($sinistre) {
        $this->sinistre = $sinistre;
    }

    function getLo() {
        return $this->lo;
    }

    function addLo($lo) {
        $this->lo[] = $lo;
    }

}
