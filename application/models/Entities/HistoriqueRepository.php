<?php

namespace Entities;

use Doctrine\ORM\EntityRepository;

/**
 * HistoriqueRepository
 */
class HistoriqueRepository extends EntityRepository {

    public function getNextChrono($type, $sinistre) {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        if ($sinistre instanceof Sinistre) {
            $req = $qb->select('MAX(h.num_chrono) as current_chrono')
                    ->from('Entities\Historique', 'h')
                    ->where($qb->expr()->eq('h.sinistre', '?1'))
                    ->andWhere($qb->expr()->eq('h.type', '?2'))
                    ->setParameter(1, $sinistre)
                    ->setParameter(2, $type);
        }

        $result = $qb->getQuery()->getResult();
        return $result[0]['current_chrono'] + 1;
    }

    public function getLibellesMail($sinistre) {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $req = $qb->select('distinct h.libelle')
                ->from('Entities\Historique', 'h')
                ->andWhere($qb->expr()->eq('h.type', '?1'))
                ->andWhere($qb->expr()->eq('h.sinistre', '?2'))
                ->setParameter(1, 1)
                ->setParameter(2, $sinistre);

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function getLibellesCourrier($sinistre) {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $req = $qb->select('distinct h.libelle')
                ->from('Entities\Historique', 'h')
                ->andWhere($qb->expr()->eq('h.type', '?1'))
                ->andWhere($qb->expr()->eq('h.sinistre', '?2'))
                ->setParameter(1, 2)
                ->setParameter(2, $sinistre);

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function listeFiltree($sinistre, $date = null, $type = null, $libelle = null, $recherche = null, $statut = null) {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        if ($statut) {
            $id = $statut->getId();
            $statut = $em->getRepository('Entities\Statut')->find($id);
        }
        $req = $qb->select('h')
                ->from('Entities\Historique', 'h')
                ->where($qb->expr()->eq('h.sinistre', '?5'))
                ->setParameter(5, $sinistre);
        if ($date)
            $qb->andWhere($qb->expr()->like('h.date', '?1'))
                    ->setParameter(1, $date . "%");
        if ($type)
            $qb->andWhere($qb->expr()->eq('h.type', '?2'))
                    ->setParameter(2, $type);
        if ($libelle)
            $qb->andWhere($qb->expr()->eq('h.libelle', '?3'))
                    ->setParameter(3, $libelle);
        if ($recherche) {
            $qb->andWhere($qb->expr()->like('h.data', '?4'))
                    ->setParameter(4, "%$recherche%");
        }
        if ($statut) {
            $qb->andWhere($qb->expr()->eq('h.statut', '?5'))
                    ->setParameter(5, $statut);
        }

        $qb->orderBy('h.date','DESC');
        $result = $qb->getQuery()->getResult();
        return $result;
    }

}
