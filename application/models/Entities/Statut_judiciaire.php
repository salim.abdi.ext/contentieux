<?php

namespace Entities;

use Entities\Adresse;
use Entities\Statut;

/**
 * @Entity(repositoryClass="Statut_JudiciaireRepository")
 * @Table
 */
class Statut_judiciaire extends Statut {

    /**
     * @Column(type="date", length=32, unique=false, nullable=false)
     */
    private $date_assignation;

    /**
     * @Column(type="string", length=50, unique=false, nullable=true)
     */
    private $ref_dossier_expert_compagnie;

    /**
     * @Column(type="string", length=50, unique=false, nullable=true)
     */
    private $ref_dossier_avocat;

    /**
     * @OneToOne(targetEntity="Entities\Sinistre", mappedBy="judiciaire")
     */
    private $sinistre;

    /**
     * @OneToOne(targetEntity="Entities\Contact", cascade={"all"})
     */
    private $expert_judiciaire;

    /**
     * @OneToOne(targetEntity="Entities\Contact", cascade={"all"})
     */
    private $cabinet_expert_compagnie_judiciaire;

    /**
     * @OneToOne(targetEntity="Entities\Contact", cascade={"all"})
     */
    private $expert_compagnie_judiciaire;

    /**
     * @OneToOne(targetEntity="Entities\Contact", cascade={"all"})
     */
    private $cabinet_avocat;

    /**
     * @OneToOne(targetEntity="Entities\Contact", cascade={"all"})
     */
    private $avocat_dossier;

    /**
     * @OneToOne(targetEntity="Entities\Contact", cascade={"all"})
     */
    private $collaborateur_avocat_dossier;
    
    function getRef_dossier_expert() {
        return $this->ref_dossier_expert;
    }

    function getRef_dossier_expert_compagnie() {
        return $this->ref_dossier_expert_compagnie;
    }

    function getRef_dossier_avocat() {
        return $this->ref_dossier_avocat;
    }

    function getSinistre() {
        return $this->sinistre;
    }

    function getExpert_judiciaire() {
        return $this->expert_judiciaire;
    }

    function getCabinet_expert_compagnie_judiciaire() {
        return $this->cabinet_expert_compagnie_judiciaire;
    }

    function getExpert_compagnie_judiciaire() {
        return $this->expert_compagnie_judiciaire;
    }

    function getCabinet_avocat() {
        return $this->cabinet_avocat;
    }

    function getAvocat_dossier() {
        return $this->avocat_dossier;
    }

    function getCollaborateur_avocat_dossier() {
        return $this->collaborateur_avocat_dossier;
    }

    function setRef_dossier_expert($ref_dossier_expert) {
        $this->ref_dossier_expert = $ref_dossier_expert;
    }

    function setRef_dossier_expert_compagnie($ref_dossier_expert_compagnie) {
        $this->ref_dossier_expert_compagnie = $ref_dossier_expert_compagnie;
    }

    function setRef_dossier_avocat($ref_dossier_avocat) {
        $this->ref_dossier_avocat = $ref_dossier_avocat;
    }

    function setSinistre($sinistre) {
        $this->sinistre = $sinistre;
    }

    function setExpert_judiciaire($expert_judiciaire) {
        $this->expert_judiciaire = $expert_judiciaire;
    }

    function setCabinet_expert_compagnie_judiciaire($cabinet_expert_compagnie_judiciaire) {
        $this->cabinet_expert_compagnie_judiciaire = $cabinet_expert_compagnie_judiciaire;
    }

    function setExpert_compagnie_judiciaire($expert_compagnie_judiciaire) {
        $this->expert_compagnie_judiciaire = $expert_compagnie_judiciaire;
    }

    function setCabinet_avocat($cabinet_avocat) {
        $this->cabinet_avocat = $cabinet_avocat;
    }

    function setAvocat_dossier($avocat_dossier) {
        $this->avocat_dossier = $avocat_dossier;
    }

    function setCollaborateur_avocat_dossier($collaborateur_avocat_dossier) {
        $this->collaborateur_avocat_dossier = $collaborateur_avocat_dossier;
    }

    function getDate_assignation() {
        return $this->date_assignation;
    }

    function setDate_assignation($date_assignation) {
        $this->date_assignation = $date_assignation;
    }

}
