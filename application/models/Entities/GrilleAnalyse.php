<?php

namespace Entities;

/**
 * @Entity
 * @Table(name="grille_analyse")
 */
class GrilleAnalyse {

    /**
     * @Column(type="integer", length=32, unique=true, nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Column(type="boolean", nullable=true)
     */
    private $implication_qc;

    /**
     * @Column(type="boolean", nullable=true)
     */
    private $caractere_isole;

    /**
     * @Column(type="text", unique=false, nullable=true)
     */
    private $analyses;

    /**
     * @Column(type="text", unique=false, nullable=true)
     */
    private $conclusion_qc;

    /**
     * @Column(type="text", unique=false, nullable=true)
     */
    private $conclusion_expert;

    /**
     * @Column(type="datetime", nullable=true)
     */
    private $diffusee;

    /**
     * @Column(type="datetime", nullable=true)
     */
    private $differee;

    /**
     * @Column(type="boolean", nullable=true)
     */
    private $avis_rcr;

    /**
     * @Column(type="boolean", nullable=true)
     */
    private $conclusion_contestee;

    /**
     * @Column(type="string", length=150, unique=false, nullable=true)
     */
    private $auteur;

    /**
     * @Column(type="string", length=150, unique=false, nullable=true)
     */
    private $mail_auteur;

    /**
     * @Column(type="text", unique=false, nullable=true)
     */
    private $commentaire_interne;

    /**
     * @OneToOne(targetEntity="Entities\File", mappedBy="grilleAnalyse")
     */
    private $doc;

    function getId() {
        return $this->id;
    }

    function getImplication_qc() {
        return $this->implication_qc;
    }

    function getCaractere_isole() {
        return $this->caractere_isole;
    }

    function getAnalyses() {
        if ($this->analyses == null)
            return null;
        else
            return unserialize($this->analyses);
    }

    function getDoc() {
        return $this->doc;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setImplication_qc($implication_qc) {
        $this->implication_qc = $implication_qc;
    }

    function setCaractere_isole($caractere_isole) {
        $this->caractere_isole = $caractere_isole;
    }

    function setAnalyses($analyses) {
        if ($analyses == null)
            $this->analyses = null;
        else
            $this->analyses = serialize($analyses);
    }

    function setDoc($doc) {
        $this->doc = $doc;
    }

    function getDiffusee() {
        return $this->diffusee;
    }

    function setDiffusee($diffusee) {
        $this->diffusee = $diffusee;
    }

    function getConclusion_qc() {
        return $this->conclusion_qc;
    }

    function getConclusion_expert() {
        return $this->conclusion_expert;
    }

    function setConclusion_qc($conclusion_qc) {
        $this->conclusion_qc = $conclusion_qc;
    }

    function setConclusion_expert($conclusion_expert) {
        $this->conclusion_expert = $conclusion_expert;
    }

    function getAvis_rcr() {
        return $this->avis_rcr;
    }

    function setAvis_rcr($avis_rcr) {
        $this->avis_rcr = $avis_rcr;
    }

    function getConclusion_contestee() {
        return $this->conclusion_contestee;
    }

    function setConclusion_contestee($conclusion_contestee) {
        $this->conclusion_contestee = $conclusion_contestee;
    }

    function getDifferee() {
        return $this->differee;
    }

    function setDifferee($differee) {
        $this->differee = $differee;
    }

    function getAuteur() {
        return $this->auteur;
    }

    function setAuteur($auteur) {
        $this->auteur = $auteur;
    }

    function getMail_auteur() {
        return $this->mail_auteur;
    }

    function setMail_auteur($mail_auteur) {
        $this->mail_auteur = $mail_auteur;
    }

    function getCommentaire_interne() {
        return $this->commentaire_interne;
    }

    function setCommentaire_interne($commentaire_interne) {
        $this->commentaire_interne = $commentaire_interne;
    }

}
