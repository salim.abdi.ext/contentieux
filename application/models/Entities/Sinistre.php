<?php

/*
 * Statuts :
 *  1 - DO
 *  2 - AV1
 *  3 - Jud
 *  4 - Amiable
 */

namespace Entities;

use Entities\Affaire;

/**
 * @Entity(repositoryClass="Entities\SinistreRepository")
 * @Table(name="sinistre")
 */
class Sinistre {

    public function __construct() {
        $this->memos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->documents = new \Doctrine\Common\Collections\ArrayCollection();
        $this->historique = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @Column(type="integer", length=32, unique=true, nullable=false)
     * @Id
     * @GeneratedValue
     */
    private $id;

    /**
     * @Column(type="integer", length=32, unique=false, nullable=false)
     */
    private $statut;

    /**
     * @Column(type="date", length=32, unique=false, nullable=true)
     */
    private $date_declaration;

    /**
     * @Column(type="string", length=500, unique=false, nullable=true)
     */
    private $description;

    /**
     * @Column(type="integer", length=32, unique=false, nullable=true)
     */
    private $statut_initial;

    /**
     * @ManyToOne(targetEntity="Entities\Affaire", inversedBy="sinistres", cascade={"persist"})
     */
    protected $affaire;

    /**
     * @ManyToOne(targetEntity="Entities\Adresse", cascade={"persist"})
     */
    private $adresse;

    /**
     * @OneToOne(targetEntity="Entities\Statut_DO", inversedBy="sinistre", cascade={"persist"})
     */
    private $do;

    /**
     * @OneToOne(targetEntity="Entities\Statut_AV1", inversedBy="sinistre", cascade={"persist"})
     */
    private $av1;

    /**
     * @OneToOne(targetEntity="Entities\Statut_judiciaire", inversedBy="sinistre", cascade={"persist"})
     */
    private $judiciaire;

    /**
     * @OneToOne(targetEntity="Entities\Statut_amiable", inversedBy="sinistre", cascade={"persist"})
     */
    private $amiable;

    /**
     * @OneToOne(targetEntity="Entities\Statut_rp", inversedBy="sinistre", cascade={"persist"})
     */
    private $rp;

    /**
     * @OneToOne(targetEntity="Entities\File", cascade={"persist"})
     */
    private $contrat;

    /**
     * @Column(type="string", length=50, nullable=true)
     */
    private $ref_asqua;

    /**
     * @Column(type="string", length=50, nullable=true)
     */
    private $assureur;

    /**
     * @Column(type="string", length=50, nullable=true)
     */
    private $token_asqua;

    /**
     * @Column(type="datetime", unique=false, nullable=true)
     */
    private $date_derniere_synchro = null;

    /**
     * @Column(type="datetime", unique=false, nullable=true)
     */
    private $date_derniere_vue_asqua = null;

    /**
     * @Column(type="integer", length=32, unique=false, nullable=false)
     */
    private $num_chrono;

    /**
     * @Column(type="date", nullable=true)
     */
    private $date_reception_ouvrage;

    /**
     * @Column(type="date", nullable=true)
     */
    private $droc;

    /**
     * @Column(type="string", length=200, unique=false, nullable=true)
     */
    private $nom;

    /**
     * @Column(type="boolean", nullable=true)
     */
    private $rc;

    /**
     * @Column(type="boolean", nullable=true)
     */
    private $rcd;

    /**
     * @Column(type="boolean", nullable=true)
     */
    private $reception_prononcee;

    /**
     * @Column(type="datetime",unique=false, nullable=true)
     */
    private $date_fermeture;

    /**
     * @OneToMany(targetEntity="Entities\Memo", mappedBy="sinistre", cascade={"persist"}, orphanRemoval=true)
     * @OrderBy({"date" = "DESC"})
     */
    private $memos;

    /**
     * @OneToMany(targetEntity="Entities\Historique", mappedBy="sinistre")
     */
    private $historique;

    /**
     * @OneToMany(targetEntity="Entities\File", mappedBy="sinistre")
     */
    private $documents;

    /**
     * @Column(type="integer", nullable=true)
     */
    private $tranche;

    /**
     * @Column(type="integer", nullable=true)
     * 
     * 1 - Supprimé
     * 2 - Clos
     */
    private $flag_sinistre_desactive;

    /**
     * @Column(type="date", nullable=true)
     */
    private $sinistre_clos_date;

    /**
     * @Column(type="integer", nullable=true)
     */
    private $sinistre_clos_cout;

    /**
     * @Column(type="integer", nullable=true)
     */
    private $sinistre_clos_cout_qc;

    /**
     * @Column(type="string", nullable=true)
     */
    private $sinistre_clos_mission;

    /**
     * @Column(type="string", nullable=true)
     */
    private $sinistre_clos_destination_ouvrage;

    /**
     * @Column(type="string", nullable=true)
     */
    private $sinistre_clos_partie_ouvrage;

    /**
     * @Column(type="string", nullable=true)
     */
    private $sinistre_clos_phase_concernee;

    /**
     * @Column(type="datetime", nullable=true)
     */
    private $date_cloture_suppression;

    /**
     * @Column(type="string", nullable=true, length=500)
     */
    private $description_qc;

    /**
     * @Column(type="array", nullable=true)
     */
    private $flag_documents;

    function getAffaire() {
        return $this->affaire;
    }

    function setAffaire(Affaire $affaire) {
        $this->affaire = $affaire;
        $affaire->addSinistre($this);
    }

    function getStatut() {
        return $this->statut;
    }

    function getDate_declaration() {
        return $this->date_declaration;
    }

    function getDescription() {
        return $this->description;
    }

    function getStatut_initial() {
        return $this->statut_initial;
    }

    function setStatut($statut) {
        $this->statut = $statut;
    }

    function setDate_declaration($date_declaration) {
        $this->date_declaration = $date_declaration;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setStatut_initial($statut_initial) {
        $this->statut_initial = $statut_initial;
    }

    function getId() {
        return $this->id;
    }

    function getAdresse() {
        return $this->adresse;
    }

    function setAdresse($adresse) {
        $this->adresse = $adresse;
    }

    function getDo() {
        return $this->do;
    }

    function setDo($do) {
        $this->do = $do;
        $do->setSinistre($this);
    }

    function getAv1() {
        return $this->av1;
    }

    function setAv1($av1) {
        $this->av1 = $av1;
    }

    function getJudiciaire() {
        return $this->judiciaire;
    }

    function setJudiciaire($judiciaire) {
        $this->judiciaire = $judiciaire;
    }

    function getAmiable() {
        return $this->amiable;
    }

    function setAmiable($amiable) {
        $this->amiable = $amiable;
    }

    function getContrat() {
        return $this->contrat;
    }

    function setContrat($contrat) {
        $this->contrat = $contrat;
    }

    function getMemos() {
        return $this->memos;
    }

    function addMemo($memo) {
        $this->memos->add($memo);
        $memo->setSinistre($this);
    }

    function delMemo($memo) {
        $this->memos->removeElement($memo);
    }

    function getToken_asqua() {
        return $this->token_asqua;
    }

    function setToken_asqua($token_asqua) {
        $this->token_asqua = $token_asqua;
    }

    function getRef_asqua() {
        return $this->ref_asqua;
    }

    function setRef_asqua($ref_asqua) {
        $this->ref_asqua = $ref_asqua;
    }

    function getNum_chrono() {
        return $this->num_chrono;
    }

    function setNum_chrono($num_chrono) {
        $this->num_chrono = $num_chrono;
    }

    function getHistorique() {
        return $this->historique;
    }

    function addHistorique($historique) {
        $this->historique[] = $historique;
    }

    function getDocuments() {
        return $this->documents;
    }

    function addDocument($documents) {
        $this->documents[] = $documents;
    }

    function getDate_reception_ouvrage() {
        return $this->date_reception_ouvrage;
    }

    function setDate_reception_ouvrage($date_reception_ouvrage) {
        $this->date_reception_ouvrage = $date_reception_ouvrage;
    }

    function getDate_derniere_synchro() {
        return $this->date_derniere_synchro;
    }

    function setDate_derniere_synchro($date_derniere_synchro) {
        $this->date_derniere_synchro = $date_derniere_synchro;
    }

    function getDroc() {
        return $this->droc;
    }

    function setDroc($droc) {
        $this->droc = $droc;
    }

    function getNom() {
        return $this->nom;
    }

    function setNom($nom) {
        $this->nom = $nom;
    }

    function getRc() {
        return $this->rc;
    }

    function getRcd() {
        return $this->rcd;
    }

    function getDate_fermeture() {
        return $this->date_fermeture;
    }

    function setRc($rc) {
        $this->rc = $rc;
    }

    function setRcd($rcd) {
        $this->rcd = $rcd;
    }

    function setDate_fermeture($date_fermeture) {
        $this->date_fermeture = $date_fermeture;
    }

    function getTranche() {
        return $this->tranche;
    }

    function setTranche($tranche) {
        $this->tranche = $tranche;
    }

    function getAssureur() {
        return $this->assureur;
    }

    function setAssureur($assureur) {
        $this->assureur = $assureur;
    }

    function getReception_prononcee() {
        return $this->reception_prononcee;
    }

    function setReception_prononcee($reception_prononcee) {
        $this->reception_prononcee = $reception_prononcee;
    }

    function getFlag_sinistre_desactive() {
        return $this->flag_sinistre_desactive;
    }

    function setFlag_sinistre_desactive($flag_sinistre_desactive) {
        $this->flag_sinistre_desactive = $flag_sinistre_desactive;
    }

    function getDescription_qc() {
        return $this->description_qc;
    }

    function setDescription_qc($description_qc) {
        $this->description_qc = substr($description_qc, 0, 500);
    }

    public function is_synchro() {
        if ($this->date_derniere_synchro != null)
            return true;
        return false;
    }

    function getRp() {
        return $this->rp;
    }

    function setRp($rp) {
        $this->rp = $rp;
    }

    function getFlag_documents() {
        return $this->flag_documents;
    }

    function setFlag_documents($flag_documents) {
        $this->flag_documents = $flag_documents;
    }

    function getDate_derniere_vue_asqua() {
        return $this->date_derniere_vue_asqua;
    }

    function setDate_derniere_vue_asqua($date_derniere_vue_asqua) {
        $this->date_derniere_vue_asqua = $date_derniere_vue_asqua;
    }

    function getSinistre_clos_cout() {
        return $this->sinistre_clos_cout;
    }

    function getSinistre_clos_cout_qc() {
        return $this->sinistre_clos_cout_qc;
    }

    function getSinistre_clos_mission() {
        return $this->sinistre_clos_mission;
    }

    function getSinistre_clos_destination_ouvrage() {
        return $this->sinistre_clos_destination_ouvrage;
    }

    function getDate_cloture_suppression() {
        return $this->date_cloture_suppression;
    }

    function setSinistre_clos_cout($sinistre_clos_cout) {
        $this->sinistre_clos_cout = $sinistre_clos_cout;
    }

    function setSinistre_clos_cout_qc($sinistre_clos_cout_qc) {
        $this->sinistre_clos_cout_qc = $sinistre_clos_cout_qc;
    }

    function setSinistre_clos_mission($sinistre_clos_mission) {
        $this->sinistre_clos_mission = $sinistre_clos_mission;
    }

    function setSinistre_clos_destination_ouvrage($sinistre_clos_destination_ouvrage) {
        $this->sinistre_clos_destination_ouvrage = $sinistre_clos_destination_ouvrage;
    }

    function setDate_cloture_suppression($date_cloture_suppression) {
        $this->date_cloture_suppression = $date_cloture_suppression;
    }

    function getSinistre_clos_date() {
        return $this->sinistre_clos_date;
    }

    function getSinistre_clos_partie_ouvrage() {
        return $this->sinistre_clos_partie_ouvrage;
    }

    function setSinistre_clos_date($sinistre_clos_date) {
        $this->sinistre_clos_date = $sinistre_clos_date;
    }

    function setSinistre_clos_partie_ouvrage($sinistre_clos_partie_ouvrage) {
        $this->sinistre_clos_partie_ouvrage = $sinistre_clos_partie_ouvrage;
    }

    function getSinistre_clos_phase_concernee() {
        return $this->sinistre_clos_phase_concernee;
    }

    function setSinistre_clos_phase_concernee($sinistre_clos_phase_concernee) {
        $this->sinistre_clos_phase_concernee = $sinistre_clos_phase_concernee;
    }

    function get_current_statut() {
        $statut_id = $this->getStatut();

        switch ($statut_id) {
            case 1:
                return $this->getDo();
                break;
            case 2:
                return $this->getAv1();
                break;
            case 3:
                return $this->getJudiciaire();
                break;
            case 4:
                return $this->getAmiable();
                break;
            case 5:
                return $this->getRp();
                break;
        }
    }

}
