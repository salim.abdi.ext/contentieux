<?php

namespace Entities;

/*
 * Types histo :
 *  1 - Mails
 *  2 - Editions
 *  3 - Sinistre
 * 4 - Asqua
 */

/**
 * @Entity(repositoryClass="Entities\HistoriqueRepository")
 * @Table(name="historique")
 */
class Historique {

    /**
     * @Column(type="integer", length=32, unique=true, nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Column(type="string", length=32, unique=false, nullable=true)
     */
    private $type;

    /**
     * @Column(type="datetime", length=32, unique=false, nullable=false)
     */
    private $date;

    /**
     * @Column(type="text", unique=false, nullable=true)
     */
    private $data;

    /**
     * @Column(type="integer", length=32, unique=false, nullable=false)
     */
    private $num_chrono;

    /**
     * @Column(type="string", length=100, unique=false, nullable=true)
     */
    private $libelle;

    /**
     * @Column(type="integer", nullable=true)
     */
    private $type_mail;

    /**
     * @ManyToOne(targetEntity="Entities\Sinistre", inversedBy="historique")
     */
    private $sinistre;

    /**
     * @ManyToOne(targetEntity="Entities\Statut", inversedBy="historique")
     */
    private $statut;

    public function __construct() {
        $this->date = new \DateTime;
    }

    function getId() {
        return $this->id;
    }

    function getType() {
        return $this->type;
    }

    function getDate() {
        return $this->date;
    }

    function getData() {
        return unserialize($this->data);
    }

    function setId($id) {
        $this->id = $id;
    }

    function setType($type) {
        $this->type = $type;
    }

    function setDate($date) {
        $this->date = $date;
    }

    function setData($data) {
        $this->data = serialize($data);
    }

    function getNum_chrono() {
        return $this->num_chrono;
    }

    function getSinistre() {
        return $this->sinistre;
    }

    function getStatut() {
        return $this->statut;
    }

    function setNum_chrono($num_chrono) {
        $this->num_chrono = $num_chrono;
    }

    function setSinistre($sinistre) {
        $this->sinistre = $sinistre;
        $sinistre->addHistorique($this);
    }

    function setStatut($statut) {
        $this->statut = $statut;
        $statut->addHistorique($this);
    }

    function getLibelle() {
        return $this->libelle;
    }

    function setLibelle($libelle) {
        $this->libelle = $libelle;
    }

    function getType_mail() {
        return $this->type_mail;
    }

    function setType_mail($type_mail) {
        $this->type_mail = $type_mail;
    }

}
