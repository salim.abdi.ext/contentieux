<?php

namespace Entities;

/**
 * @Entity()
 * @Table(name="suppleance")
 */
class Suppleance {

    /**
     * @Column(type="integer", length=32, unique=true, nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Column(type="integer", length=32, unique=true, nullable=false)
     */
    private $id_utilisateur;

    /**
     * @Column(type="integer", length=32, unique=true, nullable=false)
     */
    private $id_suppleant;

    /**
     * @Column(type="string", length=150, unique=false, nullable=true)
     */
    private $nom_suppleant;

    /**
     * @Column(type="date", nullable=true)
     */
    private $date_debut;

    /**
     * @Column(type="date", nullable=true)
     */
    private $date_fin;

    function getId() {
        return $this->id;
    }

    function getId_utilisateur() {
        return $this->id_utilisateur;
    }

    function getId_suppleant() {
        return $this->id_suppleant;
    }

    function getDate_debut() {
        return $this->date_debut;
    }

    function getDate_fin() {
        return $this->date_fin;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setId_utilisateur($id_utilisateur) {
        $this->id_utilisateur = $id_utilisateur;
    }

    function setId_suppleant($id_suppleant) {
        $this->id_suppleant = $id_suppleant;
    }

    function setDate_debut($date_debut) {
        $this->date_debut = $date_debut;
    }

    function setDate_fin($date_fin) {
        $this->date_fin = $date_fin;
    }

    function getNom_suppleant() {
        return $this->nom_suppleant;
    }

    function setNom_suppleant($nom_suppleant) {
        $this->nom_suppleant = $nom_suppleant;
    }

}
