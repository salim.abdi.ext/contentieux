<?php

namespace Entities;

/**
 * @Entity
 * @Table(name="acces")
 */
class Acces {

    /**
     * @Column(type="integer",unique=true, nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Column(type="string", length=200, unique=true, nullable=false)
     */
    private $libelle;

    /**
     * @Column(type="string", length=150, unique=false, nullable=false)
     */
    private $classe;

    /**
     * @Column(type="string", length=150, unique=false, nullable=true)
     */
    private $methode;

    /**
     * @Column(type="smallint", unique=false, nullable=true)
     */
    private $generaliste;

    /**
     * @Column(type="smallint", unique=false, nullable=true)
     */
    private $aca;

    /**
     * @Column(type="smallint", unique=false, nullable=true)
     */
    private $rca;

    /**
     * @Column(type="smallint", unique=false, nullable=true)
     */
    private $da;

    /**
     * @Column(type="smallint", unique=false, nullable=true)
     */
    private $rcr;

    /**
     * @Column(type="smallint", unique=false, nullable=true)
     */
    private $dr;

    /**
     * @Column(type="smallint", unique=false, nullable=true)
     */
    private $dcn;

    /**
     * @Column(type="smallint", unique=false, nullable=true)
     */
    private $dj;

    /**
     * @Column(type="smallint", unique=false, nullable=true)
     */
    private $asqua;

    function getId() {
        return $this->id;
    }

    function getLibelle() {
        return $this->libelle;
    }

    function getClasse() {
        return $this->classe;
    }

    function getMethode() {
        return $this->methode;
    }

    function getGeneraliste() {
        return $this->generaliste;
    }

    function getAca() {
        return $this->aca;
    }

    function getRca() {
        return $this->rca;
    }

    function getDa() {
        return $this->da;
    }

    function getRcr() {
        return $this->rcr;
    }

    function getDr() {
        return $this->dr;
    }

    function getDcn() {
        return $this->dcn;
    }

    function getDj() {
        return $this->dj;
    }

    function getAsqua() {
        return $this->asqua;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setLibelle($libelle) {
        $this->libelle = $libelle;
    }

    function setClasse($classe) {
        $this->classe = $classe;
    }

    function setMethode($methode) {
        $this->methode = $methode;
    }

    function setGeneraliste($generaliste) {
        $this->generaliste = $generaliste;
    }

    function setAca($aca) {
        $this->aca = $aca;
    }

    function setRca($rca) {
        $this->rca = $rca;
    }

    function setDa($da) {
        $this->da = $da;
    }

    function setRcr($rcr) {
        $this->rcr = $rcr;
    }

    function setDr($dr) {
        $this->dr = $dr;
    }

    function setDcn($dcn) {
        $this->dcn = $dcn;
    }

    function setDj($dj) {
        $this->dj = $dj;
    }

    function setAsqua($asqua) {
        $this->asqua = $asqua;
    }

}
