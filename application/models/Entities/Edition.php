<?php

namespace Entities;

/**
 * @Entity(repositoryClass="EditionRepository")
 * @Table(name="edition")
 */
class Edition {

    /**
     * @Column(type="integer", length=32, unique=true, nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Column(type="decimal",nullable=false)
     */
    private $etat;

    /**
     * @Column(type="string", length=32, unique=false, nullable=false)
     */
    private $auteur;

    /**
     * @Column(type="string", length=100, unique=false, nullable=false)
     */
    private $mail_auteur;

    /**
     * @Column(type="text", nullable=true)
     */
    private $diffusion;

    /**
     * @Column(type="date")
     */
    private $date;

    /**
     * @Column(type="text",nullable=true)
     */
    private $data;

    /**
     * @Column(type="integer",nullable=false)
     */
    private $type_aspose;

    /**
     * @Column(type="string", length=150, unique=false, nullable=false)
     */
    private $nom_tmp;

    /**
     * @ManyToOne(targetEntity="Entities\Sinistre")
     */
    private $sinistre;

    /**
     * @ManyToOne(targetEntity="Entities\Statut")
     */
    private $statut;

    /**
     * @OneToOne(targetEntity="Entities\File")
     */
    private $document;

    function getId() {
        return $this->id;
    }

    function getEtat() {
        return $this->etat;
    }

    function getAuteur() {
        return $this->auteur;
    }

    function getSinistre() {
        return $this->sinistre;
    }

    function getStatut() {
        return $this->statut;
    }

    function getDiffusion() {
        return $this->diffusion;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setEtat($etat) {
        $this->etat = $etat;
    }

    function setAuteur($auteur) {
        $this->auteur = $auteur;
    }

    function setSinistre($sinistre) {
        $this->sinistre = $sinistre;
    }

    function setStatut($statut) {
        $this->statut = $statut;
    }

    function setDiffusion($diffusion) {
        $this->diffusion = $diffusion;
    }

    function getDocument() {
        return $this->document;
    }

    function setDocument($document) {
        $this->document = $document;
    }

    function getDate() {
        return $this->date;
    }

    function setDate($date) {
        $this->date = $date;
    }

    function getNom_tmp() {
        return $this->nom_tmp;
    }

    function setNom_tmp($nom_tmp) {
        $this->nom_tmp = $nom_tmp;
    }

    function getData() {
        return $this->data;
    }

    function setData($data) {
        $this->data = $data;
    }

    function getType_aspose() {
        return $this->type_aspose;
    }

    function setType_aspose($type_aspose) {
        $this->type_aspose = $type_aspose;
    }

    function getMail_auteur() {
        return $this->mail_auteur;
    }

    function setMail_auteur($mail_auteur) {
        $this->mail_auteur = $mail_auteur;
    }

}
