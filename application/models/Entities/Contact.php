<?php

namespace Entities;

/**
 * @Entity
 * @Table(name="contact")
 */
class Contact {

    /**
     * @Column(type="integer", length=32, unique=true, nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Column(type="string", length=50, unique=false, nullable=true)
     */
    private $nom;

    /**
     * @Column(type="string", length=50, unique=false, nullable=true)
     */
    private $prenom;

    /**
     * @Column(type="string", length=50, unique=false, nullable=true)
     */
    private $telephone;

    /**
     * @Column(type="string", length=50, unique=false, nullable=true)
     */
    private $email;

    /**
     * @OneToOne(targetEntity="Entities\Adresse", cascade={"all"})
     */
    private $adresse;

    /**
     * @ManyToOne(targetEntity="Entities\Statut_amiable", inversedBy="lo")
     */
    private $amiable;

    function getId() {
        return $this->id;
    }

    function getNom() {
        return $this->nom;
    }

    function getPrenom() {
        return $this->prenom;
    }

    function getTelephone() {
        return $this->telephone;
    }

    function getEmail() {
        return $this->email;
    }

    function getAdresse() {
        return $this->adresse;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNom($nom) {
        $this->nom = $nom;
    }

    function setPrenom($prenom) {
        $this->prenom = $prenom;
    }

    function setTelephone($telephone) {
        $this->telephone = $telephone;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setAdresse($adresse) {
        $this->adresse = $adresse;
    }

    function getAmiable() {
        return $this->amiable;
    }

    function setAmiable($amiable) {
        $this->amiable = $amiable;
    }

}
