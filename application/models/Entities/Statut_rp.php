<?php

namespace Entities;

use Entities\Adresse;
use Entities\Statut;

/**
 * @Entity(repositoryClass="Statut_JudiciaireRepository")
 * @Table
 */
class Statut_rp extends Statut {

	/**
	 * @Column(type="string", length=50, unique=false, nullable=true)
	 */
	private $ref_dossier_expert;

	/**
	 * @OneToOne(targetEntity="Entities\Sinistre", mappedBy="rp")
	 */
	private $sinistre;

	/**
	 * @OneToOne(targetEntity="Entities\Contact", cascade={"all"})
	 */
	private $expert_judiciaire;

	function getRef_dossier_expert() {
		return $this->ref_dossier_expert;
	}

	function getSinistre() {
		return $this->sinistre;
	}

	function getExpert_judiciaire() {
		return $this->expert_judiciaire;
	}

	function setRef_dossier_expert($ref_dossier_expert) {
		$this->ref_dossier_expert = $ref_dossier_expert;
	}

	function setSinistre($sinistre) {
		$this->sinistre = $sinistre;
	}

	function setExpert_judiciaire($expert_judiciaire) {
		$this->expert_judiciaire = $expert_judiciaire;
	}

}
