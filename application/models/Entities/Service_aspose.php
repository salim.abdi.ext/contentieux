<?php

namespace Entities;

/**
 * @Entity
 * @Table(name="service_aspose")
 */
class Service_aspose {

    /**
     * @Column(type="integer", length=32, unique=true, nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Column(type="integer", length=32, unique=false, nullable=true)
     */
    private $type;

    /**
     * @Column(type="text", unique=false, nullable=true)
     */
    private $data;

    /**
     * @Column(type="string", length=150, unique=false, nullable=false)
     */
    private $nom_fichier;

    /**
     * @Column(type="boolean", nullable=false)
     */
    private $preview;

    /**
     * @ManyToOne(targetEntity="Entities\Sinistre")
     */
    private $sinistre;

    /**
     * @ManyToOne(targetEntity="Entities\Statut")
     */
    private $statut;

    /**
     * @Column(type="string", unique=true, nullable=true, length=50)
     */
    private $guid;

    /**
     * @OneToOne(targetEntity="Entities\Edition")
     */
    private $edition;

    function getId() {
        return $this->id;
    }

    function getType() {
        return $this->type;
    }

    function getData() {
        return $this->data;
    }

    function getSinistre() {
        return $this->sinistre;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setType($type) {
        $this->type = $type;
    }

    function setData($data) {
        $this->data = $data;
    }

    function setSinistre($sinistre) {
        $this->sinistre = $sinistre;
    }

    function getBinary() {
        return $this->binary;
    }

    function setBinary($binary) {
        $this->binary = $binary;
    }

    function getGuid() {
        return $this->guid;
    }

    function setGuid($guid) {
        $this->guid = $guid;
    }

    function getStatut() {
        return $this->statut;
    }

    function setStatut($statut) {
        $this->statut = $statut;
    }

    function getEdition() {
        return $this->edition;
    }

    function setEdition($edition) {
        $this->edition = $edition;
    }

    function getNom_fichier() {
        return $this->nom_fichier;
    }

    function setNom_fichier($nom_fichier) {
        $this->nom_fichier = $nom_fichier;
    }

    function getPreview() {
        return $this->preview;
    }

    function setPreview($preview) {
        $this->preview = $preview;
    }
}
