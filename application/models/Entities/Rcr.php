<?php

namespace Entities;

/**
 * @Entity(repositoryClass="Entities\RcrRepository")
 * @Table(name="rcr")
 */
class Rcr {

    /**
     * @Column(type="integer", length=32, unique=true, nullable=false)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Column(type="string", length=32, unique=false, nullable=true)
     */
    private $id_rcr;

    /**
     * @Column(type="string", length=150, unique=false, nullable=true)
     */
    private $nom_rcr;

    /**
     * @Column(type="string", length=150, unique=false, nullable=true)
     */
    private $mail_rcr;

    /**
     * @Column(type="string", length=32, unique=false, nullable=true)
     */
    private $id_agence;

    /**
     * @Column(type="string", length=150, unique=false, nullable=true)
     */
    private $nom_agence;

    /**
     * @Column(type="string", length=150, unique=false, nullable=true)
     */
    private $nom_rca;

    /**
     * @Column(type="string", length=150, unique=false, nullable=true)
     */
    private $mail_rca;

    /**
     * @Column(type="string", length=50, unique=false, nullable=true)
     */
    private $id_rca;

    /**
     * @Column(type="string", length=150, unique=false, nullable=true)
     */
    private $nom_aca;

    /**
     * @Column(type="string", length=150, unique=false, nullable=true)
     */
    private $mail_aca;

    /**
     * @Column(type="string", length=50, unique=false, nullable=true)
     */
    private $id_aca;

    /**
     * @Column(type="string", length=50, unique=false, nullable=true)
     */
    private $cp_agence;

    function getId() {
        return $this->id;
    }

    function setId($id) {
        $this->id = $id;
    }

    function getId_rcr() {
        return $this->id_rcr;
    }

    function getId_agence() {
        return $this->id_agence;
    }

    function setId_rcr($id_rcr) {
        $this->id_rcr = $id_rcr;
    }

    function setId_agence($id_agence) {
        $this->id_agence = $id_agence;
    }

    function getNom_rcr() {
        return $this->nom_rcr;
    }

    function getMail_rcr() {
        return $this->mail_rcr;
    }

    function getNom_agence() {
        return $this->nom_agence;
    }

    function setNom_rcr($nom_rcr) {
        $this->nom_rcr = $nom_rcr;
    }

    function setMail_rcr($mail_rcr) {
        $this->mail_rcr = $mail_rcr;
    }

    function setNom_agence($nom_agence) {
        $this->nom_agence = $nom_agence;
    }

    function getNom_rca() {
        return $this->nom_rca;
    }

    function getId_rca() {
        return $this->id_rca;
    }

    function setNom_rca($nom_rca) {
        $this->nom_rca = $nom_rca;
    }

    function setId_rca($id_rca) {
        $this->id_rca = $id_rca;
    }

    function getMail_rca() {
        return $this->mail_rca;
    }

    function setMail_rca($mail_rca) {
        $this->mail_rca = $mail_rca;
    }

    function getCp_agence() {
        return $this->cp_agence;
    }

    function setCp_agence($cp_agence) {
        $this->cp_agence = $cp_agence;
    }

    function getNom_aca() {
        return $this->nom_aca;
    }

    function getMail_aca() {
        return $this->mail_aca;
    }

    function getId_aca() {
        return $this->id_aca;
    }

    function setNom_aca($nom_aca) {
        $this->nom_aca = $nom_aca;
    }

    function setMail_aca($mail_aca) {
        $this->mail_aca = $mail_aca;
    }

    function setId_aca($id_aca) {
        $this->id_aca = $id_aca;
    }

}
