<?php

namespace Entities;

use Entities\Adresse;
use Entities\Statut;
use Entities\Contact;

/**
 * @Entity(repositoryClass="Statut_DORepository")
 */
Class Statut_DO extends Statut {

    /**
     * @Column(type="string", length=50, unique=false, nullable=false)
     */
    private $ref_dossier_expert;

    /**
     * @Column(type="date", length=32, unique=false, nullable=false)
     */
    private $date_declaration_do;

    /**
     * @Column(type="date", length=32, unique=false, nullable=false)
     */
    private $date_premiere_reunion_expertise;

    /**
     * @OneToOne(targetEntity="Entities\Sinistre", mappedBy="do")
     */
    private $sinistre;

    /**
     * @OneToOne(targetEntity="Entities\Contact", cascade={"all"})
     */
    private $cabinet_expert;

    /**
     * @OneToOne(targetEntity="Entities\Contact", cascade={"all"})
     */
    private $contact_expert;

    function getRef_dossier_expert() {
        return $this->ref_dossier_expert;
    }

    function getDate_declaration_do() {
        return $this->date_declaration_do;
    }

    function getDate_premiere_reunion_expertise() {
        return $this->date_premiere_reunion_expertise;
    }

    function getSinistre() {
        return $this->sinistre;
    }

    function getCabinet_expert() {
        return $this->cabinet_expert;
    }

    function getContact_expert() {
        return $this->contact_expert;
    }

    function setRef_dossier_expert($ref_dossier_expert) {
        $this->ref_dossier_expert = $ref_dossier_expert;
    }

    function setDate_declaration_do($date_declaration_do) {
        $this->date_declaration_do = $date_declaration_do;
    }

    function setDate_premiere_reunion_expertise($date_premiere_reunion_expertise) {
        $this->date_premiere_reunion_expertise = $date_premiere_reunion_expertise;
    }

    function setSinistre($sinistre) {
        $this->sinistre = $sinistre;
    }

    function setCabinet_expert($cabinet_expert) {
        $this->cabinet_expert = $cabinet_expert;
    }

    function setContact_expert($contact_expert) {
        $this->contact_expert = $contact_expert;
    }

}
