<?php

class Aspose_model extends Dao {

    public function __construct() {
        parent::__construct();
    }

    public function get_row_service_aspose($id) {
        $query = "SELECT * FROM service_aspose WHERE id = ?";
        return $this->contentieux->query($query, array($id))->row();
    }

    public function delete_row($id) {
        $query = "DELETE FROM service_aspose WHERE id = ?";
        $this->contentieux->query($query, array($id));
    }

}
