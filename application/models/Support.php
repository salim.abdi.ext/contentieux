<?php

class Support extends CI_Model {
    
    private $gaia;
    
    public function __construct() {
        parent::__construct();
        $this->gaia = $this->load->database('gaia', true);
    }


    public function add($id_service, $id_collab_artemis, $nom_auteur, $urgence, $type, $objet, $text, $code_affaire) {
        $date = date('Y-m-d H:i:s');
        $query = <<<SQL
                INSERT INTO gaia_ticket (tkt_application, tkt_id_service, tkt_auteur_id, tkt_auteur, tkt_date_creation, tkt_statut, tkt_urgence, tkt_type, tkt_objet, tkt_description, tkt_code_affaire)
                                 VALUES ('3', '$id_service', '$id_collab_artemis', '$nom_auteur', '$date', '1', '$urgence', '$type', '$objet', '$text', '$code_affaire')
                
SQL;
        
        $this->gaia->query($query);
    }
}
