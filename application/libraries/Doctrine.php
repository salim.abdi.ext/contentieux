<?php

use Doctrine\Common\ClassLoader,
    Doctrine\ORM\Configuration,
    Doctrine\ORM\EntityManager,
    Doctrine\Common\Cache\ArrayCache,
    Doctrine\DBAL\Logging\EchoSQLLogger;

/**
 * @property EntityManager $em
 */
class Doctrine {

    public $em = null;

    public function __construct() {
        // load database configuration from CodeIgniter
        require_once APPPATH . 'config/' . ENVIRONMENT . '/database.php';

        // Set up class loading. You could use different autoloaders, provded by your favorite framework,
        // if you want to.
        require_once APPPATH . 'third_party/vendor/autoload.php';

        $doctrineClassLoader = new ClassLoader('Doctrine', APPPATH . 'libraries');
        $doctrineClassLoader->register();
        $entitiesClassLoader = new ClassLoader('Entities', APPPATH . 'models');
        $entitiesClassLoader->register();
        $proxiesClassLoader = new ClassLoader('Proxies', APPPATH . 'models');
        $proxiesClassLoader->register();

        // Set up caches
        $config = new Configuration;
        $cache = new ArrayCache();
        //$cache = new Doctrine\Common\Cache\ApcCache();
        $config->setMetadataCacheImpl($cache);
        $driverImpl = $config->newDefaultAnnotationDriver(array(APPPATH . 'models/Entities'));

        $config->setMetadataDriverImpl($driverImpl);
        $config->setQueryCacheImpl($cache);

        // Proxy configuration
        $config->setProxyDir(APPPATH . 'models/Proxies');
        $config->setProxyNamespace('Proxies');

        // Set up logger
        //$logger = new EchoSQLLogger;
        //$config->setSQLLogger($logger);


        $config->setAutoGenerateProxyClasses(false);

        // Database connection information
        $connectionOptions = array(
            'driver' => 'sqlsrv',
            'user' => $db['contentieux']['username'],
            'password' => $db['contentieux']['password'],
            'host' => $db['contentieux']['hostname'],
            'dbname' => $db['contentieux']['database'],
            'charset' => $db['contentieux']['char_set']
        );

        // Create EntityManager
        $this->em = EntityManager::create($connectionOptions, $config);
    }

}
