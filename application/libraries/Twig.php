<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * Library to wrap Twig layout engine. Originally from Bennet Matschullat.
 * Code cleaned up to CodeIgniter standards by Erik Torsner
 *
 * PHP Version 5.3
 *
 * @category Layout
 * @package  Twig
 * @author   Bennet Matschullat <bennet@3mweb.de>
 * @author   Erik Torsner <erik@torgesta.com>
 * @license  Don't be a dick http://www.dbad-license.org/
 * @link     https://github.com/bmatschullat/Twig-Codeigniter
 */

/**
 * Main (and only) class for the Twig wrapper library
 * 
 * @category Layout
 * @package  Twig
 * @author   Bennet Matschullat <hello@bennet-matschullat.com>
 * @author   Erik Torsner <erik@torgesta.com>
 * @license  Don't be a dick http://www.dbad-license.org/
 * @link     https://github.com/bmatschullat/Twig-Codeigniter
 */
class Twig {

    const TWIG_CONFIG_FILE = 'twig';

    /**
     * Path to templates. Usually application/views.
     * 
     * @var string
     */
    protected $template_dir;

    /**
     * Path to cache.  Usually applcation/cache.
     * 
     * @var string
     */
    protected $cache_dir;

    /**
     * Reference to code CodeIgniter instance.
     * 
     * @var CodeIgniter object
     */
    private $_ci;

    /**
     * Twig environment see http://twig.sensiolabs.org/api/v1.8.1/Twig_Environment.html.
     * 
     * @var Twig_Envoronment object
     */
    private $_twig_env;

    /**
     * constructor of twig ci class
     */
    public function __construct() {
        $this->_ci = & get_instance();
        $this->_ci->config->load(self::TWIG_CONFIG_FILE); // load config file
        // set include path for twig
        require_once APPPATH . 'third_party/vendor/autoload.php';
        // init paths
        $this->template_dir = $this->_ci->config->item('template_dir');
        $this->cache_dir = $this->_ci->config->item('cache_dir');
        // load environment
        $loader = new \Twig\Loader\FilesystemLoader($this->template_dir, $this->cache_dir);
        $this->_twig_env = new \Twig\Environment($loader, array(
            'cache' => $this->cache_dir,
            'auto_reload' => TRUE,
            //'debug' => TRUE
            ));

        // assets perso
        $this->_twig_env->addGlobal('assetsdir', str_replace("\\", "/", urldecode("https://" . str_replace($_SERVER['DOCUMENT_ROOT'], $_SERVER['HTTP_HOST'], realpath(APPPATH . "../assets")))));
        $this->_twig_env->addGlobal('base_url', str_replace("\\", "/", urldecode("https://" . str_replace($_SERVER['DOCUMENT_ROOT'], $_SERVER['HTTP_HOST'], realpath(APPPATH . "../")))));
        $this->_twig_env->addGlobal('ajax_url', str_replace("\\", "/", urldecode("https://" . str_replace($_SERVER['DOCUMENT_ROOT'], $_SERVER['HTTP_HOST'], realpath(APPPATH . "../")))));
        $this->_twig_env->addGlobal('missions_cloture', unserialize(LISTE_MISSIONS_CLOTURE));

        $this->ci_function_init();
    }

    /**
     * render a twig template file
     * 
     * @param string  $template template name
     * @param array   $data	    contains all varnames
     * @param boolean $render   render or return raw?
     *
     * @return void
     * 
     */
    public function render($template, $data = array(), $render = TRUE) {
        $data['context'] = $this->_ci->context->get();

        $template = $this->_twig_env->load($template);
        log_message('debug', 'twig template loaded');
        return ($render) ? $template->render($data) : $template;
    }

    /**
     * Execute the template and send to CI output
     * 
     * @param string $template Name of template
     * @param array  $data     Parameters for template
     * 
     * @return void
     * 
     */
    public function display($template, $data = array()) {

        $data['context'] = $this->_ci->context->get();
        $user = $this->_ci->session->userdata('user');

        $template = $this->_twig_env->load($template);
        $this->_ci->output->set_output($template->render($data));
    }

    /**
     * Entry point for controllers (and the likes) to register
     * callback functions to be used from Twig templates
     * 
     * @param string                 $name     name of function
     * @param Twig_FunctionInterface $function Function pointer
     * 
     * @return void
     * 
     */
    public function register_function($name, Twig_FunctionInterface $function) {
        $this->_twig_env->addFunction($name, $function);
    }

    /**
     * Initialize standard CI functions
     * 
     * @return void
     */
    public function ci_function_init() {
        $this->_twig_env->addFunction(new \Twig\TwigFunction('base_url', 'base_url'));
//        $this->_twig_env->addFunction('site_url', new \Twig\TwigFunction('site_url'));
//        $this->_twig_env->addFunction('current_url', new \Twig\TwigFunction('current_url'));
    }

}

/* End of file Twig.php */
/* Location: ./libraries/Twig.php */
