<?php

/**
 * Classe pour la synchro asqua, sert également à toute action journalière, un cron execute synchroAsqua() tous les jours à 4h30
 */
require 'Sinistre_asqua.php';

class Asqua_lib {

    private $_ci;

    public function __construct() {
        $this->_ci = &get_instance();
    }

    public function test() {

        $srepo = $this->_ci->doctrine->em->getRepository('Entities\Sinistre');
        $qb = $this->_ci->doctrine->em->createQueryBuilder();

        $qb->select('s')
                ->from('Entities\sinistre', 's')
                ->where($qb->expr()->isNull('s.flag_sinistre_desactive'));
        $allSin = $qb->getQuery()->getResult();

        $cntSin = count($allSin);
        echo "nb sinistres : " . $cntSin . "<br>";

        $qb = $this->_ci->doctrine->em->createQueryBuilder();

        $qb->select('s')
                ->from('Entities\sinistre', 's')
                ->where($qb->expr()->isNotNull('s.ref_asqua'));
        $n = $qb->getQuery()->getResult();
        echo "nb sinistres avec ref asqua : " . count($n) . "<br>";

        $qb = $this->_ci->doctrine->em->createQueryBuilder();

        $qb->select('s')
                ->from('Entities\sinistre', 's')
                ->where($qb->expr()->isNull('s.date_derniere_synchro'))
                ->andWhere($qb->expr()->isNotNull('s.ref_asqua'));
        $n2 = $qb->getQuery()->getResult();
        echo "nb sinistres synchros: " . count($n2);

        echo "<br>";
        foreach ($n as $sin) {
            echo $sin->getAffaire()->getCode() . " " . $sin->getNum_chrono() . " - " . $sin->getRef_asqua() . "<br>";
        }
    }

    public function ref_asqua($id, $guid) {
        $sinistre = $this->_ci->doctrine->em->getRepository('Entities\Sinistre')->find($id);
        if (!$sinistre)
            redirect(404);
        $token = $sinistre->getToken_asqua();
        if ($guid == $token) {
            if (isset($_POST['ref_asqua'])) {
                mb_internal_encoding('UTF-8');
                $ref_trimee = preg_replace("/(.*\/|[^\.a-zA-Z\d:]*)/u", '', $_POST['ref_asqua']);
                $sinistre->setRef_asqua($ref_trimee);
                $sinistre->setToken_asqua(null);
                $this->_ci->doctrine->em->persist($sinistre);
                $this->_ci->doctrine->em->flush();
                $this->_ci->twig->display('mails/asqua.html.twig', array('done' => true));
            } else {
                $this->_ci->twig->display('mails/asqua.html.twig');
            }
        } else {
            $this->_ci->twig->display('mails/asqua.html.twig', array('alreadydone' => true));
        }
    }

    private function update_new_sinistre() {
        $axa = APPPATH . '../files/asqua/gaia_axa.csv';
        $sma = APPPATH . '../files/asqua/gaia_sma.csv';

        $arr_1 = $this->parse_file($axa);
        $arr_2 = $this->parse_file($sma);
        array_shift($arr_1);
        array_shift($arr_2);

        $array = array_merge($arr_1, $arr_2);

        $this->process($array);
    }

    public function synchroAsqua($pass, $ref_asqua, $update = true) {
        //ob_implicit_flush(true);
        //ob_start();

        echo "Synchronisation Asqua ...<br>";

//        if ($_SERVER['REMOTE_ADDR'] != 'localhost' && $_SERVER['REMOTE_ADDR'] != '192.168.100.49') {
//            echo 'echec';
//            die();
//        }

        if ($pass != 'gLr887sQ') {
            die('Acces refusé');
        }

        if ($update == true) {
            echo "Telechargement des fichiers... <br>";
            $server = '87.238.144.42';
            $user = "qualiconsult";
            $pass = "larit2PP*";
            $nom_f1 = 'gaia_axa.csv';
            $nom_f2 = 'gaia_sma.csv';

            $file1 = $this->get_file_ftp($server, $user, $pass, $nom_f1);
            $file2 = $this->get_file_ftp($server, $user, $pass, $nom_f2);
            if ($file2 == false || $file1 == false) {
                echo 'erreur';
                echo 'Impossible de récuperer le fichier';
                //ob_flush();
                die();
            } else {
                var_dump($file1);
            }
        } else {
            echo "Pas de maj des fichiers ... <br>";

            $file1 = APPPATH . '../files/asqua/gaia_axa.csv';
            $file2 = APPPATH . '../files/asqua/gaia_sma.csv';
        }
        ob_flush();
        echo "Parsing file1..<br>";
        $arr1 = $this->parse_file($file1);
        echo "Parsing file2..<br>";
        $arr2 = $this->parse_file($file2);

        array_shift($arr1);
        array_shift($arr2);

        $array = array_merge($arr1, $arr2);
        echo "Process...<br>";
        ob_flush();
        $this->process($array, $ref_asqua);

        echo "Check grille analyse...";
        if (!$ref_asqua) {
            $this->checkGrillesAnalyse();
        }
    }

    private function log_msg($message) {
        file_put_contents(APPPATH . '../logs/synchro_asqua.txt', PHP_EOL . date('d-m-Y h:i:s') . "  -  " . $message);
    }

    private function get_file_ftp($server, $user, $pass, $file = null) {
        $path = APPPATH . '../files/asqua/' . $file;

        $conn = ftp_connect($server);
        $login_result = ftp_login($conn, $user, $pass);

        if ($login_result == false) {
            $this->log_msg("Erreur de connexion au ftp");
            echo "Erreur ftp : " . $server . " / " . $user . " / " . $pass;
            ftp_close($conn);
            die();
        }
        $file_name = 'test.txt';
        if (ftp_get($conn, $path, "array1/Partage_Web/" . $file, FTP_BINARY)) {
            $this->log_msg("Fichier recupere : " . $file);
        } else {
            $this->log_msg("Erreur de recuperation : " . $file);
            return false;
        }
        ftp_close($conn);
        echo 'Fichier récupéré :' . $file . '<br>';
        return $path;
    }

    private function parse_file($file) {
        $doc = array();
        $handle = fopen($file, 'r');
        while (!feof($handle)) {
            $data = fgetcsv($handle, 0, "|");
            if (is_array($data)) {
                foreach ($data as $i => $k) {
                    $data[$i] = utf8_encode($k);
                }
                $doc[] = $data;
            }
        }

        return $doc;
    }

    private function find_sinistre($row) {
        echo "Recherche sinistre ref : $row[3]";
        if ($row[4] != "ref provisoire" && $row[4] != "")
            echo ' ,ref Provisoire :' . $row[4];
        echo "<br>";
        $repo = $this->_ci->doctrine->em->getRepository('Entities\Sinistre');
        $asqua = $row[3];
        $asqua_temp = $row[4];
        $sinistres = $repo->findBy(array('ref_asqua' => $asqua));
        if ($sinistres) {
            echo "<span style='color:green'>" . count($sinistres) . ' Sinistre(s) trouvés </span><br>';
            foreach ($sinistres as $sinistre) {
                $sinistre->setDate_derniere_vue_asqua(new DateTime());
                $this->_ci->doctrine->em->persist($sinistre);
            }
            $this->_ci->doctrine->em->flush();
            return $sinistres;
        } else {
            $sinistres = $repo->findBy(array('ref_asqua' => $asqua_temp));
            if (count($sinistres) > 0) {
                foreach ($sinistres as $sinistre) {
                    $sinistre->setRef_asqua($asqua);
                    $this->_ci->doctrine->em->persist($sinistre);
                }
                $this->_ci->doctrine->em->flush();
                return $sinistres;
            }
        }
        return false;
    }

    private function process($array, $ref_asqua) {
        $i = 0;
        echo 'Traitement du fichier, ' . count($array) . ' lignes<br>';

        $nb_synchronises = 0;
        $nb_erreurs = 0;
        $date = new DateTime();
        $c = 1;
        foreach ($array as $row) {
            echo $row[3] . "\n";
            if ($ref_asqua && ($row[3] != $ref_asqua && $row[4] != $ref_asqua)) {
                continue;
            }
            try {
                echo $c . "<br>";
                $c++;

                $sinistres = $this->find_sinistre($row);

                if ($sinistres) {
                    foreach ($sinistres as $sinistre) {
                        if ($this->is_synchronized($row, $sinistre)) {
                            echo "Sinistre déjà synchronisé <br>";
                            continue;
                        }
                        $mapped_row = new Sinistre_asqua($row);
                        echo "Synchronisation...<br>";
                        $this->synchronize($mapped_row, $sinistre);
                    }
                } else {
                    echo "Sinistre non trouvé<br>";
                }
                $i++;
            } catch (Exception $e) {
                continue;
            }
        }
    }

    private function is_synchronized($row, $sinistre) {
        $date_modif_asqua = date_create_from_format("d/m/Y h:i:s", $row[0]);
        $date_synchro_sinistre = $sinistre->getDate_derniere_synchro();
        $statuts_asqua = array(
            "DO" => 1,
            "AV1" => 2,
            "judiciaire" => 3,
            "autre" => 4
        );
        echo "sinistre : " . $sinistre->getStatut() . "<br>";
        echo "statut asqua : " . $statuts_asqua[$row[2]] . "<br>";
        if ($date_synchro_sinistre != null && $sinistre->getStatut() == $statuts_asqua[$row[2]]) {
            echo "sinistre " . $sinistre->getId() . " - " . $sinistre->getRef_asqua() . "(" . $sinistre->getNom() . ") deja synchro<br>";
            return true;
        } else {
            return false;
        }
    }

    private function synchronize(Sinistre_asqua $asqua, Entities\Sinistre $sinistre) {
        echo "Synchro sinistre " . $sinistre->getId() . "<br>";
        $r1 = "";
        $r2 = "";
        $i = 0;
        foreach ($asqua as $k => $v) {
            $r1 .= "<td>" . $k . "</td>";
            if (!$v instanceof DateTime)
                $r2 .= "<td>" . $v . "</td>";
            else
                $r2 .= "<td>" . $v->format('d/m/Y') . "</td>";
            $i++;
        }
        echo "<table><tr><td colspan=$i>Données asqua</td></tr>";
        echo "<tr>$r1</tr><tr>$r2</tr></table><br>";
        $hist = new \Entities\Historique();
        $this->update_sinistre($asqua, $sinistre);
        $statut = $asqua->statut;
        switch ($statut) {
            case "DO":
                $this->update_do($asqua, $sinistre);
                break;
            case "AV1":
                $this->update_av1($asqua, $sinistre);
                break;
            case "judiciaire":
                $this->update_judiciaire($asqua, $sinistre);
                break;
            case "autre":
            case "":
                $this->update_autre($asqua, $sinistre);
                break;
        }
    }

    private function update_do(Sinistre_asqua $asqua, \Entities\Sinistre $sinistre) {
        echo "Statut : DO <br>";
        $do = $sinistre->getDo();
        if (!$do instanceof \Entities\Statut_DO) {
            $repo = $this->_ci->doctrine->em->getRepository('Entities\Sinistre');
            $do = $repo->newDo();
            $sinistre->setDo($do);
            $sinistre->setStatut(1);
            $histRepo = $this->_ci->doctrine->em->getRepository('Entities\Historique');
            $chrono = $histRepo->getNextChrono(4, $sinistre);
            $hist = new \Entities\Historique();
            $hist->setDate(new DateTime());
            $hist->setNum_chrono($chrono);
            $hist->setSinistre($sinistre);
            $hist->setStatut($do);
            $hist->setType(3);
            $hist->setLibelle("Passage du sinistre en DO");
            $hist->setData(array('expediteur' => 'System'));

            $this->_ci->doctrine->em->persist($hist);
        }
        $expert = $do->getContact_expert();
        $cabexpert = $do->getCabinet_expert();
        $adresse_expert = $cabexpert->getAdresse();


        $do->setRef_dossier_expert($asqua->ref_expert_do);
        //$do->setDate_declaration_do($asqua->date_derniere_modif);
        $expert->setNom($asqua->nom_expert_do);
        $expert->setPrenom($asqua->prenom_expert_do);
        $expert->setTelephone($asqua->tel_expert_do);
        $expert->setEmail($asqua->mail_expert_do);
        $adresse_expert->setRue1($asqua->rue1_expert_do);
        $adresse_expert->setRue2($asqua->rue2_expert_do);
        $adresse_expert->setRue3($asqua->rue3_expert_do);
        $adresse_expert->setCommune($asqua->commune_expert_do);
        $adresse_expert->setCp($asqua->cp_expert_do);

        $this->_ci->doctrine->em->persist($adresse_expert);
        $this->_ci->doctrine->em->persist($expert);
        $this->_ci->doctrine->em->persist($cabexpert);
        $this->_ci->doctrine->em->persist($sinistre);
        $this->_ci->doctrine->em->persist($do);

        $this->_ci->doctrine->em->flush();
    }

    private function update_av1(Sinistre_asqua $asqua, Entities\Sinistre $sinistre) {
        echo "Statut Av1";
        $av1 = $sinistre->getAv1();

        if (!$av1 instanceof \Entities\Statut_AV1) {
            $repo = $this->_ci->doctrine->em->getRepository('Entities\Sinistre');
            $av1 = $repo->newAv1();
            $sinistre->setAv1($av1);

            $sinistre->setStatut(2);
            $histRepo = $this->_ci->doctrine->em->getRepository('Entities\Historique');
            $chrono = $histRepo->getNextChrono(4, $sinistre);
            $hist = new \Entities\Historique();
            $hist->setDate(new DateTime());
            $hist->setNum_chrono($chrono);
            $hist->setSinistre($sinistre);
            $hist->setStatut($av1);
            $hist->setType(3);
            $hist->setLibelle("Passage du sinistre en AV1");
            $hist->setData(array('expediteur' => 'System'));

            $this->_ci->doctrine->em->persist($hist);
        }

        $cabexpert = $av1->getCabinet_expert();
        $cabexpertcie = $av1->getCabinet_expert_compagnie();
        $adresse_expert = $cabexpert->getAdresse();
        $adresse_expertcie = $cabexpertcie->getAdresse();
        $expert = $av1->getContact_expert();
        $expertcie = $av1->getContact_expert_compagnie();


        $av1->setRef_dossier_expert($asqua->ref_expert_do);
        $av1->setRef_dossier_expert_compagnie($asqua->ref_expert_cie);

        $expert->setPrenom($asqua->prenom_expert_do);
        $expert->setNom($asqua->nom_expert_do);
        $expert->setEmail($asqua->mail_expert_do);
        $expert->setTelephone($asqua->tel_expert_do);
        $adresse_expert->setRue1($asqua->rue1_expert_do);
        $adresse_expert->setRue2($asqua->rue2_expert_do);
        $adresse_expert->setRue3($asqua->rue3_expert_do);
        $adresse_expert->setCp($asqua->cp_expert_do);
        $adresse_expert->setCommune($asqua->commune_expert_do);
        $expertcie->setNom($asqua->nom_expert_cie);
        $expertcie->setPrenom($asqua->prenom_expert_cie);
        $expertcie->setEmail($asqua->mail_expert_cie);
        $expertcie->setTelephone($asqua->tel_expert_cie);
        $adresse_expertcie->setRue1($asqua->rue1_expert_cie);
        $adresse_expertcie->setRue2($asqua->rue2_expert_cie);
        $adresse_expertcie->setRue3($asqua->rue3_expert_cie);
        $adresse_expertcie->setCp($asqua->cp_expert_cie);
        $adresse_expertcie->setCommune($asqua->commune_expert_cie);
        $cabexpertcie->setNom($asqua->cabinet_expert_cie);

        $this->_ci->doctrine->em->persist($expert);
        $this->_ci->doctrine->em->persist($expertcie);
        $this->_ci->doctrine->em->persist($cabexpert);
        $this->_ci->doctrine->em->persist($cabexpertcie);
        $this->_ci->doctrine->em->persist($adresse_expert);
        $this->_ci->doctrine->em->persist($adresse_expertcie);
        $this->_ci->doctrine->em->persist($av1);
        $this->_ci->doctrine->em->flush();
    }

    private function update_judiciaire(Sinistre_asqua $asqua, Entities\Sinistre $sinistre) {
        $judiciaire = $sinistre->getJudiciaire();
        if (!$judiciaire instanceof \Entities\Statut_judiciaire) {
            $repo = $this->_ci->doctrine->em->getRepository('Entities\Sinistre');
            $judiciaire = $repo->newJud();
            $sinistre->setJudiciaire($judiciaire);

            $sinistre->setStatut(3);
            $histRepo = $this->_ci->doctrine->em->getRepository('Entities\Historique');
            $chrono = $histRepo->getNextChrono(4, $sinistre);
            $hist = new \Entities\Historique();
            $hist->setDate(new DateTime());
            $hist->setNum_chrono($chrono);
            $hist->setSinistre($sinistre);
            $hist->setStatut($judiciaire);
            $hist->setType(3);
            $hist->setLibelle("Passage du sinistre en Judiciaire");
            $hist->setData(array('expediteur' => 'System'));

            $this->_ci->doctrine->em->persist($hist);
        }

        $expert = $judiciaire->getExpert_judiciaire();
        $adresse_expert = $expert->getAdresse();
        $expertcie = $judiciaire->getExpert_compagnie_judiciaire();
        $cabexpertcie = $judiciaire->getCabinet_expert_compagnie_judiciaire();
        $adresse_cie = $cabexpertcie->getAdresse();
        $avocat = $judiciaire->getAvocat_dossier();
        $cabavocat = $judiciaire->getCabinet_avocat();
        $adresse_avocat = $cabavocat->getAdresse();

        $judiciaire->setRef_dossier_expert_compagnie($asqua->ref_expert_cie);
        $judiciaire->setRef_dossier_expert($asqua->ref_expert_do);
        $judiciaire->setRef_dossier_avocat($asqua->ref_avocat);

        $expert->setNom($asqua->nom_expert_jud);
        $expert->setPrenom($asqua->prenom_expert_jud);
        $expert->setEmail($asqua->mail_expert_jud);
        $expert->setTelephone($asqua->tel_expert_jud);

        $adresse_expert->setRue1($asqua->rue1_expert_jud);
        $adresse_expert->setRue2($asqua->rue2_expert_jud);
        $adresse_expert->setRue3($asqua->rue3_expert_jud);
        $adresse_expert->setCp($asqua->cp_expert_jud);
        $adresse_expert->setCommune($asqua->commune_expert_jud);

        $expertcie->setNom($asqua->nom_expert_cie);
        $expertcie->setPrenom($asqua->prenom_expert_cie);
        $expertcie->setEmail($asqua->mail_expert_cie);
        $expertcie->setTelephone($asqua->tel_expert_cie);

        $adresse_cie->setRue1($asqua->rue1_expert_cie);
        $adresse_cie->setRue2($asqua->rue2_expert_cie);
        $adresse_cie->setRue3($asqua->rue3_expert_cie);
        $adresse_cie->setCp($asqua->cp_expert_cie);
        $adresse_cie->setCommune($asqua->commune_expert_cie);

        $avocat->setNom($asqua->nom_avocat);
        $avocat->setPrenom($asqua->prenom_avocat);
        $avocat->setEmail($asqua->mail_avocat);
        $avocat->setTelephone($asqua->tel_avocat);

        $adresse_avocat->setRue1($asqua->rue1_avocat);
        $adresse_avocat->setRue2($asqua->rue2_avocat);
        $adresse_avocat->setRue3($asqua->rue3_avocat);
        $adresse_avocat->setCp($asqua->cp_avocat);
        $adresse_avocat->setCommune($asqua->commune_avocat);

        $this->_ci->doctrine->em->persist($judiciaire);
        $this->_ci->doctrine->em->flush();



        echo 'statut judiciaire';
    }

    private function update_autre(Sinistre_asqua $asqua, Entities\Sinistre $sinistre) {
        echo 'update autre';
        $autre = $sinistre->getAmiable();

        if (!$autre instanceof Entities\Statut_amiable) {
            $repo = $this->_ci->doctrine->em->getRepository('Entities\Sinistre');
            $autre = $repo->newAmiable();
            $sinistre->setAmiable($autre);

            $sinistre->setStatut(4);
            $histRepo = $this->_ci->doctrine->em->getRepository('Entities\Historique');
            $chrono = $histRepo->getNextChrono(4, $sinistre);
            $hist = new \Entities\Historique();
            $hist->setDate(new DateTime());
            $hist->setNum_chrono($chrono);
            $hist->setSinistre($sinistre);
            $hist->setStatut($autre);
            $hist->setType(3);
            $hist->setLibelle("Passage du sinistre en 'Autre'");
            $hist->setData(array('expediteur' => 'System'));

            $this->_ci->doctrine->em->persist($hist);
        }
        $expert = $autre->getExpert_compagnie();
        $cabexpert = $autre->getCabinet_expert_compagnie();
        $adresse = $cabexpert->getAdresse();

        $expert->setNom($asqua->nom_expert_cie);
        $expert->setPrenom($asqua->prenom_expert_cie);
        $expert->setEmail($asqua->mail_expert_cie);
        $expert->setTelephone($asqua->tel_expert_cie);

        $cabexpert->setNom($asqua->cabinet_expert_cie);

        $adresse->setRue1($asqua->rue1_expert_cie);
        $adresse->setRue2($asqua->rue2_expert_cie);
        $adresse->setRue3($asqua->rue3_expert_cie);
        $adresse->setCp($asqua->cp_expert_cie);
        $adresse->setCommune($asqua->commune_expert_cie);


        $autre->setRef_dossier_expert_compagnie($asqua->ref_expert_cie);

        $this->_ci->doctrine->em->persist($autre);
        $this->_ci->doctrine->em->flush();
    }

    private function update_sinistre(Sinistre_asqua $asqua, Entities\Sinistre $sinistre) {

        echo 'Synchro du sinistre <br>';
        $adresse = $sinistre->getAdresse();
        if ($asqua->date_survenance != "" && $asqua->date_survenance != null)
            $sinistre->setDate_declaration($asqua->date_survenance);
        if ($asqua->date_derniere_modif != "" && $asqua->date_derniere_modif != null)
            $sinistre->setDate_derniere_synchro($asqua->date_derniere_modif);
        if ($asqua->droc != "" && $asqua->droc != null)
            $sinistre->setDroc($asqua->droc);
        $sinistre->setDescription($asqua->description);
        $sinistre->setAssureur($asqua->base_asqua);
        $sinistre->setNom($asqua->libelle);

        if ($asqua->ref_asqua != null & $asqua->ref_asqua != '')
            $sinistre->setRef_asqua($asqua->ref_asqua);
        else
            $sinistre->setRef_asqua($asqua->ref_provisoire);

        switch ($asqua->statut) {
            case 'DO':
                $do = $sinistre->getDo();
                if (!$do instanceof Entities\Statut_DO) {
                    $do = new Entities\Statut_DO();
                }
        }
    }

    public function synchro_demo(Entities\Sinistre $sinistre) {
        ob_start();
        $statuts = array("", "DO", "AV1", "judiciaire", "autre");

        $sinistre_asqua_demo = new Sinistre_asqua(array(
            date("d/m/Y"),
            "AXA",
            $statuts[$sinistre->getStatut_initial()],
            "REF_TEST_ASQUA",
            "",
            "Sinistre démo",
            "Description sinistre démo",
            "En cours",
            date("d/m/Y"),
            date("d/m/Y"),
            "BatiDec12345",
            ($sinistre->getStatut_initial() == 3) ? "OUI" : "NON",
            ($sinistre->getStatut_initial() == 1 || $sinistre->getStatut_initial() == 2) ? "OUI" : "NON",
            "AXA",
            "Jean",
            "Dupont",
            "Cabinet Dupont",
            "3 Rue de la gare",
            "",
            "78140",
            "Vélizy",
            "0140837647",
            "jean@cabinetdupont.fr",
            "REF_TEST_EXPERTDO",
            "CRISTALIS",
            "Jean-Louis",
            "BOUE",
            "Cabinet Cristalis",
            "4 Rue de la gare",
            "",
            "78140",
            "Vélizy",
            "0140837647",
            "jl.boue@cristalis.fr",
            "REF_TEST_EXPERTCIE",
            "Letort",
            "Cabinet letort",
            "5 Rue de la gare",
            "",
            "78140",
            "Vélizy",
            "Georges",
            "Letort",
            "0140837647",
            "georges@letort.fr",
            "Delormeau & co",
            "Delormeau & co",
            "6 rue de la gare",
            "",
            "78140",
            "Vélizy",
            "0140837647",
            "Jacques",
            "Delormeau",
            "jacques@delormeau.fr",
            "REF_TEST_AVOCAT"
        ));

        $this->synchronize($sinistre_asqua_demo, $sinistre);
        ob_clean();
    }

    public function checkGrillesAnalyse() {
        $file_r = $this->_ci->doctrine->em->getRepository('Entities\File');
        $files = $file_r->getRapports_expert();

        foreach ($files as $file) {
            echo "check du fichier : " . $file->getId();
            $date_r = new DateTime;
            $date_r = $file->getDate_reception();
            $diff = $date_r->diff(new DateTime)->format('%R%a days');
            $nb_jours = $diff[1];
            //Check des 8 jours depuis reception
            if ($diff[0] == "+" && $nb_jours >= 8) {
                $grille = $file->getGrilleAnalyse();
                if ($grille && $grille->getDiffusee() == true)
                    continue;
                $id_agence = $file->getStatut()->getSinistre()->getAffaire()->getId_agence();
                $statut = $file->getStatut();
                $rca = $this->_ci->doctrine->em->getRepository('Entities\Rcr')->findOneBy(array('id_agence' => $id_agence));
                $code_affaire = $file->getStatut()->getSinistre()->getAffaire()->getCode();
                $num_sin = $file->getStatut()->getSinistre()->getNum_chrono();

                $this->_ci->load->helper('mailer');

                $data["rca"] = $rca;
                $data["file"] = $file;
                $data['nb_jours'] = $nb_jours;

                $mail = makeMail('rappel_grille_analyse.html.twig', $data);
                @$mail_rca = $rca->getMail_rca();
                @$mail_rcr = $rca->getMail_rcr();
                if ($mail_rca) {
                    echo "ok";
                    sendMail($mail, "Rappel - Analyse rapport expert", $mail_rca, "gaia_contentieux@ne_pas_repondre.net", $mail_rcr, null);
                } else {
                    sendMail("RCA introuvable grille analyse aff :" . $code_affaire . ", sin : " . $num_sin, "RCA introuvable", "martin.le-barbenchon@qualiconsult.fr", "contentieux@qualigroup.net", null, null);
                }
                echo "affaire : " . $code_affaire;
                echo "agence :" . $id_agence;
                echo "rca :" . $mail_rca;
                echo $mail;
            }
            $grille = $file->getGrilleAnalyse();
            //check des mails differes
            if ($grille instanceof Entities\GrilleAnalyse && (!$grille->getDiffusee() && !$grille->getDifferee() == null && $grille->getDifferee()->format('d/m/Y') == date('d/m/Y'))) {
                $this->_ci->load->helper('mailer');
                if ($grille->getDoc()->getStatut() instanceof \Entities\Statut_DO) {
                    $mail = makeMail('grille_analyse/mail3_do.twig', array('grille' => $grille));
                    $dest = getMailDest(19);
                }
                if ($grille->getDoc()->getStatut() instanceof \Entities\Statut_AV1) {
                    $mail = makeMail('grille_analyse/mail3_av1.twig', array('grille' => $grille));
                    $dest = getMailDest(19);
                }
                if ($grille->getDoc()->getStatut() instanceof \Entities\Statut_judiciaire) {
                    $mail = makeMail('grille_analyse/mail3_judiciaire.twig', array('grille' => $grille));
                    $dest = getMailDest(20);
                }

                $cc = $dest['CC'];
                $rcr = $this->_ci->doctrine->em->getRepository('Entities\Rcr')->findOneBy(array('id_agence' => $grille->getDoc()->getStatut()->getSinistre()->getAffaire()->getId_agence()))->getMail_rcr();
                $cc .= "," . $rcr;
                $mail_expert_do = $grille->getDoc()->getStatut()->getContact_expert()->getEmail();
                sendMail($mail, "Grille d'analyse expert DO", $mail_expert_do, $grille->getMail_auteur(), $cc, $grille->getDoc());
                $grille->setDiffusee(new DateTime);
                $this->_ci->doctrine->em->persist($grille);
                $this->_ci->doctrine->em->flush();
            }
        }
    }

}
