<?php

use Entities\Sinistre;
use Entities\File;

class Files {

    private $dir;
    private $ci;
    private $doctrine;
    private $basePath;

    public function __construct() {
        $this->ci = &get_instance();
        $this->doctrine = $this->ci->doctrine;
        $this->dir = 'files';
        $this->basePath = BASEPATH . "../" . $this->dir . "/";
    }

    /**
     * Definis le path pour un sinistre
     *
     * @param Sinistre $sinistre
     * @param Statut $statut
     * @param Affaire $affaire
     * @return String path
     */
    public function getPath($sinistre, $statut = null, $affaire) {
        $uploaddir = $affaire->getCode();
        if ($sinistre != null)
            $uploaddir .= "/sinistre" . $sinistre->getNum_chrono();
        else
            $uploaddir .= "/docs";

        if ($statut != null) {
            $entity = explode("\\", get_class($statut));
            $uploaddir .= '/' . end($entity);
        }

        if (!is_dir($this->basePath . $uploaddir)) {
            mkdir($this->basePath . $uploaddir, null, true);
        }

        return $uploaddir;
    }

    /**
     * @param type $file
     * @param type $type
     * @param type $statut ou $sinsitre <==
     * @param type $affaire
     * @param type $date
     * @return boolean|File
     */
    public function upload($file, $type, $statut = null, $affaire = null, $date = null, $date_reception = null, $nom_public = null) {

        if ($affaire == null)
            $affaire = $this->ci->context->getAffaire();

        if ($statut instanceof \Entities\Statut)
            $sinistre = $statut->getSinistre();
        else if ($statut instanceof Sinistre) {
            $sinistre = $statut;
            $statut = null;
        } else
            $sinistre = $this->ci->context->getSinistre();

        $uploaddir = $this->getPath($sinistre, $statut, $affaire);

        $split = explode(".", $file['name']);
        $ext = end($split);
        $name = str_pad($type->getNum(), 3, '0', STR_PAD_LEFT) . uniqid();

        $uri = $uploaddir . "/" . $name . '.' . $ext;
        $fullURI = $this->basePath . $uri;
        if (move_uploaded_file($file['tmp_name'], $fullURI)) {

            $efile = new File();
            $efile->setUri($uri);
            $efile->setNom($file['name']);
            $efile->setType($type);
            $efile->setUpload_date(new DateTime());
            $efile->setExt($ext);
            $efile->setSize($file['size']);
            $efile->setDate_reception($date_reception);
            if ($nom_public == $file["name"] || $nom_public == null || $nom_public == "") {
                $nom_public = $file["name"];
                $split = explode(".", $nom_public);
                array_pop($split);
                $nom_public = implode(".", $split);
            }
            $efile->setNom_public($nom_public);
            if ($date)
                $efile->setDate($date);
            var_dump($efile->getType()->getNum());
            if ($efile->getType()->getNum() > 19)
                $efile->setSinistre($sinistre);
            else
                $efile->setAffaire($affaire);
            if ($statut != null)
                $efile->setStatut($statut);

            $this->doctrine->em->persist($efile);
            $this->doctrine->em->flush();

            return $efile;
        } else {
            return false;
        }
    }

    public function safeMove($oldfile, $newfile) {
        if (!rename($oldfile, $newfile)) {
            if (copy($oldfile, $newfile)) {
                unlink($oldfile);
                return TRUE;
            }
            return FALSE;
        }
        return TRUE;
    }

    public function move($file, $num_type, $statut = null, $affaire = null) {
        if ($statut) {
            $sinistre = $statut->getSinistre();
        } else {
            $sinistre = null;
        }

        $uploaddir = $this->getPath($sinistre, $statut, $affaire);

        $split = explode(".", $file['name']);
        $ext = end($split);

        $name = str_pad($num_type, 3, '0', STR_PAD_LEFT) . uniqid();

        $uri = $uploaddir . "/" . $name . '.' . $ext;
        $fullURI = $this->basePath . $uri;
        if ($this->safeMove($file['tmp_name'], $fullURI)) {
            $this->ci->load->model("Documents_model");
            $doc = new Document_dto();

            $doc->nom = $file['name'];
            $doc->nom_public = $file['name'];
            $doc->date = new DateTime();
            $doc->date_reception = new DateTime();
            $doc->ext = $ext;
            $doc->num_type = $num_type;
            $doc->size = $file["size"];
            $doc->uri = $uri;

            $id = $this->ci->Documents_model->ajouter($doc);

            $doc_repo = $this->doctrine->em->getRepository("Entities\File");
            $efile = $doc_repo->find($id);

            return $efile;
        } else {
            return false;
        }
    }

}
