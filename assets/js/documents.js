$(document).ready(function () {

    function filtrer() {

        var recherche = $('input[name="docs_recherche"]').val().toLowerCase();
        var type = $('select[name="docs_type"]').val();
        var date = $('#input_date').val();
        var cat = $('select[name="docs_categorie"]').val();
        var filtres = {
            recherche,
            type,
            date,
            cat
        }
        ;
        //Enregistre les filtres en session
        $.post(AJAXPATH + "/documents/enregistrer_filtres", {"data": filtres});

        let nums_types_cat = [];
        for (let elem of liste_categories) {
            if (elem.num_categorie == cat) {
                nums_types_cat.push(elem.num_type);
            }
        }

        $('.row_doc').toArray().forEach(function (elem) {

            $(elem).show();
            if (recherche && recherche !== "") {
                console.log($(elem).find('.tdnom').html().toLowerCase());
                if ($(elem).find('.tdnom').html().toLowerCase().indexOf(recherche) == -1) {
                    $(elem).hide();
                }
            }

            if (type && type !== "none") {
                if ($(elem).attr("type_doc") !== type) {
                    $(elem).hide();
                }
            }

            if (date && date !== "") {
                if ($(elem).attr("date_doc").indexOf(date) == -1) {
                    $(elem).hide();
                }
            }

            if (cat && cat !== "none") {
                if (!nums_types_cat.includes(parseInt($(elem).attr("type_doc")))) {
                    $(elem).hide();
                }
            }

        });
    }

    $('input[name="docs_recherche"], #input_date').keyup(filtrer);

    $('select[name="docs_categorie"]').change(function () {
        var cat = $(this).val();
        $('select[name="docs_type"]').val("none");
        $('select[name="docs_type"] option').toArray().forEach(function (opt) {
            $(opt).show();
            if (cat != "none" && $(opt).attr("cat") != cat) {
                $(opt).hide();
            }
        });
    });

    $('select[name="docs_type"],select[name="docs_categorie"], input[name="docs_gaia"], input[name="docs_contentieux"], #input_date').change(filtrer);

    filtrer();

    $(document).on("click",".btn_renommer",function () {
        var nom = prompt("Entrez le nouveau nom du document");
        var id = $(this).attr("doc_id");
        $.post(AJAXPATH + "/documents/renommer/" + id, {"nom": JSON.stringify(nom)}, function (r) {
            if (r == "ok") {
                $($(this).parents("tr").find("td")[1]).html(nom);
            }
        }.bind(this));
    });
    $(document).on("click",".btn_supprimmer",function () {
        if (confirm("Êtes-vous certain de vouloir supprimer le document?")) {
            var id = $(this).attr("doc_id");
            $.post(AJAXPATH + "/documents/supprimer/" + id, function (r) {
                document.location = AJAXPATH + "/documents";
            });
        }
    });
    $(".btn_lier_affaire").click(function () {
        var id = $(this).attr("doc_id");
        $.post(AJAXPATH + "/documents/lier_affaire/" + id, function (r) {
            document.location = AJAXPATH + "/documents/gestion";
        });
    });
    $(".btn_lier_sinistre").click(function () {
        var id = $(this).attr("doc_id");
        $.post(AJAXPATH + "/documents/lier_sinistre/" + id, function (r) {
            document.location = AJAXPATH + "/documents/gestion";
        });
    });
    $(".btn_flag").click(function () {
        var id = $(this).attr("doc_id");
        $.post(AJAXPATH + "/documents/flagger/" + id, function (r) {
            document.location = AJAXPATH + "/documents/gestion";
        });
    });
    $('#btn_upload').click(function () {
        $('#dlg_upload').dialog({
            resizable: false,
            width: 700
        });
    });
    $('#input_file').change(function () {
        var elem = document.getElementById('input_file');
        console.log(elem.files);
        var nom_fichier = elem.files[0]['name'];
        $('#input_nom').val(nom_fichier);
    });
    var url = AJAXPATH + "/ajax_c/getTypeDocs";
    $.ajax(url).done(function (data) {
        $('select[name=type_doc]').empty().append(data);
    });
    $('#btn_enregistrer').click(function (e) {
        var doc = $('#input_file').val();
        var ext = doc.substring(doc.length - 3);
        if (ext.toLowerCase() != 'pdf' && ext.toLowerCase() != 'eml') {
            e.preventDefault();
            alert("Seuls les documents au format pdf peuvent-être importés");
        }
    });

    $(document).on("change", ".select_type_doc", function () {
        let num = $(this).val();
        let id_doc = $(this).attr("id_doc");

        $.get(AJAXPATH + "/documents/changer_type/" + id_doc + "/" + num, function (r) {
            if (r == "ok") {
                $(this).parent().parent().attr("type_doc", num);
            }
        }.bind(this));
    });

    $(document).on("change", ".input_date_doc", function () {
        let date = $(this).val();
        let id_doc = $(this).attr("id_doc");

        $.get(AJAXPATH + "/documents/changer_date/" + id_doc + "?date=" + JSON.stringify(date));
    });

    trier_par_date = function (dir) {

        let ordre = [];

        $(".row_doc").toArray().forEach(function (row) {
            let date_str;
            let split;
            let date;
            let td_date = $($(row).find("td").toArray()[3]);
            if ($(td_date).find("input").toArray().length > 0) {
                date_str = $(td_date).find("input").val();
                split = date_str.split("-");
                date = new Date(split[0], split[1] - 1, split[2]);
            } else {
                date_str = $(td_date).html();
                split = date_str.split("/");
                date = new Date(split[2], split[1] - 1, split[0]);
            }

            let wrapper = {
                date,
                row
            };
            ordre.push(wrapper);
        });

        ordre.sort(function (a, b) {
            if (dir == 'asc') {
                return comparer_date(a.date, b.date);
            } else {
                return -1 * comparer_date(a.date, b.date);
            }
        });

        $(".liste_docs").empty();

        ordre.forEach(function (elem) {
            $(".liste_docs").append(elem.row);
        });

    };

    trier_par_nom = function (dir) {

        let ordre = [];

        $(".row_doc").toArray().forEach(function (row) {
            let libelle;
            let td_libelle = $($(row).find("td").toArray()[1]);
            libelle = $(td_libelle).html();
            let wrapper = {
                libelle,
                row
            };
            ordre.push(wrapper);
        });
        if (dir == "asc") {
            ordre.sort(function (a, b) {
                if (a.libelle < b.libelle) {
                    return 1;
                }
                if (a.libelle == b.libelle)
                    return 0;
                return -1;
            });
        } else {
            ordre.sort(function (a, b) {
                if (a.libelle < b.libelle) {
                    return 1;
                }
                if (a.libelle == b.libelle)
                    return 0;
                return -1;
            }).reverse();
        }
        $(".liste_docs").empty();

        ordre.forEach(function (elem) {
            $(".liste_docs").append(elem.row);
        });
    };

    $(document).on("click", ".sortlibelle", function () {

        $('span.sort').removeClass("glyphicon-triangle-top").removeClass("glyphicon-triangle-bottom").addClass("glyphicon-triangle-right");

        let ordre_actuel = $(this).attr("ordre");
        if (ordre_actuel == "desc" || ordre_actuel == "none") {
            trier_par_nom("asc");
            $(this).attr("ordre", "asc");
            $(this).removeClass("glyphicon-triangle-bottom");
            $(this).addClass("glyphicon-triangle-top");
        } else {
            trier_par_nom("desc");
            $(this).removeClass("glyphicon-triangle-top");
            $(this).addClass("glyphicon-triangle-bottom");
            $(this).attr("ordre", "desc");
        }
    });

    function comparer_date(date1, date2) {
        let date1int = date1.getYear() + '' + ("00" + date1.getMonth()).substr(-2, 2) + '' + ("00" + date1.getDate()).substr(-2, 2);
        let date2int = date2.getYear() + '' + ("00" + date2.getMonth()).substr(-2, 2) + '' + ("00" + date2.getDate()).substr(-2, 2);

        if (date1int === date2int)
            return 0;
        if (date1int > date2int)
            return 1;

        return -1;
    }

    trier_par_date("desc");

    $(document).on("click", ".sortdate", function () {

        $('span.sort').removeClass("glyphicon-triangle-top").removeClass("glyphicon-triangle-bottom").addClass("glyphicon-triangle-right");

        let ordre_actuel = $(this).attr("ordre");
        if (ordre_actuel == "desc" || ordre_actuel == "none") {
            trier_par_date("asc");
            $(this).removeClass("glyphicon-triangle-bottom");
            $(this).addClass("glyphicon-triangle-top");
            $(this).attr("ordre", "asc");
        } else {
            trier_par_date("desc");
            $(this).removeClass("glyphicon-triangle-top");
            $(this).addClass("glyphicon-triangle-bottom");
            $(this).attr("ordre", "desc");
        }
    });

});