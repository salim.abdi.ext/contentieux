var qcDocs = (function () {
    var public = {};
    var docs = [];
    var icon_pdf;
    var icon_doc;
    var icon_xl;
    var icon_default;
    var controller_url;
    var docs_list = [];
    var docs_requis = [];

    public.test = function () {
        console.log(docs);
        console.log(docs_list);
    }
    public.getList = function () {
        return docs_list;
    };



    public.init = function (url, id, base_url, form_id, min, max, types_autorises, types_requis) {

        base_url = base_url;
        controller_url = url;
        icon_pdf = "<img width=14 src='" + base_url + "/assets/images/qcdocs/pdf.png'>";
        icon_doc = "<img width=14 src='" + base_url + "/assets/images/qcdocs/word.png'>";
        icon_xl = "<img width=14 src='" + base_url + "/assets/images/qcdocs/excel.png'>";
        icon_default = "<img width=14 src='" + base_url + "/assets/images/qcdocs/file.png'>";

        $('#list_docs').attr('form', form_id);
        if (types_requis == null) {
            $('#div_docs_requis').hide();
        }
        for (var type in types_requis) {
            var requis = {
                type: type,
                nb: types_requis[type],
                count: 0
            };
            docs_requis.push(requis);
        }

        docs_requis.forEach(function (requis) {
            $.ajax(url + "getType/" + requis.type).done(function (data) {
                requis.type_str = data;
                displayRequis();
            });
        });


        $('#' + form_id).submit(function (e) {
            var nb_docs = Object.keys(docs_list).length;
            if (min != null && min != undefined && nb_docs < min) {
                e.preventDefault();
                alert('Veuillez sélectionner au moins ' + min + ' document(s)');
            }
            if (max != null && max != undefined && nb_docs > max) {
                e.preventDefault();
                alert('Veuillez sélectionner moins de ' + max + ' documents');
            }

            docs_requis.forEach(function (requis) {
                if (requis.count < requis.nb) {
                    e.preventDefault();
                    alert('Veuillez sélectionner ' + requis.nb + ' document(s) de type "' + requis.type_str + '"');
                }
            });
        })

        $.ajax(url + "getTypesList").done(function (data) {
            var types = JSON.parse(data);
            if (types_autorises == undefined || types_autorises.length > 1) {
                var option = "<option value=''>Tous les types</option>";
                $('#select_type_doc').append(option);
            }

            types.forEach(function (type) {
                if (types_autorises != undefined) {
                    for (var typea in types_autorises) {
                        if (type[0] == types_autorises[typea]) {
                            var option = "<option value='" + type[0] + "'>" + type[1] + "</option>";
                            $('#select_type_doc').append(option);
                        }
                    }
                } else {
                    var option = "<option value='" + type[0] + "'>" + type[1] + "</option>";
                    $('#select_type_doc').append(option);
                }
            });
            getDocs();
        });

        $('#btn_filtrer_docs').click(function (e) {
            e.preventDefault();
            getDocs();
        });

        function getDocs() {
            var recherche = $('#recherche_doc').val();
            var type = $('#select_type_doc').val();
            $.post(url + "filtrer", {
                recherche: recherche,
                type: type
            }, function (data) {
                var data_docs = parseToArray(data);
                docs = data_docs;
                fillList(docs);
            });
        }

    }

    function countRequis() {
        docs_requis.forEach(function (requis) {
            var type = requis.type;
            requis.count = 0;
            //docs_list.forEach(function (doc) {
            for (var d in docs_list) {
                var doc = docs_list[d];
                if (doc.num_type == requis.type) {
                    requis.count++;
                }
            }
            //});
        });
    }

    function displayRequis() {
        countRequis();
        $('#qcDocsReqtbody').empty();
        docs_requis.forEach(function (e) {
            var ok = "<span class='glyphicon glyphicon-ok' style='color: green'></span>";
            var nok = "<span class='glyphicon glyphicon-warning-sign' style='color: red'></span>";

            if (e.nb == e.count)
                var isok = ok;
            else
                var isok = nok;
            var html = "<tr><td>" + e.type_str + "</td><td>" + e.nb + "</td><td>" + e.count + "</td><td>" + isok + "</td></tr>";
            $('#qcDocsReqtbody').append(html);
        });
    }

    function parseToArray(data) {
        data = JSON.parse(data);
        var result = [];
        for (var d in data) {
            result[d] = data[d];
        }
        return result;
    }

    function createRow(doc, sel) {
        if (doc.date == null)
            doc.date = "";
        var str = "<tr class='row_doc' id=" + doc.id + "><td style='max-width: 200px'>" + doc.nom_public + "</td><td>" + doc.type + "</td><td class='doc_dwnld' style='cursor: pointer'>" + doc.icon + "</td>";
        if (sel == false)
            return str + "<td><span style='cursor: pointer' class='remove_doc glyphicon glyphicon-remove'></span></td></tr>";
        else
            return str + "<td><input class='check_doc' type='checkbox'></td></tr>";
    }

    function fillList(docs) {
        $('#qcDocstbody').empty();
        for (var doc in docs) {
            doc = docs[doc];
            doc.icon = icon_default;
            if (doc.ext !== null) {
                switch (doc.ext.toLowerCase()) {
                    case "pdf" :
                        doc.icon = icon_pdf;
                        break;
                    case "doc" :
                        doc.icon = icon_doc;
                        break;
                    case "xlsx" :
                    case "csv" :
                        doc.icon = icon_xl;
                        break;
                }
            }
            var row = createRow(doc);
            $('#qcDocstbody').append(row);

            for (var selected in docs_list) {
                if (docs_list[selected].id == doc.id)
                    $('#qcDocstbody tr[id="' + doc.id + '"] input').prop('checked', true);
            }
        }
    }

    function fillselected(docs) {
        $('#qcDocsSeltbody').empty();
        for (var doc in docs) {
            doc = docs[doc];
            doc.icon = icon_default;
            switch (doc.ext) {
                case "pdf" :
                    doc.icon = icon_pdf;
                    break;
                case "doc" :
                    doc.icon = icon_doc;
                    break;
                case "xlxs" :
                case "csv" :
                    doc.icon = icon_xl;
                    break;
            }
            var row = createRow(doc, false);
            $('#qcDocsSeltbody').append(row);
        }
    }

    $(document).on('change', '.check_doc', function (e) {

        var doc_id = $(this).parents("tr").prop('id');
        if ($(this).prop('checked'))
            docs_list[doc_id] = docs[doc_id];
        else
            delete docs_list[doc_id];
        fillselected(docs_list);
        updateInput();
        displayRequis();
    });

    $(document).on('click', '.remove_doc', function (e) {
        var doc_id = $(this).parents("tr").prop('id');
        delete docs_list[doc_id];
        fillselected(docs_list);
        updateInput();
        $('#qcDocstbody tr[id="' + doc_id + '"] input').prop('checked', false);
        displayRequis();
    });

    $(document).on('click', '.doc_dwnld', function (e) {
        var doc_id = $(this).parents("tr").prop('id');
        window.open(AJAXPATH + "/documents/download/" + doc_id);
    });

    function updateInput() {
        var str = "";
        var i = 0;
        for (var doc in docs_list) {
            if (i != 0)
                str += ",";
            str += docs_list[doc].id;
            i++;
        }
        $('#list_docs').val(str);
        var updateEvent = new CustomEvent(
                "docsUpdated",
                {
                    bubbles: true,
                    cancelable: true
                }
        );

        document.getElementById('docs_list').dispatchEvent(updateEvent);
    }

    return public;
})();